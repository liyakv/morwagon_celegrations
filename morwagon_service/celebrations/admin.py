from django.contrib import admin
from .models import Units,Tax_details,Enquiry,Adds,Album_details, Morwagon_users,Category, Review_images,Sub_category,Products,Popular,Trending,Favourites,Banner,Packages,Vendor_package,Album, Verifyemail,Videos,Review,Review_images,Settings,Notification,Forgetpassword,Review,Addscount,Adds,Review
from .models import PriceUnit, Report_vendor, Tax_details,Enquiry,Adds,Album_details, Morwagon_users,Category, Review_images,Sub_category,Products,Popular,Trending,Favourites,Banner,Packages, Units,Vendor_package,Album, Verifymobileupdate,Videos,Review,Review_images,Settings,Notification,Forgetpassword,Review,Addscount,Adds,Faq,Review_images
# from .models import Tax_details,Enquiry,Adds, Morwagon_users,Category, Review_images,Sub_category,Products,Popular,Trending,Favourites,Banner,Packages,Vendor_package,Album,Videos,Review,Review_images,Settings,Notification,Forgetpassword,Tax_details
# Register your models here.

admin.site.register(Forgetpassword)   
admin.site.register(Morwagon_users)   
admin.site.register(Album_details)
admin.site.register(Enquiry)
admin.site.register(Tax_details)
admin.site.register(Vendor_package)
admin.site.register(Products)
admin.site.register(Popular)
admin.site.register(Review)
admin.site.register(Category)
admin.site.register(Packages)
admin.site.register(Sub_category)
admin.site.register(Album)
admin.site.register(Addscount)
admin.site.register(Adds)
admin.site.register(Settings)

   
admin.site.register(Report_vendor)
admin.site.register(Verifymobileupdate)
admin.site.register(Units)
admin.site.register(PriceUnit)
admin.site.register(Faq)
admin.site.register(Review_images)
admin.site.register(Verifyemail)
admin.site.register(Banner)

