from.models import Banner,Category,Sub_category,Morwagon_users,Adds
from rest_framework import serializers
from django.contrib.auth.models import User

class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model=Banner
        fields = "__all__"
class SubcategorySerializer(serializers.ModelSerializer):
    class Meta:
    
        model=Sub_category
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', )

class TimeSerializer(serializers.ModelSerializer):
    """
    specifying the field here rather than relying on `depth` to automatically
    render nested relations allows us to specify a custom serializer class
    """
    user = UserSerializer()

    class Meta:
        model = Morwagon_users        

class AddSerializer(serializers.ModelSerializer):
    class Meta:
    
        model=Adds
        fields = ("image",)


