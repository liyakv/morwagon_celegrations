
// -----------------------------------------------------for places api------------------------------------------------------------------
function initMap() {
    const center = { lat: 50.064192, lng: -130.605469 };

const defaultBounds = {
  north: center.lat + 0.1,
  south: center.lat - 0.1,
  east: center.lng + 0.1,
  west: center.lng - 0.1,
};
const input = document.getElementById("pac-input");


const options = {
  types: ['(cities)'],
  componentRestrictions: {country: "in"}
};
const autocomplete = new google.maps.places.Autocomplete(input, options);

autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      var latitude = place.geometry.location.lat();
      var longitude = place.geometry.location.lng();
      document.getElementById('lat').value = latitude;
      document.getElementById('lng').value = longitude;
    });
}


// <!------------------------------------------------same product name check----------------------------------------------------------- -->

$("#id_pdtName").change(function(){
        var name = $(this).val();
        $.ajax({
            method: "GET",
            url: "/vendor/NameCheck",
            data: { 'nme':name,
                    },
                    success: function(data)
                   
            {
                console.log(data['status'])
                if (data['status']==200){
                    $('#pdtName-error').css("display", "block");
                     $('#pdtName-error').text("Name already exist");
                    $("#id_pdtName").val('');
                }
            }
            })
    });
// <!-- ---------------------------------------------------ckeditor------------------------------------------------------------------- -->

    CKEDITOR.replace( 'content', {
	toolbar: [
		{ name: 'document', items: [ '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
		[  '-' ],			// Defines toolbar group without name.
		// '/',																					// Line break - next group will be placed in new line.
		{ name: 'basicstyles', items: [  'Bold', 'Italic' , 'Strike'] }
	]
});



// <!-- ---------------------------------------------------validation------------------------------------------------------------------------ -->


$(function () {
    CKEDITOR.instances['cke'].on('contentDom', function () {
        this.document.on('keyup', function (event) {
            formValidation();
        });
    });
});
    function formValidation() { 
        submit=true;
        let pdtName = $('#id_pdtName').val();
        let imageURL = $('#hidden-imageedit').val();
        let location = $('#pac-input').val();
        let category = $('#id_category').val();
        let openingTime = $('#opening_time').val();
        let closingTime = $('#closing_time').val();
        let phonenmbr = $('#id_phonenmbr').val();
        let address = $('#id_address').val();
        var lat=$('#lat').val();
        var lng=$('#lng').val();

        var pass=true;
        repeatArray.forEach((element,key) => {
           
            if(element.unit==""){
                $('#unitErr'+key).show();
                pass=false;
            }
            else{
                $('#unitErr'+key).hide();
            }
            if(element.checkbox==0 && element.field1==""){
                $('#f1Err'+key).show();
                pass=false;
            }
            else{
                $('#f1Err'+key).hide();
            }
            if(element.checkbox==1){
                
                if(element.field2==""){
                    $('#f2Err'+key).show();
                    pass=false;
                   
                }
                else{
                   
                    $('#f2Err'+key).hide();
                }
                if(element.field3==""){
                    $('#f3Err'+key).show();
                    pass=false;
                }
                else{
                    $('#f3Err'+key).hide();
                }
            }
            else{

            }
        });

        console.log(pass);     
        let description =  CKEDITOR.instances['cke'].getData();
        let img_val=$('#edit-imageedit').val();

        if (pdtName === "") {
            $('#pdtName-error').css("display", "block");
            $('#pdtName-error').text("Enter the valid service name");
            // is_valid = false;
        } else {
            $('#pdtName-error').css("display", "none");
        }
// -------------------------------------------------------------------------------------------------------------------------
        if (openingTime === "") {
            $('#openTime-error').css("display", "block");
            $('#openTime-error').text("Enter opening time");
            // is_valid = false;
        } else {
            $('#openTime-error').css("display", "none");
        }
        if (closingTime === "") {
            $('#closeTime-error').css("display", "block");
            $('#closeTime-error').text("Enter closing time");
            // is_valid = false;
        } else {
            $('#closeTime-error').css("display", "none");
        }
// -------------------------------------------------------------------------------------------------------------------------

        if (imageURL === "" && img_val ==="" ) {
            $('#image-error').css("display", "block");
            $('#image-error').text(" please select thumbnail image");
        } else {
            $('#image-error').css("display", "none");
        }

        if (phonenmbr === "") {
            $('#phonenmbr-error').css("display", "block");
            $('#phonenmbr-error').text("Enter valid phone number");
        } else {
            var len_nmbr=phonenmbr.length
                if (len_nmbr!=10){
                    $('#phonenmbr-error').css("display", "block");
                $('#phonenmbr-error').text("phone number must be 10 digits");
                }
                else{
                    $('#phonenmbr-error').css("display", "none");
                }            
        }
        if (address === "") {
            $('#address-error').css("display", "block");
            $('#address-error').text("Enter valid address");
        } else {
            

            if (address.replace(/<[^>]*>/gi, '').length - 1 > 499) {
                // alert("else")
                $('#address-error').css("display", "block");
                $('#address-error').text("Address too long(max.500 characters)");
            } else {
                $('#address-error').css("display", "none");
            }
            
        }
        if (category === "") {
            $('#category-error').css("display", "block");
            $('#category-error').text("Enter valid category");
        } else {
            $('#category-error').css("display", "none");
        }

        if (lat === "" || lng ==="") {
        document.getElementById("pac-input").value = '';
        } 
        if (location === "" ) {          
                $('#location-error').css("display", "block");
                $('#location-error').text("Enter valid location");
            } else {
                $('#location-error').css("display", "none");
            }
            if (description === "") {
                $('#description-error').css("display", "block");
                $('#description-error').text("Enter description");
            } else {
                if (description.replace(/<[^>]*>/gi, '').length - 1 > 1200) {
                    $('#description-error').css("display", "block");
                    $('#description-error').text("Description too long(max.1200 characters)");
                } else {
                    $('#description-error').css("display", "none");
                }
            }

            if(pdtName!='' && location!='' && lat!='' && lng!='' && category!='' && description!='' && openingTime!='' && closingTime!='' && phonenmbr!='' && address!='' && pass==true){
            var is_valid=0 
            // alert(pass+"555")           
            }
            return is_valid;
           
        }

        $("#pac-input").change(function() {
            document.getElementById("lat").value = '';
            document.getElementById("lng").value = '';

        })

        function formSubmission() { 
            // alert(pass)
            // alert("submit function")
            is_valid=formValidation()
         if (is_valid==0){
                var textmenu;
                var textappointment;
                var form_clone= JSON.stringify(repeatArray);
                var service_name=$('#id_pdtName').val();
                var _location =$('#pac-input').val();
                var lat=$('#lat').val();
                var lng=$('#lng').val();
                var category1=$('#id_category').val();
                var subcategory=$('#id_subcategory').val();
                var opening_time=$('#opening_time').val();
                var closing_time=$('#closing_time').val();
                var phonenmbr=$('#id_phonenmbr').val();
                var address=$('#id_address').val();
                textmenu=$('#textmenu').val();
                csrf = $('#csrf').val();
                textappointment=$('#textappointment').val();
                var cke_description=CKEDITOR.instances['cke'].getData();
                var img=$('#hidden-imageedit').val();
                var editid= $('#editId').val();
                data2={ 
                        'unit_lst':form_clone,
                        'service_name':service_name,
                        '_location':_location,
                        'lat':lat,
                        'lng':lng,
                        'category1':category1,
                        'subcategory':subcategory,
                        'opening_time':opening_time,
                        'closing_time':closing_time,
                        'phonenmbr':phonenmbr,
                        'address':address,
                        'textmenu':textmenu,
                        'textappointment':textappointment,
                        'cke_description':cke_description,
                        'img':img,
                        'editid':editid,
                        }                    
                $.ajax({
                    method: "POST",
                    url: `/vendor/details/?Eid=${editid}`,
                    headers: { 'X-CSRFToken': csrf },
                    data: data2,
                    success: function(data)
                    {
                        
                    Swal.fire(
                        'Send for review, Your service will be added after approval of admin',
                      ).then(function() {
                        window.location = `/vendor/create`;
                    });

                    }
                    })
            }
            else{
                console.log("else....................")
            }
        }



        
           
    $('#textmenu').css('display', 'none');
    $('#textappointment').css('display', 'none');

    $('input[type=radio][name=menuradio]').change(function () { 
        // debugger;
        var selectedValuemenu = $(this).attr('value');
        if(selectedValuemenu=="0"){
            $('#textmenu').css('display', 'block');
        }
        else{
            $('#textmenu').val('');
            $('#textmenu').css('display', 'none'); 
        }
    });

    $('input[type=radio][name=appointmtntradio]').change(function () { 
    // debugger;
    var selectedValueapp = $(this).attr('value');
        if(selectedValueapp=="0"){
            $('#textappointment').css('display', 'block');
        }
        else{
            $('#textappointment').val('');
            $('#textappointment').css('display', 'none');
        }
    });

// <!------------------------------------------------------for form cloning----------------------------------------------------------------------------- -->

    $('.repeatErr').hide();
    var repeatArray=[];
    var unitOptions=[];
    var unitOptions1=[];
    var repeatArray_edit=[];
    var submit=false;
    var editid= $('#editId').val()
    if(editid==""){
        repeatArray=[
            {   
                "unit":"",
                "checkbox":0,
                "field1":"",
                "field2":"",
                "field3":""
            }
        ];
    }

    


       
    $(document).ready(function(){
          


        var editid= $('#editId').val()
        if(editid){     
            $.ajax({
                type: 'GET',
                url: `/vendor/get-units/?p=${editid}`,
                success: function (dataunit) {  
                for (var i = 0; i < dataunit.length; i++){
                    var checkbox =dataunit[i].fields.is_fixedprice
                    if (checkbox==1){
                        checkbox=0
                    }
                    else{
                        checkbox=1
                    }
                var obj={"unit":dataunit[i].fields.unit,"checkbox":checkbox,"field1":dataunit[i].fields.price,"field2":dataunit[i].fields.from_price,"field3":dataunit[i].fields.to_price};
                repeatArray.push(obj)
                            }
                }
            });
        }
        $.ajax({
            type: 'GET',
            url: '/vendor/get-units',
            success: function (responsedata) {

                for (var i = 0; i < responsedata.length; i++){
                    var obj={"value":responsedata[i].pk,"label":responsedata[i].fields.unit};
                    unitOptions1.push(obj);
                }

                append();
            }
        });

       

    });

    append=()=>{
        
        
        var div ='';
        var btnDIv='';
        
        $("#main").empty();
        // console.log(repeatArray);
        $.each(repeatArray, function(index, data) {
            if(index==0){
                btnDIv='<div class="col-2 mt-4">'
                    +'<button class="btn btn-success mt-2" onclick="alterRows(0,'+index+')">Add more unit</button>'
                +'</div>';
            }
            else{
                btnDIv='<div class="col-2 mt-4">'
                    +'<button class="btn btn-danger mt-2" onclick="alterRows(1,'+index+')"><i class="fa fa-trash"></i></button>'
                +'</div>';
            }
            var unitOptions=[];
            $.each(unitOptions1,function(i,option){
                var selected="";
                
        
                if(data.unit==option.value){
                    selected="selected";
                }
                else{
                    selected="";
                }
               
                unitOptions+='<option value='+option.value+' '+selected+'>'+option.label+'</option>';
        
            });
            var valYes,valNo,div1,div2="";
  
            
            if(data.checkbox==0){
                // $('#field_one'+index).val('')


                data.field2="";
                data.field3="";
                // delete repeatArray[index].field2
                // delete repeatArray[index].field3
                // console.log(repeatArray[index],".............................repeatArray[index]")
                // repeatArray[index].field2=''
                // repeatArray[index].field3=''
                valYes="checked";
                valNo="";
                div1="d-block";
                div2="d-none";

            
            }
            else{
                // delete repeatArray[index].field1
                // repeatArray[index].field1=''
                // $('#field_two'+index).val('')
                // $('#field_three'+index).val('')

                data.field1="";

                valYes="";
                valNo="checked";
                div1="d-none";
                div2="d-block";
            }

        
            div+='<div class="row"><div class="col-12 mb-4">'
            +'<p class="qstn-text">Is price fixed?</p>'
                    +'<div class="form-check-inline mt-2">'
                        +'<label class="form-check-label" for="radio'+index+'">'
                        +'<input type="radio" class="form-check-input fixedPrice'+index+'" id="radioyes'+index+'" name="checkbox'+index+'" value="0" '+valYes+'  onclick="getCheckBoxData('+index+',0)">Yes'
                        +'</label>'
                    +'</div>'
                    +'<div class="form-check-inline mt-2">'
                        +'<label class="form-check-label" for="radio'+index+'">'
                        +'<input type="radio" class="form-check-input fixedPrice'+index+'" id="radiono'+index+'" name="checkbox'+index+'" value="1" '+valNo+' onclick="getCheckBoxData('+index+',1)">No'
                        +'</label>'
                    +'</div>'
                +'</div>'
                +'<div class="col-4">'
                +'<div class="form-group">'
                    +'<label for="sel1">Select list:</label>'
                    +'<select class="form-control" id="sel'+index+'" name="'+index+'" onkeyup="formValidation()" onchange="getData('+index+',0)">'
                    +'<option value="">Select</option>'+unitOptions+''
                    
                    +'</select>'
                    +'<span id="unitErr'+index+'" class="error p-2 repeatErr">* Required</span>'
                +'</div>'
            +'</div>'
                +'<div class="col-4 one'+index+' '+div1+'">'
                    +'<div class="form-group">'
                        +'<label for="field'+index+'">Price:</label>'
                        +'<input type="text" class="form-control" id="field_one'+index+'" name="field_one'+index+'" value="'+repeatArray[index].field1+'" onpaste="return false;"   onkeypress="validate2(event)" onkeyup="formValidation()"  onchange="getData('+index+',1)">'
                        +'<span id="f1Err'+index+'" class="error p-2 repeatErr">* Required</span>'
                    +'</div>'
                    
                +'</div>'
                +'<div class="col-4 two'+index+' '+div2+'">'
                    +'<div class="form-group">'
                        +'<label for="field'+index+'">From price:</label>'
                        +'<input type="text" class="form-control" id="field_two'+index+'" name="field_two'+index+'"  value="'+repeatArray[index].field2+'" onpaste="return false;"   onkeypress="validate2(event)" onkeyup="formValidation()" onchange="getData('+index+',2)">'
                        +'<span id="f2Err'+index+'" class="error p-2 repeatErr">* Required</span>'
                    +'</div>'
                    
                    +'<div class="form-group">'
                        +'<label for="field'+index+'">To price:</label>'
                        +'<input type="text" class="form-control" id="field_three'+index+'" name="field_three'+index+'"  value="'+repeatArray[index].field3+'" onpaste="return false;"   onkeypress="validate2(event)" onkeyup="formValidation()" onchange="getData('+index+',3)">'
                        +'<span id="f3Err'+index+'" class="error p-2 repeatErr">* Required</span>'
                    +'</div>'
                    
                +'</div>'
                
                +btnDIv+'</div>';   
        });
        $("#main").append(div);
      
        $('.repeatErr').hide();
        
        
    }

    alterRows=(type,index)=>{
        // alert(option1)
        
        if(type==0){
            repeatArray.push({  
                "unit":"",
                "checkbox":0,
                "field1":"",
                "field2":"",
                "field3":""
            });
            
        }
        else{
            repeatArray.splice(index,1);
        }
        append();
    }
    
    getCheckBoxData=(index,type)=>{
        $("input[type=radio][name=checkbox"+index+"]").removeAttr('checked');
       
        if(type==1){
            value=1;
            $('.one'+index).removeClass('d-block').addClass('d-none');
            $('.two'+index).removeClass('d-none').addClass('d-block');
            $('#radiono'+index).attr('checked',true);
            $('#field_one'+index).val('')
        }
        else{
            value=0;
            $('.one'+index).removeClass('d-none').addClass('d-block');
            $('.two'+index).removeClass('d-block').addClass('d-none');
            $('#radioyes'+index).attr('checked',true);
            $('#field_two'+index).val('')
            $('#field_three'+index).val('')
        }
        repeatArray[index].checkbox=value;
        
        
    }
    
    getData=(index,type)=>{
      
        if(type==0){
            var unit_value=$('#sel'+index).val();
            repeatArray[index].unit=unit_value*1;
        }
        else if(type==1){
            var price_val=$('#field_one'+index).val();
            repeatArray[index].field1=price_val*1;
            repeatArray[index].field2='';
            repeatArray[index].field3='';
        }
        else if(type==2){
            var fromprice_val=$('#field_two'+index).val();
            repeatArray[index].field2=fromprice_val*1;
            repeatArray[index].field1='';
        }
        else if(type==3){
            var toprice_val=$('#field_three'+index).val();
            repeatArray[index].field3=toprice_val*1;
            repeatArray[index].field1='';
        }
    }

    // function validate1(evt) {
    //     var theEvent = evt || window.event;
      
    //     // Handle paste
    //     if (theEvent.type === 'paste') {
    //         key = event.clipboardData.getData('text/plain');
    //     } else {
    //     // Handle key press
    //         var key = theEvent.keyCode || theEvent.which;
    //         key = String.fromCharCode(key);
    //     }
    //     var regex = /[0-9]|\./;
    //     if( !regex.test(key) ) {
    //       theEvent.returnValue = false;
    //       if(theEvent.preventDefault) theEvent.preventDefault();
    //     }
    //   }


