# Generated by Django 3.2.7 on 2022-05-24 08:59

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Adds',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('image', models.ImageField(upload_to='images/adds')),
                ('is_approved', models.BooleanField(default=False)),
                ('reject_reason', models.CharField(blank=True, max_length=100, null=True)),
                ('temp_sts', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('album_name', models.CharField(max_length=50)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('category_name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True, null=True)),
                ('thumbnail', models.ImageField(upload_to='images/category')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Forgetpassword',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('email', models.EmailField(max_length=254)),
                ('otp', models.IntegerField()),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Morwagon_users',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('first_name', models.CharField(blank=True, max_length=100, null=True)),
                ('last_name', models.CharField(blank=True, max_length=100, null=True)),
                ('email', models.EmailField(blank=True, max_length=254, null=True, unique=True)),
                ('mobile_number', models.BigIntegerField(blank=True, null=True, unique=True, validators=[django.core.validators.MaxValueValidator(9999999999)])),
                ('town', models.CharField(blank=True, max_length=100, null=True)),
                ('role', models.IntegerField(choices=[(1, 'admin'), (2, 'vendor'), (3, 'user')])),
                ('is_rejected', models.BooleanField(default=False)),
                ('reject_reason', models.TextField(blank=True, max_length=2000, null=True)),
                ('expirydate', models.DateTimeField(blank=True, null=True)),
                ('is_updated', models.BooleanField(default=False)),
                ('update_rejected', models.BooleanField(default=False)),
                ('update_reject_reasson', models.CharField(blank=True, max_length=200, null=True)),
                ('whatsapp_number', models.BigIntegerField(blank=True, null=True, unique=True, validators=[django.core.validators.MaxValueValidator(9999999999)])),
                ('address1', models.TextField(blank=True, null=True)),
                ('address2', models.TextField(blank=True, null=True)),
                ('district', models.CharField(blank=True, max_length=100, null=True)),
                ('state', models.CharField(blank=True, max_length=100, null=True)),
                ('pincode', models.IntegerField(blank=True, null=True)),
                ('incorporation_certificate', models.FileField(blank=True, null=True, upload_to='files/')),
                ('GST', models.FileField(blank=True, null=True, upload_to='files/')),
                ('pancard', models.FileField(blank=True, null=True, upload_to='files/')),
                ('address_proof', models.FileField(blank=True, null=True, upload_to='files/')),
                ('location', models.CharField(blank=True, max_length=200, null=True)),
                ('logo_image', models.ImageField(blank=True, null=True, upload_to='images/')),
                ('thumbnail', models.ImageField(blank=True, null=True, upload_to='images/')),
                ('caption', models.CharField(blank=True, max_length=200, null=True)),
                ('Description', models.TextField(blank=True, null=True)),
                ('is_verified', models.BooleanField(default=False)),
                ('latitude', models.DecimalField(blank=True, decimal_places=10, max_digits=20, null=True)),
                ('logitude', models.DecimalField(blank=True, decimal_places=10, max_digits=20, null=True)),
                ('gst_number', models.CharField(blank=True, max_length=100, null=True)),
                ('store_name', models.CharField(blank=True, max_length=200, null=True)),
                ('page_no', models.IntegerField(default=0)),
                ('is_done', models.BooleanField(default=False)),
                ('approval', models.BooleanField(default=False)),
                ('is_suspended', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Packages',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('package_name', models.CharField(max_length=50)),
                ('price', models.DecimalField(decimal_places=2, max_digits=9)),
                ('product_listign_limit', models.BigIntegerField()),
                ('dispaly_image_limit', models.BigIntegerField()),
                ('benifits', models.TextField(max_length=200)),
                ('months', models.IntegerField()),
                ('add_limit', models.IntegerField()),
                ('video_limit', models.IntegerField()),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='pkgOrder',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('Razor_order_id', models.CharField(max_length=50)),
                ('Package_id', models.IntegerField()),
                ('amount', models.IntegerField()),
                ('payment_id', models.CharField(blank=True, max_length=50, null=True)),
                ('status', models.CharField(default='pending', max_length=50)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='payment_order', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Products',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('product_name', models.CharField(max_length=50)),
                ('description', models.TextField(max_length=2000)),
                ('image', models.ImageField(upload_to='images/service/')),
                ('lng', models.DecimalField(decimal_places=10, max_digits=20)),
                ('lat', models.DecimalField(decimal_places=10, max_digits=20)),
                ('location', models.CharField(max_length=100)),
                ('is_approved', models.BooleanField(default=False)),
                ('reason', models.CharField(blank=True, max_length=100, null=True)),
                ('is_updated', models.BooleanField(default=False)),
                ('is_rejected', models.BooleanField(default=False)),
                ('update_rejected', models.BooleanField(default=False)),
                ('update_reject_reason', models.CharField(blank=True, max_length=100, null=True)),
                ('is_suspended', models.BooleanField(default=False)),
                ('temp_sts', models.BooleanField(default=False)),
                ('phone', models.BigIntegerField(blank=True, null=True, unique=True, validators=[django.core.validators.MaxValueValidator(9999999999)])),
                ('menu', models.CharField(blank=True, max_length=100, null=True)),
                ('appointments', models.CharField(blank=True, max_length=100, null=True)),
                ('opening_Time', models.TimeField(blank=True, null=True, verbose_name='Conversation Time')),
                ('closing_Time', models.TimeField(blank=True, null=True, verbose_name='Conversation Time')),
                ('address', models.CharField(blank=True, max_length=100, null=True)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='category_products', to='celebrations.category')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=200)),
                ('rating', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(5)])),
                ('reject_reason', models.TextField(blank=True, max_length=300, null=True)),
                ('review', models.TextField(max_length=300)),
                ('approvel_sts', models.IntegerField(choices=[(0, 'pending'), (1, 'Accept'), (2, 'reject')], default=0)),
                ('flag', models.BooleanField(default=False)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviews', to='celebrations.products')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='review_user', to='celebrations.morwagon_users')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('need_approval', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Tax_details',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('tax_name', models.CharField(max_length=50)),
                ('percentage', models.IntegerField(default=0)),
                ('flag', models.BooleanField()),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Units',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('unit', models.CharField(max_length=50)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Verifyemail',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('email', models.EmailField(max_length=254)),
                ('otp', models.IntegerField()),
                ('verify', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Verifymobileupdate',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('mobile', models.BigIntegerField(blank=True, null=True, validators=[django.core.validators.MaxValueValidator(9999999999)])),
                ('otp', models.IntegerField()),
                ('authenticated', models.BooleanField(default=None, null=True)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Videos',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=50)),
                ('thumbnail', models.ImageField(upload_to='images/')),
                ('is_approved', models.BooleanField(default=False)),
                ('Videos', models.FileField(upload_to='files/')),
                ('temp_sts', models.BooleanField(default=False)),
                ('service', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='vendor_video', to='celebrations.products')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Vendor_package',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('recently_created', models.BooleanField(default=False)),
                ('package', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='package_vendor', to='celebrations.packages')),
                ('vendor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='vendor_package', to='celebrations.morwagon_users')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Trending',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('click_count', models.BigIntegerField()),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='trending', to='celebrations.products')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Sub_category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('subcategory_name', models.CharField(max_length=50)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='category', to='celebrations.category')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Review_images',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('images', models.ImageField(upload_to='images/')),
                ('review', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='review_images', to='celebrations.review')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Report_vendor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('reason', models.IntegerField(choices=[(0, 'vendors pricing is incorrect'), (1, 'vendors phone number is incorrect'), (2, 'vendors address/city is incorrect'), (3, 'This vendor has plagiarised images')])),
                ('description', models.TextField()),
                ('flag', models.BooleanField(default=False)),
                ('vendor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_report', to='celebrations.morwagon_users')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='products',
            name='subcategory',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subcategory', to='celebrations.sub_category'),
        ),
        migrations.AddField(
            model_name='products',
            name='vendor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='vendor', to='celebrations.morwagon_users'),
        ),
        migrations.CreateModel(
            name='PriceUnit',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('price', models.IntegerField(blank=True, null=True)),
                ('from_price', models.IntegerField(blank=True, null=True)),
                ('is_fixedprice', models.BooleanField(default=False)),
                ('to_price', models.IntegerField(blank=True, null=True)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='product_unit', to='celebrations.products')),
                ('unit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='units', to='celebrations.units')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Popular',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('ip_address', models.GenericIPAddressField()),
                ('click_count', models.BigIntegerField()),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='celebrations.products')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='placedOrders',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('main_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='order_id_main', to='celebrations.pkgorder')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=9)),
                ('tax_amount', models.DecimalField(decimal_places=2, max_digits=9)),
                ('total_amount', models.DecimalField(decimal_places=2, max_digits=9)),
                ('payment_id', models.CharField(max_length=50)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='payment_usr', to='celebrations.morwagon_users')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('message', models.TextField()),
                ('read', models.BooleanField(default=False)),
                ('recieved_date', models.DateTimeField(auto_now_add=True)),
                ('ulr_typ', models.CharField(max_length=50)),
                ('url_id', models.IntegerField()),
                ('recipient', models.ManyToManyField(to='celebrations.Morwagon_users')),
                ('sender', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sender_notification', to='celebrations.morwagon_users')),
            ],
        ),
        migrations.AddField(
            model_name='morwagon_users',
            name='packages',
            field=models.ForeignKey(blank=True, max_length=100, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='packages', to='celebrations.packages'),
        ),
        migrations.AddField(
            model_name='morwagon_users',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='users', to=settings.AUTH_USER_MODEL),
        ),
        migrations.CreateModel(
            name='Favourites',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='favourites_products', to='celebrations.products')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='favourites_user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Faq',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('question', models.TextField()),
                ('answer', models.TextField()),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='celebrations.products')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Enquiry',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('ip_address', models.GenericIPAddressField()),
                ('click_count', models.BigIntegerField()),
                ('type', models.IntegerField(choices=[(1, 'call'), (2, 'message')])),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='celebrations.products')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=50)),
                ('type', models.CharField(choices=[('product', 'product'), ('service', 'service')], max_length=10)),
                ('image', models.ImageField(upload_to='images/banner')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='celebrations.category')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Album_details',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('image', models.ImageField(upload_to='images/')),
                ('is_approved', models.BooleanField(default=False)),
                ('temp_sts', models.BooleanField(default=False)),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='album_details', to='celebrations.album')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='album',
            name='service',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='vendor_album', to='celebrations.products'),
        ),
        migrations.CreateModel(
            name='Addscount',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('clickcount', models.BigIntegerField(blank=True, null=True)),
                ('adds', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='adds', to='celebrations.adds')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='customer', to='celebrations.morwagon_users')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='adds',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='product', to='celebrations.products'),
        ),
    ]
