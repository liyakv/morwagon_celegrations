from django.contrib import admin
from django.urls import path,include
from celebrations.views import beta



urlpatterns = [
    path('create/', beta.ProductCreation, name='ProductCreation'),
    path('details/', beta.ProductDetails, name='ProductDetails'),
    path('LoadSubCategoryDropdown/', beta.LoadSubCategoryDropdown,name='LoadSubCategoryDropdown'),
    path('EditProduct/', beta.EditProduct, name='EditProduct'),
    path('ViewProduct/', beta.ViewProduct, name='ViewProduct'),
    path('DeleteProduct/', beta.DeleteProduct, name='DeleteProduct'),
    path('Enquiry/', beta.EnquiryView, name='EnquiryView'),
    path('Review/', beta.ApproveReview, name='ApproveReview'),
    path('Video/<int:pid>', beta.VideoUpload, name='VideoUpload'),
    path('dashboard', beta.VendorDashboard, name='VendorDashboard'),
    path('Delete_img/', beta.DeleteImage, name='Delete_img'),
    path('ads/<int:pid>', beta.advertisement_upload, name='advertisement'),
    path('lmtpdt/', beta.LimitProduct, name='LimitProduct'),
    path('NameCheck/', beta.NameCheck, name='NameCheck'),
    path('Albumsave/<int:pid>', beta.Albumsave, name='Albumsave'),
    path('PkgUpdation/', beta.PkgUpdation, name='PkgUpdation'),
    path('AD_view/', beta.AD_view, name='AD_view'),
    path('IdpickAlbum/', beta.IdpickAlbum, name='IdpickAlbum'),
    path('IdpickVideo/', beta.IdpickVideo, name='IdpickVideo'),
    path('PaymentFinal/', beta.PaymentFinal, name='PaymentFinal'),
    path('success' , beta.success , name='success'),
    path('forget/password', beta.forgotpassword , name='forget-password'),
    path('forget', beta.forgotemail , name='forget-email'),
    path('checkotp', beta.checkotp , name='check-otp'),
    path('resetpassword/', beta.resetpassword , name='reset-password'),
    path('success/<int:id>' , beta.success , name='success'), 
    path('FaildHtml/' , beta.FaildHtml , name='FaildHtml'),
    path('html_to_pdf_view/', beta.html_to_pdf_view, name='html_to_pdf_view'),
    path('PkgConform/', beta.PkgConform, name='PkgConform'),
    path('DeselectService/', beta.DeselectService, name='DeselectService'),
    # path('product/details', beta.productdetailsfetch, name='kk'),
    path('RemoveMedia/', beta.RemoveMedia, name='RemoveMedia'),
    path('DisableImg/', beta.DisableImg, name='DisableImg'),
    
    

    path('test-api/', beta.apiTest, name='apiTest'),
    path('units/', beta.createUnits, name='unit'),
    path('edit-unit/', beta.EditUnit, name='edit-unit'),
    path('delete-unit/', beta.DeleteUnit, name='delete-unit'),
    path('get-unit/', beta.getUnit, name='get-unit'),
    path('get-units/', beta.getUnitDropdown, name='get-units'),
    # path('get-number/', beta.getNumber, name='get-number'),
    
    path('changeflag/', beta.Flagchange, name='flagchange'),

    
    


# ////////////////////////////////////////////////////////////// liyaaaa

    path('register_validation',beta.Register_validation,name='register_validation'),
    path('vendor_register/',beta.vendor_register,name='vendor_register'),
    path('profile_vendor/',beta.profile_vendor,name='profile_vendor'),
    path('edit_vendor/<int:vendor_id>/',beta.edit_vendor,name='edit_vendor'),
    path('termsconditions/',beta.terms_conditions_vendor,name='terms_conditions_vendor'),

    path('PkgUpdationnew/<int:id>/', beta.PkgUpdationnew, name='PkgUpdationnew'),
    path('PaymentFinalnew/', beta.PaymentFinalnew, name='PaymentFinalnew'),
    path('successnew/<int:id>' , beta.successnew , name='successnew'),
    path('FaildHtml/' , beta.FaildHtml , name='FaildHtml'),


    path('vendor_personal', beta.vendor_personal, name='vendor_personal'),
    path('vendor_store', beta.vendor_store, name='vendor_store'),
    path('vendor_document', beta.vendor_document, name='vendor_document'),
    path('vendor_package', beta.vendor_package, name='vendor_package'),
    path('mail_verification', beta.mail_verification, name='mail_verification'),
    path('otp_check', beta.otp_check, name='otp_check'),
    path('html_to_pdf_viewnew', beta.html_to_pdf_viewnew, name='html_to_pdf_viewnew'),
    path('vendor_reset_passwordfirst', beta.vendor_reset_passwordfirst, name='vendor_reset_passwordfirst'),
    path('password_verification', beta.password_verification, name='password_verification'),
    path('new_password_change', beta.new_password_change, name='new_password_change'),
    path('Vendorswal/<str:pages>/<str:id>',beta.Vendorswal, name='Vendorswal'),
    path('vendor_logout',beta.vendor_logout, name='vendor_logout'),
    path('report_vendor_customer',beta.report_vendor_customer, name='report_vendor_customer'),
    # path('suspend_report/<int:id>',beta.suspend_report, name='suspend_report'),
    path('flag_report/<int:id>',beta.flag_report, name='flag_report'),

    path('ReviewFilter/', beta.ReviewFilter, name='ReviewFilter'),
    path('test/', beta.test, name='test'),
    path("whatsapp",
         beta.check_whatsapp_new, name='edit_whats'),       

    path('faq/',beta.faqFirstView,name='product-faq'),
    path('ViewFaq/<int:id>/',beta.viewFaqs,name='view-faq'),
    # path('AddFaq/',beta.AddFaq,name='add-faq'),
    path('DeleteFaq/<int:id>/',beta.deleteFaq,name='delete-faq'),
    path('EditFaq/<int:id>/',beta.editFaq,name='edit-faq'),



    
]
  
    
   
  
