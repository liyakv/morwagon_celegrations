from httplib2 import Response
from rest_framework import serializers
from .models import Faq, PriceUnit, Products, Report_vendor, Units
from rest_framework import serializers
from .models import Category, Products,Sub_category,Banner,Popular,Products,Enquiry,Review,Morwagon_users,Review_images,Album_details,Album,Videos,Favourites,Adds,Addscount,Verifymobileupdate
from django.contrib.auth.models import User
from django.db.models import Avg
class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model=Products
        fields = "__all__"
class MorwagonUserforreviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Morwagon_users
        fields = ('first_name','last_name','email','mobile_number')

class MorwagonUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Morwagon_users
        fields = "__all__"

class ProductsReviewSerializer(serializers.ModelSerializer):
    avg_rating = serializers.FloatField()
    class Meta:
        model=Products
        fields = ('product_name','category','subcategory','description','vendor','image','location','lng','lat','is_approved','is_updated','avg_rating')

class CurrentUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Morwagon_users
        # fields = "__all__"
        fields = ('user','first_name','last_name','email','mobile_number','role','is_verified',)
class DefaultUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"
        # fields = ('id', 'first_name', 'last_name')
class Morwagon_usersSerializer(serializers.ModelSerializer): # vendor
    users = DefaultUserSerializer(many=True, read_only=True)
    class Meta:
        model = Morwagon_users
        fields = ('id','user','role','mobile_number', 'whatsapp_number','address1','address2','town','district','state','pincode','incorporation_certificate','GST','pancard','address_proof','packages','account_holder_name','bank_name','branch','ifsc_code','account_number','users')
class Sub_categorySerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source='subcategory_name')
    class Meta:
        model=Sub_category
        fields= ('id', 'category_name', 'category')
class CategorySerializer(serializers.ModelSerializer):
    category = Sub_categorySerializer(many=True, read_only=True)
    class Meta:
        model=Category
        fields = ('id', 'category_name', 'description', 'thumbnail', 'category')
class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model=Banner
        fields = "__all__"
        depth=1
class PopularSerializer(serializers.ModelSerializer):
    class Meta:
        model=Popular
        fields = "__all__"

class EnquirySerializer(serializers.ModelSerializer):
    class Meta:
        model=Enquiry
        fields = "__all__"
class Review_imagesSerializer(serializers.ModelSerializer):
    class Meta:
        model=Review_images
        fields=('images',)
class ReviewupdateSerializer(serializers.ModelSerializer):
    class Meta:
        model=Review
        fields=('id', 'product', 'user', 'title', 'rating','reject_reason','review','approvel_sts',)

class ReviewSerializer(serializers.ModelSerializer):
    review_images = Review_imagesSerializer(many=True, read_only=True)
    user = MorwagonUserforreviewSerializer(read_only=True)
    class Meta:
        model=Review
        fields = ('id', 'product', 'title', 'rating','reject_reason','review','approvel_sts', 'updated_on','update_date','review_images','user')
        
class ReviewuserSerializer(serializers.ModelSerializer):
    review_images = Review_imagesSerializer(many=True, read_only=True)
    class Meta:
        model=Review
        fields = ('id', 'product','user', 'title', 'rating','reject_reason','review','approvel_sts', 'updated_on','update_date','review_images')
class AlbumimageSerializer(serializers.ModelSerializer):
    class Meta:
        model=Album_details
        fields="__all__"
class AlbumSerializer(serializers.ModelSerializer):
    album_details = AlbumimageSerializer(many=True, read_only=True)
    class Meta:
        model=Album
        fields = ('id', 'album_name',  'service','album_details')
# class AddimageSerializer(seReview_imagesrializers.ModelSerializer):
#     class Meta:
#         model=Addimage
#         fields="__all__"
class AddsSerializer(serializers.ModelSerializer):
    class Meta:
        model=Adds
        fields = "__all__"
        depth=2
class VideosSerializer(serializers.ModelSerializer):
    class Meta:
        model=Videos
        fields="__all__"
# class FavouritesSerializer(serializers.ModelSerializer):
#     class Meta:
#         model=Favourites
#         fields="__all__"
class FavouritesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Favourites
        fields = ('user', 'product','favourites_products')
    favourites_products = serializers.SerializerMethodField('get_favourites_products')
    def get_favourites_products(self, obj):
        return {"product_id":obj.product.id,"product_name":obj.product.product_name,"category":obj.product.category.category_name
        ,"subcategory":obj.product.subcategory.subcategory_name,"description":obj.product.description
        ,"vendor":obj.product.vendor.first_name,"image":obj.product.image.url,"location":obj.product.location
        ,"lng":obj.product.lng,"lat":obj.product.lat,"is_approved":obj.product.is_approved}

# ProductsReviewSerializer
class ProductsfavouriteSerializer(serializers.ModelSerializer):
    is_in_wishlist = serializers.SerializerMethodField('check_wishlist')
    # avg_rating = serializers.FloatField()

    def check_wishlist(self,product):
        user_id = self.context.get("user_id")
        
        products_in_wishlist = Favourites.objects.filter(user=user_id,product=product.id)
        if len(products_in_wishlist)>0:
            return (True)
        else:
            return (False)
    class Meta:
        model=Products
        fields = ('id', 'product_name', 'category', 'subcategory', 'description', 'vendor','image','lng', 'lat', 'location','is_in_wishlist')
        depth = 1


class AddscountSerializer(serializers.ModelSerializer):
    class Meta:
        model=Addscount
        fields = "__all__"

# class UnitsSerializer(serializers.ModelSerializer):
#     class Meta:
#         model=Units
#         fields = "__all__"

class PriceUnitSerializer(serializers.ModelSerializer):
    # units = UnitsSerializer(many=True, read_only=True)
    class Meta:
        model=PriceUnit
        fields = ('id','price','unit','from_price','to_price','product')
        depth=2

class ProductsearchSerializer(serializers.ModelSerializer):
    is_in_wishlist = serializers.SerializerMethodField('check_wishlist')
    avg_rating = serializers.FloatField()
    priceunits = serializers.SerializerMethodField('price_perunit')


    def check_wishlist(self,product):
        user_id = self.context.get("user_id")
        print(user_id,".........................................................ProductsearchSerializerProductsearchSerializer")
        
        products_in_wishlist = Favourites.objects.filter(user=user_id,product=product.id)
        print(products_in_wishlist,"...............................................pppppppppppppppppppppppppppppppppppp")
        if len(products_in_wishlist)>0:
            return (True)
        else:
            return (False)
    def price_perunit(self,product):
        print(".........................................................price_perunitprice_perunitprice_perunitprice_perunit")        
        price_unit = PriceUnit.objects.filter(product=product.id)
        print(price_unit,"...............................................oooooooooooooooooooooooooooooooooo")
        if len(price_unit)>0:
            serializer=PriceUnitSerializer(price_unit, many=True)
            return serializer.data
        else:
            return price_unit
    class Meta:
        model=Products
        fields = ('id', 'product_name', 'category', 'subcategory', 'description', 'vendor','image','lng', 'lat', 'location','phone','menu','appointments','opening_Time','closing_Time','address','is_in_wishlist','avg_rating','priceunits')
        depth = 1



class ProductsearchSortSerializer(serializers.ModelSerializer):
    is_in_wishlist = serializers.SerializerMethodField('check_wishlist')
    # avg_rating = serializers.FloatField()
    avg_rating = serializers.SerializerMethodField('average_rating')
    priceunits = serializers.SerializerMethodField('price_perunit')
    image = serializers.SerializerMethodField('pro_images')

    def check_wishlist(self,product):
        user_id = self.context.get("user_id")
        
        products_in_wishlist = Favourites.objects.filter(user=user_id,product=product['id'])

        if len(products_in_wishlist)>0:
            return (True)
        else:
            return (False)
    def price_perunit(self,product):
      
        price_unit = PriceUnit.objects.filter(product=product['id'])

        if len(price_unit)>0:
            serializer=PriceUnitSerializer(price_unit, many=True)
            return serializer.data
        else:
            return price_unit
    def average_rating(self,product):
        # print(product['image'],"++++++++++++++++++++++++++++++++++++++++++++++++")
        try:
            serializer = Products.objects.filter(id=product['id']).annotate(avg_rating=Avg('reviews__rating'))
            return serializer[0].avg_rating
        except Exception as e:
            return str(e)
    def pro_images(self,product):
        print(".........................................................price_perunitprice_perunitprice_perunitprice_perunit")        
        img = str(product['image'])
        return ("/media/"+img)
    class Meta:
        model=Products
        fields = ('id', 'product_name', 'category', 'subcategory', 'description', 'vendor','image','lng', 'lat', 'location','phone','menu','appointments','opening_Time','closing_Time','address','is_in_wishlist','avg_rating','priceunits')
        depth = 1


class MorwagonReviewSerializer(serializers.ModelSerializer):
    avg_rating = serializers.FloatField()
    class Meta:
        model=Morwagon_users
        fields = ('id', 'store_name', 'location', 'address1', 'address2','logo_image','thumbnail','Description','caption','avg_rating')


class VerifymobileupdateSerializer(serializers.ModelSerializer):
    class Meta:
        model=Verifymobileupdate
        fields = "__all__"

class Report_vendorSerializer(serializers.ModelSerializer):
    class Meta:
        model=Report_vendor
        fields = "__all__"

class MorwagonduplicheckSerializer(serializers.ModelSerializer):
    class Meta:
        model=Morwagon_users
        fields = ('first_name','last_name','email','mobile_number')
        
class PhoneUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Morwagon_users
        # fields = "__all__"
        fields = ('user','first_name','last_name','mobile_number','role','is_verified',)

class FaqSerializer(serializers.ModelSerializer):
    class Meta:
        model=Faq
        fields = "__all__"



