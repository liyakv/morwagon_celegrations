# from tkinter import Menu
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from ckeditor.fields import RichTextField
from django.db.models.deletion import CASCADE
from django.db.models.fields import BooleanField, TextField
from django.db.models import Avg, base
from django.db.models.fields import CharField
from django.db import models
from django.db.models import Avg

TIME_ZONE = 'UTC'
USE_TZ = True

# Create your models here.

min = 0
max = 5


class BaseModel(models.Model):
    """Model for subclassing."""
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True
        ordering = ['-created_on']


class Packages(BaseModel):
    package_name = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    product_listign_limit = models.BigIntegerField()
    dispaly_image_limit = models.BigIntegerField()
    benifits = TextField(max_length=200)
    months = models.IntegerField()
    add_limit = models.IntegerField()
    video_limit = models.IntegerField()


SELECTROLE = ((1, "admin"), (2, "vendor"), (3, "user"))


class Morwagon_users(BaseModel):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='users')
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(
        max_length=254, blank=True, null=True, unique=True)
    mobile_number = models.BigIntegerField(
        unique=True, validators=[MaxValueValidator(9999999999)], null=True, blank=True)
    town = models.CharField(max_length=100, blank=True, null=True)
    role = models.IntegerField(choices=SELECTROLE)
    is_rejected = models.BooleanField(default=False)
    reject_reason = models.TextField(max_length=2000, blank=True, null=True)
    expirydate = models.DateTimeField(blank=True, null=True)
    is_updated = models.BooleanField(default=False)
    update_rejected = models.BooleanField(default=False)
    update_reject_reasson = models.CharField(
        max_length=200, blank=True, null=True)
    # vendor
    whatsapp_number = models.BigIntegerField(
        unique=True, validators=[MaxValueValidator(9999999999)], blank=True, null=True)
    address1 = models.TextField(blank=True, null=True)
    address2 = models.TextField(blank=True, null=True)
    district = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    pincode = models.IntegerField(blank=True, null=True)
    incorporation_certificate = models.FileField(
        upload_to="files/", max_length=100, blank=True, null=True)
    GST = models.FileField(
        upload_to="files/", max_length=100, blank=True, null=True)
    pancard = models.FileField(
        upload_to="files/", max_length=100, blank=True, null=True)
    address_proof = models.FileField(
        upload_to="files/", max_length=100, blank=True, null=True)
    packages = models.ForeignKey(Packages, on_delete=models.CASCADE,
                                 related_name='packages', max_length=100, blank=True, null=True)

    location = models.CharField(max_length=200, blank=True, null=True)
    logo_image = models.ImageField(
        upload_to="images/", max_length=100, blank=True, null=True)
    thumbnail = models.ImageField(
        upload_to="images/", max_length=100, blank=True, null=True)
    caption = models.CharField(max_length=200, blank=True, null=True)
    Description = models.TextField(blank=True, null=True)
    is_verified = models.BooleanField(default=False)
    latitude = models.DecimalField(
        max_digits=20, decimal_places=10, blank=True, null=True)
    logitude = models.DecimalField(
        max_digits=20, decimal_places=10, blank=True, null=True)
    gst_number = models.CharField(max_length=100, blank=True, null=True)
    store_name = models.CharField(max_length=200, blank=True, null=True)
    page_no = models.IntegerField(default=0)
    is_done = models.BooleanField(default=False)
    approval = models.BooleanField(default=False)
    is_suspended = models.BooleanField(default=False)

    def vendor_package(self):
        get_vendor_package = Vendor_package.objects.filter(vendor=self)
        return get_vendor_package

    def product_attribute(self):
        get_product = Products.objects.filter(vendor=self, is_deleted=False)
        return get_product

    def enquiry_attribute(self):
        get_enquiry = Enquiry.objects.filter(
            product__vendor=self, is_deleted=False, product__is_deleted=False).order_by('-updated_on')[:5]
        return get_enquiry

    def enquiryC_attribute(self):
        get_enquiryC = Enquiry.objects.filter(
            product__vendor=self, is_deleted=False, product__is_deleted=False)
        return get_enquiryC

    def review_attribute(self):
        get_review = Review.objects.filter(product__vendor=self, is_deleted=False,
                                           product__is_deleted=False, approvel_sts=True).order_by('-updated_on')[:5]
        print(get_review, ".........get_reviewget_review")
        return get_review

    def reviewC_attribute(self):
        getC_review = Review.objects.filter(
            product__vendor=self, is_deleted=False, product__is_deleted=False, approvel_sts=True)
        return getC_review

    def adds_attribute(self):
        get_add = Adds.objects.filter(
            product__vendor=self, is_deleted=False, product__is_deleted=False)
        return get_add


class Category(BaseModel):
    category_name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    thumbnail = models.ImageField(upload_to='images/category')

    def subcategory_attribute(self):
        subcategory_attributes = Sub_category.objects.filter(
            category=self, is_deleted=False)
        return subcategory_attributes


class Sub_category(BaseModel):
    subcategory_name = models.CharField(max_length=50)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name='category')


# SELECTUNIT = ((1, "Per day"), (2, "Per hour"), (3, "Per person"), (4, "Per unit"), (5, "Per pair"))
class Units(BaseModel):
    unit = models.CharField(max_length=50)

class Products(BaseModel):
    product_name = models.CharField(max_length=50)
    # price = models.DecimalField(max_digits=9, decimal_places=2)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='category_products')
    subcategory = models.ForeignKey(Sub_category, on_delete=models.CASCADE, related_name='subcategory')
    description = models.TextField(max_length=2000)
    # unit = models.IntegerField(choices=SELECTUNIT)
    vendor = models.ForeignKey(Morwagon_users, on_delete=models.CASCADE, related_name='vendor')
    image = models.ImageField(upload_to='images/service/')
    lng = models.DecimalField(max_digits=20, decimal_places=10)
    lat = models.DecimalField(max_digits=20, decimal_places=10)
    location = models.CharField(max_length=100)
    is_approved = models.BooleanField(default=False)
    reason = models.CharField(max_length=100, null=True, blank=True)
    is_updated = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    update_rejected = models.BooleanField(default=False)
    update_reject_reason = models.CharField(max_length=100, null=True, blank=True)
    is_suspended = models.BooleanField(default=False)
    temp_sts = models.BooleanField(default=False)
    phone=models.BigIntegerField(validators=[MaxValueValidator(9999999999)], null=True, blank=True)
    menu= models.CharField(max_length=100, null=True, blank=True)
    appointments= models.CharField(max_length=100, null=True, blank=True)
    # opening_Time = models.TimeField((u"Conversation Time"), blank=True, null=True)
    # closing_Time =models.TimeField((u"Conversation Time"), blank=True, null=True)
    opening_Time = models.CharField(max_length=100,blank=True, null=True)
    closing_Time =models.CharField(max_length=100,blank=True, null=True)
    address=models.CharField(max_length=100, null=True, blank=True)
    

    def __str__(self):
        return self.product_name
    def add_image(self):
        addimages = Adds.objects.filter(product=self, is_deleted=False)
        return addimages

    def ad_attribute(self):
        get_ads = Adds.objects.filter(product=self, is_deleted=False)
        return get_ads

class PriceUnit(BaseModel):
    price = models.IntegerField(blank=True, null=True)
    unit = models.ForeignKey(Units, on_delete=models.CASCADE, related_name='units')
    from_price = models.IntegerField(blank=True, null=True)
    is_fixedprice=models.BooleanField(default=False)
    to_price = models.IntegerField(blank=True, null=True)
    product = models.ForeignKey(Products, on_delete=models.CASCADE, related_name='product_unit')


# class PriceUnit(BaseModel):
#     price = models.IntegerField(blank=True, null=True)
#     unit = models.ForeignKey(Units, on_delete=models.CASCADE, related_name='units')
#     from_price = models.IntegerField(blank=True, null=True)
#     is_fixedprice=models.BooleanField(default=False)
#     to_price = models.IntegerField(blank=True, null=True)
#     product = models.ForeignKey(Products, on_delete=models.CASCADE, related_name='product_unit')


# class PriceUnit(BaseModel):
#     price = models.IntegerField(blank=True, null=True)
#     unit = models.IntegerField()
#     from_price = models.IntegerField(blank=True, null=True)
#     to_price = models.IntegerField(blank=True, null=True)
#     product = models.ForeignKey( Products, on_delete=models.CASCADE, related_name='product_unit')


SELECTSTATUS = ((0, "pending"), (1, "Accept"), (2, "reject"))


class Review(BaseModel):
    product = models.ForeignKey(
        Products, on_delete=models.CASCADE, related_name='reviews')
    user = models.ForeignKey(
        Morwagon_users, on_delete=models.CASCADE, related_name='review_user')
    title = models.CharField(max_length=200)
    rating = models.IntegerField(
        default=1, validators=[MinValueValidator(1), MaxValueValidator(5)]
    )
    reject_reason = models.TextField(max_length=300, null=True, blank=True)
    review = models.TextField(max_length=300)
    approvel_sts = models.IntegerField(choices=SELECTSTATUS, default=0)
    flag=models.BooleanField(default=False)
    update_date=models.DateField()
    

    def reviewimages_attribute(self):
        get_review_images = Review_images.objects.filter(
            review=self, is_deleted=False)
        return get_review_images


class Review_images(BaseModel):
    review = models.ForeignKey(Review, on_delete=models.CASCADE,related_name="review_images")
    images = models.ImageField(upload_to="images/")


class Album(BaseModel):
    album_name = models.CharField(max_length=50)
    service = models.ForeignKey(Products, on_delete=models.CASCADE, related_name='vendor_album')

    def album_attribute(self):
        get_album = Album_details.objects.filter(album=self, is_deleted=False)
        return get_album


class Album_details(BaseModel):
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='album_details')
    image = models.ImageField(upload_to='images/')
    is_approved = models.BooleanField(default=False)
    temp_sts = models.BooleanField(default=False)


class Videos(BaseModel):
    name = models.CharField(max_length=50)
    service = models.ForeignKey(
        Products, on_delete=models.CASCADE, related_name='vendor_video')
    thumbnail = models.ImageField(upload_to='images/')
    is_approved = models.BooleanField(default=False)
    Videos = models.FileField(upload_to='files/')
    temp_sts = models.BooleanField(default=False)


class Favourites(BaseModel):  # change morwagon  users
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='favourites_user')
    product = models.ForeignKey(
        Products, on_delete=models.CASCADE, related_name='favourites_products')


class Popular(BaseModel):
    product = models.ForeignKey(Products, on_delete=models.CASCADE,related_name="product_popular")
    ip_address = models.GenericIPAddressField()
    click_count = models.BigIntegerField()


typ_choices = (("product", "product"), ("service", "service"))


class Banner(BaseModel):
    name = models.CharField(max_length=50)
    type = models.CharField(max_length=10, choices=typ_choices)
    image = models.ImageField(upload_to='images/banner')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)


SELECTTYPE = ((1, "call"), (2, "message"))


class Enquiry(BaseModel):
    product = models.ForeignKey(Products, on_delete=models.CASCADE,related_name="product_equiry")
    ip_address = models.GenericIPAddressField()
    click_count = models.BigIntegerField()
    type = models.IntegerField(choices=SELECTTYPE)


# SELECTTYPE = ((1, "call"), (2, "message"))


# class Adds(BaseModel):
#     product = models.ForeignKey(
#         Products, on_delete=models.CASCADE, related_name="adds")
#     expirytime = models.DateTimeField(null=True, blank=True)
#     clickcount = models.BigIntegerField(null=True, blank=True)
#     type = models.IntegerField(choices=SELECTTYPE, default=False)
#     is_approved = models.BooleanField(default=False)
#     image = models.ImageField(upload_to='images/adds')
#     reject_reason = models.CharField(max_length=100, null=True, blank=True)

class Adds(BaseModel):
    product = models.ForeignKey(Products, on_delete=models.CASCADE, related_name="product")
    image = models.ImageField(upload_to='images/adds')
    is_approved = models.BooleanField(default=False)
    reject_reason = models.CharField(max_length=100, null=True, blank=True)
    temp_sts = models.BooleanField(default=False)


class Addscount(BaseModel):
    customer = models.ForeignKey(Morwagon_users, on_delete=models.CASCADE, related_name='customer')
    # category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='category')
    adds = models.ForeignKey(Adds, on_delete=models.CASCADE, related_name='adds')
    clickcount = models.BigIntegerField(null=True, blank=True)

class Verifyemail(BaseModel):
    email = models.EmailField()
    otp = models.IntegerField()
    verify = models.BooleanField(default=False)


class Vendor_package(BaseModel):
    vendor = models.ForeignKey(
        Morwagon_users, on_delete=CASCADE, related_name='vendor_package')
    package = models.ForeignKey(
        Packages, on_delete=CASCADE, related_name='package_vendor')
    recently_created = models.BooleanField(default=False)


class Payment(BaseModel):
    user = models.ForeignKey(
        Morwagon_users, on_delete=models.CASCADE, related_name='payment_usr')
    amount = models.DecimalField(max_digits=9, decimal_places=2)
    tax_amount = models.DecimalField(max_digits=9, decimal_places=2)
    total_amount = models.DecimalField(max_digits=9, decimal_places=2)
    payment_id = models.CharField(max_length=50)


class Tax_details(BaseModel):
    tax_name = models.CharField(max_length=50)
    percentage = models.IntegerField(default=0)
    flag = models.BooleanField()


class pkgOrder(BaseModel):
    Razor_order_id = models.CharField(max_length=50)
    Package_id = models.IntegerField()
    amount = models.IntegerField()
    payment_id = models.CharField(max_length=50, null=True, blank=True)
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='payment_order')
    status = models.CharField(default="pending", max_length=50)


class placedOrders(BaseModel):
    main_id = models.ForeignKey(
        pkgOrder, on_delete=models.CASCADE, related_name='order_id_main')


class Trending(BaseModel):
    product = models.ForeignKey(
        Products, on_delete=CASCADE, related_name="trending")
    click_count = models.BigIntegerField()


class Settings(models.Model):
    name = models.CharField(max_length=50)
    need_approval = models.BooleanField(default=False)


class Forgetpassword(BaseModel):
    email = models.EmailField()
    otp = models.IntegerField()


class Notification(models.Model):
    sender = models.ForeignKey(
        Morwagon_users, on_delete=models.CASCADE, null=True, related_name='sender_notification')
    recipient = models.ManyToManyField(Morwagon_users)
    title = models.CharField(max_length=100)
    message = models.TextField()
    read = models.BooleanField(default=False)
    recieved_date = models.DateTimeField(auto_now_add=True)
    ulr_typ = models.CharField(max_length=50)
    url_id = models.IntegerField()


class Verifymobileupdate(BaseModel):
    mobile = models.BigIntegerField(validators=[MaxValueValidator(9999999999)], null=True, blank=True)
    otp = models.IntegerField()
    authenticated = models.BooleanField(null=True, blank=False,default=None)


SELECTREASON = ((0, "vendors pricing is incorrect"), 
                (1, "vendors phone number is incorrect"), 
                (2, "vendors address/city is incorrect"),
                (3, "This vendor has plagiarised images"))
class Report_vendor(BaseModel):
    vendor = models.ForeignKey(Morwagon_users, on_delete=models.CASCADE, related_name='user_report')
    reason = models.IntegerField(choices=SELECTREASON)
    description = models.TextField()
    flag = models.BooleanField(default=False)


class Faq(BaseModel):
    product=models.ForeignKey(Products, on_delete=models.CASCADE)
    question=models.TextField()
    answer=models.TextField()
