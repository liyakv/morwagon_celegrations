from itertools import product
# from math import prod
from django.contrib.auth.models import User
from django.db.models.expressions import OuterRef
from django.shortcuts import render,redirect,HttpResponse
from razorpay import Order
# from celebrations.authentication import FirebaseAuthentication
from celebrations.models import Category, Faq, PriceUnit, Products,Sub_category,Banner,Popular,Enquiry,Review,Morwagon_users,Review_images,Album,Videos,Favourites,Adds,Addscount,Album_details,Settings,Verifymobileupdate,Report_vendor
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from celebrations.serializers import CategorySerializer, FaqSerializer, MorwagonduplicheckSerializer, PhoneUserSerializer,Sub_categorySerializer,BannerSerializer,ProductsSerializer,PopularSerializer,EnquirySerializer,ReviewSerializer,CurrentUserSerializer,AlbumSerializer,VideosSerializer,Morwagon_usersSerializer,FavouritesSerializer,ReviewupdateSerializer,AddsSerializer,ProductsReviewSerializer,ProductsfavouriteSerializer,MorwagonUserSerializer, Review_imagesSerializer,ReviewuserSerializer,ProductsearchSerializer,AddscountSerializer,MorwagonReviewSerializer,AlbumimageSerializer,VerifymobileupdateSerializer,DefaultUserSerializer,Report_vendorSerializer, ProductsearchSortSerializer
from django.db.models import Q
from django.db.models import Count
from django.db import connection
from django.db.models import OuterRef,Count,Sum,Avg
from django.http import QueryDict
from rest_framework.parsers import MultiPartParser
from django.db.models import Q
from urllib.parse import urlparse
from django.db.models import Max

from rest_framework_jwt.settings import api_settings
# from rest.apps.user.models import User
from django.contrib.auth import authenticate
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from django.contrib.auth.decorators import user_passes_test
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from datetime import datetime,timedelta,date
from geopy.geocoders import Nominatim
import os
from django.core.mail import EmailMessage
from django.contrib.auth.tokens import default_token_generator
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from morwagonproject.settings import NO_PRODUCTS_PER_PAGE
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
import random
from random import randint
from django.utils.timezone import utc
from django.core.mail import send_mail

JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER
# authe = FirebaseAuthentication.authenticate()
def allow_all(self):
    return True

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
# @permission_classes([AllowAny])
# class registermodel(APIView):
#     def post(self,request):
#         print(request.data)
#         first_name=request.data["first_name"]
#         last_name=request.data["last_name"]
#         email=request.data["email"]
#         password=request.data["password"]
#         emailexist=User.objects.filter(email=email).exists()
#         if emailexist is False:
#             userrecord=User.objects.create_user(first_name=first_name,last_name=last_name,email=email,username=email,password=password,is_staff=1)
#             id=userrecord.id
#             print(id,"iddddddddddddddd")

#             mobile_number=request.data["mobile_number"]
#             ordinary_dict = {'user': id, 'first_name':first_name,'last_name':last_name,'email':email,'mobile_number': mobile_number,'role': 3}
#             query_dict = QueryDict('', mutable=True)
#             query_dict.update(ordinary_dict)
#             print(first_name,"passwordddddddddd")
#             userdetails=CurrentUserSerializer(data=query_dict)
#             if userdetails.is_valid():
#                 userdetails.save()
#                 print("kkkkkkkkkkkkkkkkkkkkkkkkkkkkk") 
#                 token = default_token_generator.make_token(userrecord) 
#                 print(token,"tokennnnnnnnnnnnnnnn") 
#                 current_site = "http://localhost:3000/services/verify/"
#                 mail_subject = 'Verify your account' 
#                 to_email = email
#                 message = 'hi, please verify your account by clicking below provided link  <a href="'+current_site+str(id)+'/'+token+'"> click here to verfiy</a>'
#                 send_email = EmailMessage(mail_subject, message, to=[to_email])
#                 send_email.content_subtype = 'html'
#                 send_email.send()    
#                 return Response(userdetails.data) 
#             else:
#                 userrecord.delete()
#                 return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"Invalid user"})
#         else:
#             return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"Email already exist"})


def random_with_N_digits(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return randint(range_start, range_end)

@permission_classes([AllowAny])
class registermodel(APIView):
    def post(self,request):
        print(request.data)
        first_name=request.data["first_name"]
        last_name=request.data["last_name"]
        email=request.data["email"]
        password=request.data["password"]
        # mobile_number=request.data["mobile_number"]
        emailexist=User.objects.filter(email=email).exists()
        if emailexist is False:
            # mobileexist=Morwagon_users.objects.filter(mobile_number=mobile_number).exists()
            # if mobileexist is False:
            userrecord=User.objects.create_user(first_name=first_name,last_name=last_name,email=email,username=email,password=password,is_staff=1)
            id=userrecord.id
            ordinary_dict = {'user': id, 'first_name':first_name,'last_name':last_name,'email':email,'role': 3}
            query_dict = QueryDict('', mutable=True)
            query_dict.update(ordinary_dict)
            print(query_dict,"passwordddddddddd")
            userdetails=CurrentUserSerializer(data=query_dict)
            userdetails.is_valid()
            if userdetails.is_valid():
                userdetails.save()
                token = default_token_generator.make_token(userrecord) 
                current_site = "http://localhost:3000/services/verify/"
                mail_subject = 'Verify your account' 
                to_email = email
                # message = 'hi, please verify your account by clicking below provided link  <a href="'+current_site+str(id)+'/'+token+'"> click here to verfiy</a>'
                message = Mail(
                        from_email='liyakv11@gmail.com',
                        to_emails=email,
                        subject='Morwagon user verification',
                        html_content='hi, please verify your account by clicking below provided link  <a href="'+current_site+str(id)+'/'+token+'"> click here to verfiy</a>')
                try:
                    sg = SendGridAPIClient(os.environ.get('SG.itqsL6TtS0iwyxTXfGtw0Q.lKtCeyvdHfAXvsW59dqU8TfQPiQq457Wla4X0sp044s'))
                    response = sg.send(message)
                    print(response.status_code)
                    print(response.body)
                    print(response.headers)
                except Exception as e:
                    print(e)

                return Response(userdetails.data, status=status.HTTP_200_OK) 

            else:
                userrecord.delete()
                return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"Invalid user"})
            # else:
            #     return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"Mobile already exist"})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"Email already exist"})


@permission_classes([AllowAny])
class VerifiedUsermodel(APIView):
    def get(self,request,email):
        isuserverified=Morwagon_users.objects.get(email=email)
        print(isuserverified.is_verified,"trueeeeeeeee")
        if isuserverified.is_verified:
            return Response(status=status.HTTP_208_ALREADY_REPORTED,data={"message":"Already this mail is verified"})
        else:            
            isuserverified.is_verified=True
            isuserverified.save()
            userexist=User.objects.filter(email=email).exists()
            userdetails=User.objects.get(email=email)
            if userexist is True:
                payload = JWT_PAYLOAD_HANDLER(userdetails)
                jwt_token = JWT_ENCODE_HANDLER(payload)
                userid=payload['user_id']
                print(jwt_token,"jjjjjj")
                first_name = userdetails.first_name
                last_name = userdetails.last_name
                payload["first_name"] = first_name
                payload["last_name"] = last_name
                ordinary_dict = {'jwt_token': jwt_token, 'user_details': payload, 'message':"successfully verified user"}
                query_dict = QueryDict('', mutable=True)
                query_dict.update(query_dict)
                return Response(ordinary_dict) 
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"invalid credentials"})

    


@permission_classes([AllowAny])
class Loginmodel(APIView):
    def post(self,request):
        print(request.data)
        email=request.data["email"]
        password=request.data["password"]
        emailexist=Morwagon_users.objects.filter(email=email,is_verified=True).exists()
        if emailexist is True:
            user = authenticate(username=email, password=password)
            print(user)
            if user is not None:
                payload = JWT_PAYLOAD_HANDLER(user)
                jwt_token = JWT_ENCODE_HANDLER(payload)
                userid=payload['user_id']
                print(jwt_token,"jjjjjj")
                userdetails=User.objects.get(id=userid)
                first_name = userdetails.first_name
                last_name = userdetails.last_name
                payload["first_name"] = first_name
                payload["last_name"] = last_name
                morwagon_user = Morwagon_users.objects.get(user=userid)
                payload["mobile"] = morwagon_user.mobile_number
                ordinary_dict = {'jwt_token': jwt_token, 'user_details': payload}
                query_dict = QueryDict('', mutable=True)
                query_dict.update(query_dict)
                return Response(ordinary_dict)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"invalid credentials"})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"invalid credentials"})

    

    
@permission_classes([AllowAny])
class registersocialmediamodel(APIView):
    def post(self,request):
        print(request.data)
        first_name=request.data["first_name"]
        last_name=request.data["last_name"]
        email=request.data["email"]
        password="hacker@89"
        emailexist=User.objects.filter(email=email).exists()
        print(emailexist,"falseeeeeeeeeeeeeeeeeee")
        if emailexist is False:
            userrecord=User.objects.create_user(first_name=first_name,last_name=last_name,email=email,username=email,password=password,is_staff=1)
            id=userrecord.id
            print(id,"iddddddddddddddd")
            ordinary_dict = {'user': id, 'first_name':first_name,'last_name':last_name,'email':email,'role': 3,'is_verified':True}
            query_dict = QueryDict('', mutable=True)
            query_dict.update(ordinary_dict)
            print(first_name,"passwordddddddddd")
            userdetails=CurrentUserSerializer(data=query_dict)
            if userdetails.is_valid():
                print(userdetails.errors,"errorsssssssssss")
                userdetails.save() 
                user = authenticate(username=email, password=password)
                if user is not None: 
                    payload = JWT_PAYLOAD_HANDLER(user)
                    jwt_token = JWT_ENCODE_HANDLER(payload)
                    userid=payload['user_id']
                    print(jwt_token,"jjjjjj")
                    userdetails=User.objects.get(id=userid)
                    first_name = userdetails.first_name
                    last_name = userdetails.last_name
                    payload["first_name"] = first_name
                    payload["last_name"] = last_name
                    ordinary_dict = {'jwt_token': jwt_token, 'user_details': payload, 'message':"successfully created user"}
                    query_dict = QueryDict('', mutable=True)
                    query_dict.update(query_dict)
                    return Response(ordinary_dict) 
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"invalid credentials"})          
            else:
                print(userdetails.errors)
                userrecord.delete()
                return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"invalid credentials"})       
                
        else:
            user = User.objects.get(email=email)
            payload = JWT_PAYLOAD_HANDLER(user)
            jwt_token = JWT_ENCODE_HANDLER(payload)
            userid=payload['user_id']
            print(jwt_token,"jjjjjj")
            userdetails=User.objects.get(id=userid)
            first_name = userdetails.first_name
            last_name = userdetails.last_name
            payload["first_name"] = first_name
            payload["last_name"] = last_name
            ordinary_dict = {'jwt_token': jwt_token, 'user_details': payload, 'message':"successfully logged in"}
            query_dict = QueryDict('', mutable=True)
            query_dict.update(query_dict)
            return Response(ordinary_dict)


@permission_classes([AllowAny])
class forgetpassword(APIView):
    def post(self,request):
        email=request.data["email"]
        emailexist=User.objects.filter(email=email).exists()
        if emailexist:
            userupdate=User.objects.get(email=email)
            print("jbjhbjhbk",request.data["password"])
            newpassword=request.data["password"]
            userupdate.set_password(newpassword) 
            userupdate.save()      
            serializer=DefaultUserSerializer(userupdate)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


@permission_classes([AllowAny])
class categorymodel(APIView):
    def get(self,request):   
        categorydetail=Category.objects.filter(is_deleted=False)
        serializer=CategorySerializer(categorydetail,many=True)
        return Response(serializer.data)

@permission_classes([AllowAny])
class categorybyidmodel(APIView):
    def get(self,request,id):      
        categorydetail=Category.objects.get(id=id)
        serializer=CategorySerializer(categorydetail)
        return Response(serializer.data)

@permission_classes([AllowAny])
class Sub_categorymodel(APIView): 
    def get(self,request):      #for category and subcategory get method
        Sub_categorydetail=Sub_category.objects.filter(is_deleted=False)
        serializer=Sub_categorySerializer(Sub_categorydetail,many=True)
        return Response(serializer.data)

@permission_classes([AllowAny])
class Sub_categorybyidmodel(APIView): 
    def get(self,request,id):      #for category and subcategory by id get method
        id=id
        Sub_categorydetail=Sub_category.objects.get(id=id)
        serializer=Sub_categorySerializer(Sub_categorydetail)
        return Response(serializer.data)

@permission_classes([AllowAny])
class Bannermodel(APIView):
    def get(self,request):   #for banner get method
        Bannerdetail=Banner.objects.filter(is_deleted=False)[0:10]
        serializer=BannerSerializer(Bannerdetail,many=True)
        return Response(serializer.data)

# @permission_classes([AllowAny])
# class Productmodel(APIView):
#     def get(self,request):    #for product get method
#         productdetail=Products.objects.filter(is_approved=True,is_deleted=False,vendor__expirydate__gte=datetime.now())[0:10]
#         paginator = Paginator(productdetail, NO_PRODUCTS_PER_PAGE)
#         import math
#         no_of_pages = 0
#         if len(productdetail) > 0 and NO_PRODUCTS_PER_PAGE > 0:
#             no_of_pages = math.ceil(len(productdetail) / NO_PRODUCTS_PER_PAGE)

#         page = 1
#         if 'page' in request.GET:
#             try:
#                 page = int(request.GET['page'])
#             except:
#                 return Response(productdetail.errors,status=status.HTTP_400_BAD_REQUEST)
#         if no_of_pages >= page:
#             paged_products = paginator.get_page(page)
#             # serializer = ProductsallSerializer(paged_products, many=True, context={'user_id': request.user.id})
#             serializer=ProductsSerializer(paged_products,many=True)

#             response_context = {

#                     "products": serializer.data,
#                     "no_of_pages": no_of_pages,
#                     "current_page": int(page),
                
#             }
#         else:
#             response_context = {
#                     "products": [],
#                     "no_of_pages": no_of_pages,
#                     "current_page": int(page),
                
#             }

#         return Response(response_context, status=status.HTTP_200_OK)
        # serializer=ProductsSerializer(productdetail,many=True)
        # return Response(serializer.data)

@permission_classes([AllowAny])
class Productbyidmodel(APIView):
    def get(self,request ,id):      #for product get by id method
        productdetail=Products.objects.get(id=id,is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False)
        # serializer=ProductsfavouriteSerializer(productdetail)
        serializer = ProductsfavouriteSerializer(productdetail, context={'user_id': request.user.id})
        return Response(serializer.data)

@permission_classes([AllowAny])
class Productbyname(APIView):        
    def get(self,request,name):     # product get by name
        prod=Products.objects.filter(product_name=name,is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False)
        product=prod.annotate(avg_rating=Avg('reviews__rating')).first()
        serializer=ProductsearchSerializer(product, many=False, context={'user_id': request.user.id})
        return Response(serializer.data)

@permission_classes([AllowAny])
class Vendorbystorename(APIView):        
    def get(self,request,name):     # vendor store get by name 
        store=Morwagon_users.objects.filter(store_name=name,role=2,is_deleted=False,expirydate__gte=datetime.now(),is_suspended=False,is_rejected=False,approval=True).first()
        serializer=CurrentUserSerializer(store)
        return Response(serializer.data)

# @permission_classes([AllowAny])
# class Popularproductmodel(APIView):   
#     def get(self,request,lat,lng,radius,limit):    #for popular search get method (by button click of products)
#         latitude = float(lat)
#         logitude = float(lng)
#         radius = float(radius)
#         limit=float(limit)
#         radius = float(radius) / 1000.0

#         query = """SELECT id, (6367*acos(cos(radians(%2f))
#                 *cos(radians(lat))*cos(radians(lng)-radians(%2f))
#                 +sin(radians(%2f))*sin(radians(lat))))
#                 AS distance FROM celebrations_products HAVING
#                 distance < %2f ORDER BY distance LIMIT 0, %d""" % (
#             float(latitude),
#             float(logitude),
#             float(latitude),
#             radius,
#             limit
#         )
#         cursor = connection.cursor()
#         cursor.execute(query)
#         row = cursor.fetchall()
#         leng=len(row)
#         list1=[]
#         for i in range(leng):
#             p=row[i][0]
#             list1.append(p)        
#         duplicate_names = Popular.objects.filter(product__in=list1).values('product').annotate(Count('product')).filter(product__count__gt=0).order_by('product__count')[:5]
#         print(duplicate_names,"lllllllllllllllllllllll")        
#         lengpro=len(duplicate_names)
#         list2=[]
#         for i in range(lengpro):
#             x=row[i][0]
#             list2.append(x)        
#         product = Products.objects.filter(id__in=list2,is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False)       
#         pro = product.annotate(avg_rating=Avg('reviews__rating'))
        
#         paginator = Paginator(pro, NO_PRODUCTS_PER_PAGE)
#         import math
#         no_of_pages = 0
#         if len(pro) > 0 and NO_PRODUCTS_PER_PAGE > 0:
#             no_of_pages = math.ceil(len(pro) / NO_PRODUCTS_PER_PAGE)

#         page = 1
#         if 'page' in request.GET:
#             try:
#                 page = int(request.GET['page'])
#             except:
#                 return Response(pro.errors,status=status.HTTP_400_BAD_REQUEST)
#         if no_of_pages >= page:
#             paged_products = paginator.get_page(page)
#             serializer = ProductsearchSerializer(paged_products, many=True, context={'user_id': request.user.id})

#             response_context = {
#                     "products": serializer.data,
#                     "no_of_pages": no_of_pages,
#                     "current_page": int(page),
#             }
#         else:
#             response_context = {            
#                     "products": [],
#                     "no_of_pages": no_of_pages,
#                     "current_page": int(page),
#             }

#         return Response(response_context, status=status.HTTP_200_OK)


@permission_classes([AllowAny])
class Popularproductpostmodel(APIView):         
    def post(self,request):
        productid=request.data["product_id"]
        ip_address=request.data["ip_address"]
        ip=Popular.objects.filter(ip_address=ip_address,product=productid)
        if len(ip)==0:
            ordinary_dict = {'product': productid, 'ip_address': ip_address,'click_count': 1 }
            query_dict = QueryDict('', mutable=True)
            query_dict.update(ordinary_dict)
            populardetails=PopularSerializer(data=query_dict)
            if populardetails.is_valid():
                populardetails.save()
                return Response(populardetails.data) 
            else:
                return Response(populardetails.errors,status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_208_ALREADY_REPORTED,data={"message":"already added by this ip"})


# @permission_classes([AllowAny])
# class Trendingproductmodel(APIView):   
#     def get(self,request,lat,lng,radius,limit):    # get most trending product (by button click of call me and send message)
#         latitude = float(lat)
#         logitude = float(lng)
#         radius = float(radius)
#         limit=float(limit)
#         radius = float(radius) / 1000.0

#         query = """SELECT id, (6367*acos(cos(radians(%2f))
#                 *cos(radians(lat))*cos(radians(lng)-radians(%2f))
#                 +sin(radians(%2f))*sin(radians(lat))))
#                 AS distance FROM celebrations_products HAVING
#                 distance < %2f ORDER BY distance LIMIT 0, %d""" % (
#             float(latitude),
#             float(logitude),
#             float(latitude),
#             radius,
#             limit
#         )
#         cursor = connection.cursor()
#         cursor.execute(query)
#         row = cursor.fetchall()
#         leng=len(row)
#         list1=[]
#         for i in range(leng):
#             p=row[i][0]
#             list1.append(p)        
#         duplicate_names = Enquiry.objects.filter(product__in=list1).values('product').annotate(Count('product')).filter(product__count__gt=0).order_by('product__count')[:5]
#         print(duplicate_names,"lllllllllllllllllllllll")        
#         lengpro=len(duplicate_names)
#         list2=[]
#         for i in range(lengpro):
#             x=row[i][0]
#             list2.append(x)        
#         product=Products.objects.filter(id__in=list2,is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False)       
#         pro = product.annotate(avg_rating=Avg('reviews__rating'))

#         paginator = Paginator(pro, NO_PRODUCTS_PER_PAGE)
#         import math
#         no_of_pages = 0
#         if len(pro) > 0 and NO_PRODUCTS_PER_PAGE > 0:
#             no_of_pages = math.ceil(len(pro) / NO_PRODUCTS_PER_PAGE)

#         page = 1
#         if 'page' in request.GET:
#             try:
#                 page = int(request.GET['page'])
#             except:
#                 return Response(pro.errors,status=status.HTTP_400_BAD_REQUEST)
#         if no_of_pages >= page:
#             paged_products = paginator.get_page(page)
#             serializer = ProductsearchSerializer(paged_products, many=True, context={'user_id': request.user.id})

#             response_context = {
          
#                     "products": serializer.data,
#                     "no_of_pages": no_of_pages,
#                     "current_page": int(page),
        
#             }
#         else:
#             response_context = {
              
#                     "products": [],
#                     "no_of_pages": no_of_pages,
#                     "current_page": int(page),
                
#             }

#         return Response(response_context, status=status.HTTP_200_OK)


# @permission_classes([AllowAny])
# class Explorenearbyproductmodel(APIView):   
#     def get(self,request,lat,lng,radius,limit):    # get Explore nearby product (by location)
#         latitude = float(lat)
#         logitude = float(lng)
#         radius = float(radius)
#         limit=float(limit)
#         radius = float(radius) / 1000.0

#         query = """SELECT id, (6367*acos(cos(radians(%2f))
#                 *cos(radians(lat))*cos(radians(lng)-radians(%2f))
#                 +sin(radians(%2f))*sin(radians(lat))))
#                 AS distance FROM celebrations_products HAVING
#                 distance < %2f ORDER BY distance LIMIT 0, %d""" % (
#             float(latitude),
#             float(logitude),
#             float(latitude),
#             radius,
#             limit
#         )
#         cursor = connection.cursor()
#         cursor.execute(query)
#         row = cursor.fetchall()
#         leng=len(row)
#         list1=[]
#         for i in range(leng):
#             p=row[i][0]
#             list1.append(p)                                                        
#         product = Products.objects.filter(id__in=list1,is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False)
#         product_names = product.annotate(avg_rating=Avg('reviews__rating'))
        
#         paginator = Paginator(product_names, NO_PRODUCTS_PER_PAGE)
#         import math
#         no_of_pages = 0
#         if len(product_names) > 0 and NO_PRODUCTS_PER_PAGE > 0:
#             no_of_pages = math.ceil(len(product_names) / NO_PRODUCTS_PER_PAGE)

#         page = 1
#         if 'page' in request.GET:
#             try:
#                 page = int(request.GET['page'])
#             except:
#                 return Response(product_names.errors,status=status.HTTP_400_BAD_REQUEST)
#         if no_of_pages >= page:
#             paged_products = paginator.get_page(page)
            
#             serializer = ProductsearchSerializer(paged_products, many=True, context={'user_id': request.user.id})

#             response_context = {
#                     "products": serializer.data,
#                     "no_of_pages": no_of_pages,
#                     "current_page": int(page),
#             }
#         else:
#             response_context = {            
#                     "products": [],
#                     "no_of_pages": no_of_pages,
#                     "current_page": int(page),
#             }

#         return Response(response_context, status=status.HTTP_200_OK)          

@permission_classes([AllowAny])
class Bestservicestoremodel(APIView):
    def get(self,request,lat,lng,radius,limit):    # for getting best service store method (by rating)
        latitude = float(lat)
        logitude = float(lng)
        radius = float(radius)
        limit=float(limit)
        radius = float(radius) / 1000.0

        query = """SELECT id, (6367*acos(cos(radians(%2f))
                *cos(radians(latitude))*cos(radians(logitude)-radians(%2f))
                +sin(radians(%2f))*sin(radians(latitude))))
                AS distance FROM celebrations_morwagon_users HAVING
                distance < %2f ORDER BY distance LIMIT 0, %d""" % (
            float(latitude),
            float(logitude),
            float(latitude),
            radius,
            limit
        )
        cursor = connection.cursor()
        cursor.execute(query)
        row = cursor.fetchall()
        leng=len(row)
        list1=[]
        for i in range(leng):
            p=row[i][0]
            list1.append(p) 

        sumofcount=Morwagon_users.objects.filter(id__in=list1,role=2,is_deleted=False,expirydate__gte=datetime.now(),is_suspended=False,is_rejected=False,approval=True).annotate(avg_rating=Avg('vendor__reviews__rating')).order_by('-avg_rating') 
        print(sumofcount,"........................thufail")

        paginator = Paginator(sumofcount, NO_PRODUCTS_PER_PAGE)
        import math
        no_of_pages = 0
        if len(sumofcount) > 0 and NO_PRODUCTS_PER_PAGE > 0:
            no_of_pages = math.ceil(len(sumofcount) / NO_PRODUCTS_PER_PAGE)

        page = 1
        if 'page' in request.GET:
            try:
                page = int(request.GET['page'])
            except:
                return Response(sumofcount.errors,status=status.HTTP_400_BAD_REQUEST)
        if no_of_pages >= page:
            paged_products = paginator.get_page(page)
            # serializer = ProductsallSerializer(paged_products, many=True, context={'user_id': request.user.id})
            serializer=MorwagonReviewSerializer(paged_products,many=True)

            response_context = {
                    "store": serializer.data,
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
            }
        else:
            response_context = {            
                    "store": [],
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
            }

        return Response(response_context, status=status.HTTP_200_OK)  


# @permission_classes([AllowAny])
class Reviewratingmodel(APIView):
    parser_classes=(MultiPartParser,)  # review post
    def post(self,request,*args,**kwargs):
        user=request.user.id
        moruser=Morwagon_users.objects.get(user=user)
        id=moruser.id
        product=request.data["product"]
        title=request.data["title"]
        rating=request.data["rating"]
        review=request.data["review"]
        moruser=Review.objects.filter(Q(user=id),Q(product=product),Q(Q(approvel_sts=0) | Q(approvel_sts=1))).exists()
        if moruser==False:
            settreview=Settings.objects.get(name="review")
            reviewappr=settreview.need_approval
            if reviewappr==False:
                ordinary_dict = {'product': product, 'user': id,'title': title,'rating': rating,'review': review,'approvel_sts': 1,'update_date':date.today(),}
            else:
                ordinary_dict = {'product': product, 'user': id,'title': title,'rating': rating,'review': review,'update_date':date.today(), }
            query_dict = QueryDict('', mutable=True)
            query_dict.update(ordinary_dict)
            reviewdetails=ReviewuserSerializer(data=query_dict)
            if reviewdetails.is_valid():
                print("user",user,"pk",product)
                reviewdetails.save()
                
                id=reviewdetails['id'].value
                reviewobj=Review.objects.get(id=id)
                # file_obj=request.FILES['files']
                imagelist=request.FILES.getlist("images")
                length=len(imagelist)
                for img in imagelist:
                    Review_images.objects.create(review=reviewobj,images=img,)            
                return Response(reviewdetails.data) 
            else:
                return Response(reviewdetails.errors,status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_208_ALREADY_REPORTED)
       
# @permission_classes([AllowAny])
# class Reviewratingputandgetmodel(APIView):
#     def put(self, request,id):    # review edit by review id  # from front end we get delete image array  and get upload images
#         # fields = ('id', 'product', 'user', 'title', 'rating','reject_reason','review','approvel_sts', 'review_images')
#         pro=request.data["product"]
#         use=request.data["user"]
#         pro = Products.objects.get(id=pro)
#         use = User.objects.get(id=use)
#         review = Review.objects.get(id=id)
#         review.product=pro
#         review.user=use
#         review.title=request.data["title"]
#         review.rating=request.data["rating"]
#         review.review=request.data["review"]
#         review.approvel_sts=0
#         review.reject_reason="liyaaaaaaaaa"
#         review.save()


#         # ordinary_dict = {'product': 1, 'user': 1,'title': "kkkk",'rating': 4,'review': "good", 'approvel_sts':0,'reject_reason':"liyaaa"}
#         # query_dict = QueryDict('', mutable=True)
#         # query_dict.update(ordinary_dict)
#         # print("ppppppppppp")
#         # print(review,"jjjjjjjjj")
#         # print(query_dict,"hhhhhhhhhh")
#         # serializer = ReviewupdateSerializer(instance=review, data=query_dict,partial=True)
#         # if serializer.is_valid():
#         #     serializer.save()
#         #     print("kkkkkkkkkkkkk")
#         reviewobj=Review.objects.get(id=id)
#         imagelist=request.FILES.getlist("images")
#         length=len(imagelist)
#         for img in imagelist:
#             Review_images.objects.create(review=reviewobj,images=img,)
#         imagearray=request.POST.getlist("imagearray")
#         for url in imagearray:  #get delete image array 
#             oneurl = urlparse(url)
#             imgs=oneurl.path
#             revw=Review_images.objects.get(images=imgs[1:])
#             revw.images.storage.delete(revw.images.name)
#             revw.delete()        
#         return Response(serializer.data)



@permission_classes([AllowAny])
class Reviewratingputandgetmodel(APIView):
    def get(self,request,id):     # review get by review id
        reviewdetail=Review.objects.filter(id=id,approvel_sts=1,is_deleted=False)
        serializer=ReviewSerializer(reviewdetail,many=True)
        return Response(serializer.data)
    def put(self, request,id): 
        title=request.data["title"]
        rating=request.data["rating"]
        review=request.data["review"]
        reviews = Review.objects.get(id=id)
        reviews.title=request.data["title"]
        reviews.rating=request.data["rating"]
        reviews.review=request.data["review"]
        reviews.approvel_sts=0
        reviews.reject_reason=""
        reviews.update_date=date.today()
        reviews.save()
        imagearray=request.POST.getlist("imagearray")
        for url in imagearray:  #get delete image array 
            oneurl = urlparse(url)
            imgs="images/"+os.path.basename(oneurl.path)
            revw=Review_images.objects.get(images=imgs)
            revw.images.storage.delete(revw.images.name)
            revw.delete() 
        imagelist=request.FILES.getlist("images")
        length=len(imagelist)
        for img in imagelist:
            Review_images.objects.create(review=reviews,images=img,)  
        reviewdetail=Review.objects.filter(id=id)
        serializer=ReviewSerializer(reviewdetail,many=True)         
        return Response(serializer.data)

@permission_classes([AllowAny])
class Reviewratingpbyproductidmodel(APIView):        
    def get(self,request,id):     # review get by product id
        reviewdetail=Review.objects.filter(product=id,approvel_sts=1)
        serializerreview=ReviewSerializer(reviewdetail,many=True)
        reviewimage=Review_images.objects.filter(review__product=id,review__approvel_sts=1)
        serializerimage=Review_imagesSerializer(reviewimage,many=True)
        response_context = {
            "review": serializerreview.data,
            "reviewimage": serializerimage.data,
        }
        return Response(response_context, status=status.HTTP_200_OK)

# @permission_classes([AllowAny])
class Reviewratinggetuserpromodel(APIView):
    def get(self,request):     # review get by product id and userid
        product_id=request.GET["product_id"]
        user_id=request.user.id
        moruser=Morwagon_users.objects.get(user=user_id)
        id=moruser.id
        reviewdetails=Review.objects.filter(product=product_id,user=id,approvel_sts=1).exists()
        if reviewdetails==True:
            reviewdet=Review.objects.get(product=product_id,user=id,approvel_sts=1)
            serializer=ReviewSerializer(reviewdet)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)




@permission_classes([AllowAny])
class Productbyvendorid(APIView):        
    def get(self,request,id):     # product get by vendor id
        pro=Products.objects.filter(vendor=id,is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,is_suspended=False)
        product=pro.annotate(avg_rating=Avg('reviews__rating'))
        serializer = ProductsearchSerializer(product, many=True, context={'user_id': request.user.id})
        return Response(serializer.data)

@permission_classes([AllowAny])
class Productbycategoryid(APIView):        
    def get(self,request,id):     # product get by category id
        product=Products.objects.filter(category=id,is_approved=True,is_deleted=False,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,is_suspended=False,vendor__is_rejected=False)
        # serializer=ProductsfavouriteSerializer(product,many=True)
        serializer = ProductsfavouriteSerializer(product, many=True, context={'user_id': request.user.id})
        return Response(serializer.data)

@permission_classes([AllowAny])
class Productbysubcategoryid(APIView):        
    def get(self,request,id):     # product get by subcategory id
        productdetail=Products.objects.filter(subcategory=id,is_approved=True,is_deleted=False,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,is_suspended=False,vendor__is_rejected=False)
        paginator = Paginator(productdetail, NO_PRODUCTS_PER_PAGE)
        import math
        no_of_pages = 0
        if len(productdetail) > 0 and NO_PRODUCTS_PER_PAGE > 0:
            no_of_pages = math.ceil(len(productdetail) / NO_PRODUCTS_PER_PAGE)

        page = 1
        if 'page' in request.GET:
            try:
                page = int(request.GET['page'])
            except:
                return Response(productdetail.errors,status=status.HTTP_400_BAD_REQUEST)
        if no_of_pages >= page:
            paged_products = paginator.get_page(page)
            serializer = ProductsfavouriteSerializer(paged_products, many=True, context={'user_id': request.user.id})

            response_context = {

                    "products": serializer.data,
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
                
            }
        else:
            response_context = {
                    "products": [],
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
                
            }
            
        return Response(response_context, status=status.HTTP_200_OK)

@permission_classes([AllowAny])
class Allvendormodel(APIView):
    def get(self,request):     
        allvendor=Morwagon_users.objects.filter(role=2,user__is_active=True,is_deleted=False,expirydate__gte=datetime.now(),is_suspended=False,is_rejected=False,approval=True)
        serializer=MorwagonUserSerializer(allvendor, many=True)
        return Response(serializer.data)

@permission_classes([AllowAny])
class Vendorbyidmodel(APIView):
    def get(self,request,id):      
        vendordetail=Morwagon_users.objects.get(id=id,expirydate__gte=datetime.now())
        serializer=MorwagonUserSerializer(vendordetail)
        return Response(serializer.data)

@permission_classes([AllowAny])
class Albumsbyproductidmodel(APIView):  # albums by product id
    def get(self,request,id,page):      
        # albumdetail=Album.objects.filter(Q(service=id) & ~Q(album_details__is_approved=False))
        NO_ALBUM_PER_PAGE=4
        Albumdetail=Album.objects.get(service=id)
        albumid=Albumdetail.id
        albumimg=Album_details.objects.filter(album=albumid,is_approved=True,is_deleted=False)
        # serializer=AlbumimageSerializer(albumimg,many=True)
        # return Response(serializer.data)



        paginator = Paginator(albumimg, NO_ALBUM_PER_PAGE)
        import math
        no_of_pages = 0
        if len(albumimg) > 0 and NO_ALBUM_PER_PAGE > 0:
            no_of_pages = math.ceil(len(albumimg) / NO_ALBUM_PER_PAGE)

        # page = 1
        # if page == None:
        #     try:
        #         page = page
        #     except:
        #         return Response(Faqset.errors,status=status.HTTP_400_BAD_REQUEST)
        if no_of_pages >= page:
            paged_products = paginator.get_page(page)
            
            serializer = AlbumimageSerializer(paged_products, many=True)

            response_context = {
                    "album": serializer.data,
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
                    "total_records": len(albumimg),

            }
        else:
            response_context = {            
                    "album": [],
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
                    "total_records": len(albumimg),
            }
        return Response(response_context, status=status.HTTP_200_OK) 


@permission_classes([AllowAny])
class Albumsbyvendoridmodel(APIView):  # albums by vendor id
    def get(self,request,id):     
        Albumdetail=Album.objects.filter(service__vendor=id).values_list('id',flat=True)
        # albumid=Albumdetail.id
        albumimg=Album_details.objects.filter(album__in=Albumdetail,is_approved=True,is_deleted=False)
        serializer=AlbumimageSerializer(albumimg, many=True)
        return Response(serializer.data)

@permission_classes([AllowAny])
class Videobyproductidmodel(APIView):  # videos by product id
    def get(self,request,id):      
        videodetail=Videos.objects.filter(service=id,is_approved=True,is_deleted=False)
        serializer=VideosSerializer(videodetail, many=True)
        return Response(serializer.data)

@permission_classes([AllowAny])
class Videobyvendoridmodel(APIView):  # videos by vendor id
    def get(self,request,id):      
        videodetail=Videos.objects.filter(service__vendor=id,is_approved=True,is_deleted=False)
        serializer=VideosSerializer(videodetail, many=True)
        return Response(serializer.data)

@permission_classes([AllowAny])
class Favouritesmodel(APIView):  # favourite product by user_id and productid
    def get(self,request,productid): 
        userid=request.user.id 
        user=User.objects.get(id=userid)
        product=Products.objects.get(id=productid)
        favrecord=Favourites.objects.filter(user=userid,product=product).exists() 
        print(favrecord,"lllllllll")   
        if favrecord:
            fav=Favourites.objects.filter(user=userid,product=product).delete()
            print(fav,"jjjjjjjjj")
            if fav is None:                                    
                return HttpResponse(status=HTTP_404_NOT_FOUND)
            else:
                # favourite=Favourites.objects.filter(user=userid)
                # serializer=FavouritesSerializer(favourite, many=True)
                # return Response(serializer.data,status=status.HTTP_200_OK)
                response_context = {            
                    "favourite": {},
                    "message": "deleted",
                }

                return Response(response_context, status=status.HTTP_204_NO_CONTENT)
        else:            
            fava=Favourites.objects.create(user=user,product=product)
            if fava is None:                                    
                # return HttpResponse(status=HTTP_404_NOT_FOUND)
                response_context = {            
                    "favourite": {},
                    "message": "no data",
                }

                return Response(response_context, status=status.HTTP_200_OK)
            else:
                print("liya",productid)
                favourite=Favourites.objects.get(user=userid,product=productid)
                serializer=FavouritesSerializer(favourite)
                response_context = {            
                    "favourite": serializer.data,
                    "message": "added",
                }

                return Response(response_context, status=status.HTTP_200_OK)

# @permission_classes([AllowAny])
class Favouritesbyuser(APIView):  # favourite product by user
    def get(self,request): 
        userid=request.user.id
        favourite=Favourites.objects.filter(user=userid,product__is_suspended=False)
        serializer=FavouritesSerializer(favourite, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

# @permission_classes([AllowAny])
class Favouritesbyuserandproduct(APIView):  # favourite product by productid by user
    def get(self,request): 
        userid=request.user.id
        productid=request.GET["productid"]
        favourite=Favourites.objects.filter(product=productid,user=userid).exists()
        if favourite==True:
            return Response(status=status.HTTP_200_OK)

        else:
            return Response(status=status.HTTP_204_NO_CONTENT)



@permission_classes([AllowAny])
class Enquirypostmodel(APIView):
    def post(self,request):
        productid=request.data["product"]
        ip_address=request.data["ip_address"]
        type=request.data["type"]
        ip=Enquiry.objects.filter(ip_address=ip_address,product=productid)
        if len(ip)==0:
            ordinary_dict = {'product': productid, 'ip_address': ip_address,'click_count': 1,'type': type }
            query_dict = QueryDict('', mutable=True)
            query_dict.update(ordinary_dict)
            enquirydetails=EnquirySerializer(data=query_dict)
            if enquirydetails.is_valid():
                enquirydetails.save()
                return Response(enquirydetails.data) 
            else:
                return Response(enquirydetails.errors,status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_208_ALREADY_REPORTED,data={"message":"already added by this ip"})

# this a new function of addcount model post 
# merge the get function into it(below - get function)
# run and verify it work or not

@permission_classes([AllowAny])
class Addspostmodel(APIView):  # add clickcount post
    def get(self,request):
        addid=request.GET['addid']
        clickcount=1
        user=request.user.id
        muser=Morwagon_users.objects.get(user=user)
        userid=muser.id
        adds=Addscount.objects.filter(customer=userid,adds=addid).exists()
        if adds==True:
            addclick=Addscount.objects.get(customer=userid,adds=addid)
            clickcount=addclick.clickcount
            ordinary_dict = {'customer': userid, 'adds': addid, 'clickcount': clickcount+1,}
            query_dict = QueryDict('', mutable=True)
            query_dict.update(ordinary_dict)
            adv=Addscount.objects.get(customer=userid,adds=addid)
            serializer = AddscountSerializer(instance=adv, data=query_dict,partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data) 
            else:
                return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
        else:
            ordinary_dict = {'customer': userid, 'adds': addid, 'clickcount': clickcount,}
            query_dict = QueryDict('', mutable=True)
            query_dict.update(ordinary_dict)
            serializer=AddscountSerializer(data=query_dict)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

@permission_classes([AllowAny])
class Addsgetmodel(APIView):  # add get by latest and clickcount(location,expiry,category)
    def get(self,request,lat,lng,radius,limit):       
        user=request.user.id
        if user==None:
            addsdetail=Adds.objects.filter(is_approved=True,is_deleted=False,product__vendor__expirydate__gte=datetime.now(),product__is_suspended=False,product__vendor__is_suspended=False).order_by('-created_on')[:10]
            serializer=AddsSerializer(addsdetail, many=True)
            return Response(serializer.data)
        else:
            muser=Morwagon_users.objects.get(user=user)
            userid=muser.id

            latitude = float(lat)
            logitude = float(lng)
            radius = float(radius)
            limit=float(limit)
            radius = float(radius) / 1000.0

            query = """SELECT id, (6367*acos(cos(radians(%2f))
                    *cos(radians(lat))*cos(radians(lng)-radians(%2f))
                    +sin(radians(%2f))*sin(radians(lat))))
                    AS distance FROM celebrations_products HAVING
                    distance < %2f ORDER BY distance LIMIT 0, %d""" % (
                float(latitude),
                float(logitude),
                float(latitude),
                radius,
                limit
            )
            cursor = connection.cursor()
            cursor.execute(query)
            row = cursor.fetchall()
            leng=len(row)
            list1=[]
            for i in range(leng):
                p=row[i][0]
                list1.append(p) 

            adds_query = """select celebrations_adds.id from celebrations_adds left join celebrations_products on celebrations_adds.product_id=celebrations_products.id 
            where celebrations_products.category_id =(select category_id from celebrations_products inner join celebrations_adds 
            on celebrations_products.id=celebrations_adds.product_id inner join celebrations_addscount on celebrations_addscount.adds_id=celebrations_adds.id where celebrations_addscount.customer_id=%d
            order by clickcount desc limit 1)"""% userid

            cursor.execute(adds_query)
            rows = cursor.fetchall()
            lengs=len(rows)
            list2=[]
            for i in range(leng):
                p=row[i][0]
                list2.append(p) 
            
            addsdetail=Adds.objects.filter(product__id__in=list1,is_approved=True,is_deleted=False,product__vendor__expirydate__gte=datetime.now(),product__is_suspended=False,product__vendor__is_suspended=False).order_by('-created_on')
            if len(list2)>0:
                addsdetail = addsdetail.filter(id__in=list2)
            addsdetail = addsdetail[:10]
            serializer=AddsSerializer(addsdetail, many=True)
            return Response(serializer.data)
    


@permission_classes([AllowAny])
class Searchbykeymodel(APIView):  # search by product name and store name
    def get(self,request,search_key): 
        keys=search_key
        if keys!='':
            pro=Products.objects.filter(product_name__icontains=keys,vendor__is_suspended=False,is_suspended=False)
            proserializer=ProductsSerializer(pro, many=True)
            store=Morwagon_users.objects.filter(store_name__icontains=keys,expirydate__gte=datetime.now(),is_suspended=False)
            storeserializer=CurrentUserSerializer(store, many=True)
            return Response({
                'product': proserializer.data,
                'store': storeserializer.data,
            })
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"There is no keyword"})


@permission_classes([AllowAny])
class Searchnearbyproductmodel(APIView):
    def post(self,request):   # get search nearby product with category (by location)
        NO_PRODUCTS_PER_PAGE=5
        keys = ''
        category = ''
        subcategory = []
        sub = []
        sort_flag = False
        if 'searchkey' in request.POST:
            keys = request.POST["searchkey"]
            print(keys,"ggggggggggggggggggggggggggggg")
        if 'category' in request.POST:
            category = request.POST["category"]
        if 'subCategory' in request.POST:
            subcategory = request.POST["subCategory"]
            sub=subcategory.split(",")
            for i in range(0, len(sub)):
                sub[i] = int(sub[i])
            print(sub,"mmmmmmmmmmmmmmmm")

        if 'lat' in request.POST:
            print("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
            lat = request.POST["lat"]
            lng = request.POST["lng"]
            radius = request.POST["radius"]
            limit = int(request.POST["limit"])
            latitude = float(lat)
            logitude = float(lng)
            radius = float(radius)
            # limit=float(limit)
            radius = float(radius) / 1000.0
            query = """SELECT id, (6367*acos(cos(radians(%2f))
                    *cos(radians(lat))*cos(radians(lng)-radians(%2f))
                    +sin(radians(%2f))*sin(radians(lat))))
                    AS distance FROM celebrations_products HAVING
                    distance < %2f ORDER BY distance""" % (
                float(latitude),
                float(logitude),
                float(latitude),
                radius
            )
            cursor = connection.cursor()
            cursor.execute(query)
            row = cursor.fetchall()
            leng=len(row)
            list1=[]
            for i in range(leng):
                p=row[i][0]
                list1.append(p)

            product = Products.objects.filter(id__in=list1,is_approved=True,category__category_name__icontains=category,product_name__icontains=keys,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,is_suspended=False,is_deleted=False,vendor__is_rejected=False)
            store = Products.objects.filter(id__in=list1,is_approved=True,category__category_name__icontains=category,vendor__store_name__icontains=keys,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,is_suspended=False,is_deleted=False,vendor__is_rejected=False)
            if len(sub)>0:
                product = product.filter(subcategory_id__in=sub)
                store = product.filter(subcategory_id__in=sub)
            
            if 'min_price' in request.POST:
                min_price = int(request.POST["min_price"])
                max_price = int(request.POST["max_price"])
                productlist=[]
                productlist=list(product.values_list('id',flat=True))
                print(productlist,"1111111111111111111111111111111111111111111111")
                pricepro = PriceUnit.objects.filter(product__id__in=productlist)
                list1=[]
                list2=[]
                for profix in pricepro:
                    fixornot=profix.is_fixedprice
                    if fixornot:
                        if(profix.price>=min_price and profix.price<=max_price ):                            
                            list1.append(profix.product.id) 
                    else:
                        if((profix.from_price>=min_price and profix.to_price>=min_price) and (profix.from_price<=max_price and profix.to_price<=max_price)):
                            list1.append(profix.product.id)
                mylist1 = list(dict.fromkeys(list1))
                product = product.filter(id__in=mylist1)


                storepro=[]
                storepro=list(store.values_list('id',flat=True))
                priceprostore = PriceUnit.objects.filter(product__id__in=storepro)
                for profixstore in priceprostore:
                    fixornotstore=profixstore.is_fixedprice
                    if fixornotstore:
                        if(profixstore.price>=min_price and profixstore.price<=max_price ):                            
                            list2.append(profixstore.product.id) 
                    else:
                        if((profixstore.from_price>=min_price and profixstore.to_price>=min_price) and (profixstore.from_price<=max_price and profixstore.to_price<=max_price)):
                            list2.append(profixstore.product.id)
                mylist2 = list(dict.fromkeys(list2))
                store = store.filter(id__in=mylist2)

            if 'rating' in request.POST:
                rating_flag = True
                rating = request.POST["rating"]
                profullist = product.annotate(avg_rating=Avg('reviews__rating'))
                storefullist = store.annotate(avg_rating=Avg('reviews__rating'))
                product = profullist.filter(avg_rating__gte=rating)
                store = storefullist.filter(avg_rating__gte=rating)
            else:
                product = product.annotate(avg_rating=Avg('reviews__rating'))
                store = store.annotate(avg_rating=Avg('reviews__rating'))
            
            if 'sort' in request.POST:
                sortcategory=request.POST["sort"]
                if sortcategory=="newest":
                    product=product.order_by('-id')
                    store = store.order_by('-id')

                elif sortcategory=="price-lh":
                    sort_flag =True
                    productlist=product.values_list('id',flat=True)
                    # listToStr = ','.join(map(str, productlist))
                    listToStrpro = ','.join(str(x) for x in productlist)
                    if listToStrpro == '':
                        query = """select celebrations_products.*, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                        celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id order by prrr;"""
                    else:
                        query = """select celebrations_products.*, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                        celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrpro+""") order by prrr;"""
                    cursor = connection.cursor()
                    cursor.execute(query)
                    product=dictfetchall(cursor)
                    # j=[]
                    # for i in product:
                    #     j.append(i["id"])
                    # k=set(j)
                    # k=list(k)
                    # product = Products.objects.filter(id__in=k)

                    storelist=store.values_list('id',flat=True)
                    listToStrstore = ','.join(str(x) for x in storelist)
                    if listToStrstore == '':
                        querystore = """select celebrations_products.*, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                        celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id order by prrr;"""
                    else:
                        querystore = """select celebrations_products.*, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                        celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrstore+""") order by prrr;"""
                    cursorstore = connection.cursor()
                    cursorstore.execute(querystore)
                    store=dictfetchall(cursorstore)
                    # r=[]
                    # for i in store:
                    #     r.append(i["id"])
                    # h=set(r)
                    # h=list(h)
                    # store = Products.objects.filter(id__in=h)



                elif sortcategory=="price-hl":
                    sort_flag =True
                    productlist=product.values_list('id',flat=True)
                    # listToStr = ','.join(map(str, productlist))
                    listToStrpro = ','.join(str(x) for x in productlist)
                    print(type(listToStrpro),"mmmmmmmmmmmmmmmmmm")
                    if listToStrpro == '':
                        querypro = """select celebrations_products.*, if(is_fixedprice,price,to_price) as prrr from celebrations_products inner join 
                        celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id order by prrr desc;"""
                    else:    
                        querypro = """select celebrations_products.*, if(is_fixedprice,price,to_price) as prrr from celebrations_products inner join 
                        celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrpro+""") order by prrr desc;"""
                    cursorpro = connection.cursor()
                    cursorpro.execute(querypro)
                    product=dictfetchall(cursorpro)
                    # j=[]
                    # for i in product:
                    #     j.append(i["id"])
                    # k=set(j)
                    # k=list(k)
                    # product = Products.objects.filter(id__in=k)

                    storelist=store.values_list('id',flat=True)
                    listToStrstore = ','.join(str(x) for x in storelist)
                    print(type(listToStrstore),"kkkkkkkkkkkkkkkkkkkk")
                    if listToStrstore == '':
                        querystore = """select celebrations_products.*, if(is_fixedprice,price,to_price) as prrr from celebrations_products inner join 
                        celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id order by prrr desc;"""
                    else:
                        querystore = """select celebrations_products.*, if(is_fixedprice,price,to_price) as prrr from celebrations_products inner join 
                        celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrstore+""") order by prrr desc;"""
                    
                    cursorstore = connection.cursor()
                    cursorstore.execute(querystore)
                    store=dictfetchall(cursorstore)
                    # r=[]
                    # for i in store:
                    #     r.append(i["id"])
                    # h=set(r)
                    # h=list(h)
                    # store = Products.objects.filter(id__in=h)



                elif sortcategory=="relevance":                
                    product=product.order_by('-avg_rating')  
                    store=store.order_by('-avg_rating')  
                else:
                    product = product.annotate(pec=Count('product_popular__product')).order_by('-pec')       
                    store = store.annotate(pec=Count('product_popular__product')).order_by('-pec')       

            if sort_flag == True:
                pid = []
                pdts = [] 
                for pdt in product:
                    if pdt['id'] not in pid:
                        pdts.append(pdt)
                        pid.append(pdt['id'])
                product = pdts
            # product = list(set(product))
            paginator = Paginator(product, NO_PRODUCTS_PER_PAGE)
            import math
            no_of_pages_product = 0
            if len(product) > 0 and NO_PRODUCTS_PER_PAGE > 0:
                no_of_pages_product = math.ceil(len(product) / NO_PRODUCTS_PER_PAGE)

            page = 1
            if 'page' in request.GET:
                try:
                    page = int(request.GET['page'])
                except:
                    return Response(product.errors,status=status.HTTP_400_BAD_REQUEST)
            response_product = []
            if no_of_pages_product >= page:
                paged_products = paginator.get_page(page)
                if sort_flag == True:  
                    serializerproduct=ProductsearchSortSerializer(paged_products, many=True, context={'user_id': request.user.id})
                    # print(product[0]['image'],"------------------------------------------")
                else:
                    serializerproduct=ProductsearchSerializer(paged_products, many=True, context={'user_id': request.user.id})
                response_product = serializerproduct.data

            # store = list(set(store))
            if sort_flag == True:
                sid = []
                sts = [] 
                for st in store:
                    if st['id'] not in sid:
                        sts.append(st)
                        sid.append(st['id'])
                store = sts
            paginatorstore = Paginator(store, NO_PRODUCTS_PER_PAGE)    
            no_of_pages_store = 0
            if len(store) > 0 and NO_PRODUCTS_PER_PAGE > 0:
                no_of_pages_store = math.ceil(len(store) / NO_PRODUCTS_PER_PAGE)

            page = 1
            if 'page' in request.GET:
                try:
                    page = int(request.GET['page'])
                except:
                    return Response(store.errors,status=status.HTTP_400_BAD_REQUEST)

            response_store = []
            if no_of_pages_store >= page:
                paged_store = paginatorstore.get_page(page)
                if sort_flag == True:
                    serializerstore=ProductsearchSortSerializer(paged_store, many=True, context={'user_id': request.user.id})
                else:
                    serializerstore=ProductsearchSerializer(paged_store, many=True, context={'user_id': request.user.id})
                response_store = serializerstore.data

            if no_of_pages_product>=no_of_pages_store:
                no_of_pages=no_of_pages_product
            else:
                no_of_pages=no_of_pages_store    

            return Response({
                    'product': response_product,
                    'store':response_store,
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),

            })
        else:
            print("llllllllllllllllllllllllllllllllll")
            product = Products.objects.filter(is_approved=True,category__category_name__icontains=category,product_name__icontains=keys,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,is_suspended=False,is_deleted=False,vendor__is_rejected=False)
            store = Products.objects.filter(is_approved=True,category__category_name__icontains=category,vendor__store_name__icontains=keys,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,is_suspended=False,is_deleted=False,vendor__is_rejected=False)
            print(product,"pppppppppppppppppppppppppppp")
            if len(sub)>0:
                product = product.filter(subcategory_id__in=sub)
                store = store.filter(subcategory_id__in=sub)
            if 'min_price' in request.POST:
                min_price = int(request.POST["min_price"])
                max_price = int(request.POST["max_price"])
                print(min_price)
                print(product,"jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj")
                productlist=[]
                productlist=list(product.values_list('id',flat=True))
                print(productlist,"1111111111111111111111111111111111111111111111")
                pricepro = PriceUnit.objects.filter(product__id__in=productlist)
                
                list1=[]
                list2=[]
                for profix in pricepro:
                    fixornot=profix.is_fixedprice
                    
                    if fixornot:
                        print(type(profix.price),"first ifffffffffff")
                        if(profix.price>=min_price and profix.price<=max_price ):                            
                            list1.append(profix.product.id) 
                    else:
                        print("sec ifffffffffff")
                        if((profix.from_price>=min_price and profix.to_price>=min_price) and (profix.from_price<=max_price and profix.to_price<=max_price)):
                            list1.append(profix.product.id)
                mylist1 = list(dict.fromkeys(list1))
                product = product.filter(id__in=mylist1)

                print(product,"final product .........................kkkkkkkkkkkkkkkkkk")


                storepro=[]
                storepro=list(store.values_list('id',flat=True))
                priceprostore = PriceUnit.objects.filter(product__id__in=storepro)
                for profixstore in priceprostore:
                    fixornotstore=profixstore.is_fixedprice
                    if fixornotstore:
                        if(profixstore.price>=min_price and profixstore.price<=max_price ):                            
                            list2.append(profixstore.product.id) 
                    else:
                        if((profixstore.from_price>=min_price and profixstore.to_price>=min_price) and (profixstore.from_price<=max_price and profixstore.to_price<=max_price)):
                            list2.append(profixstore.product.id)
                mylist2 = list(dict.fromkeys(list2))
                store = store.filter(id__in=mylist2)

            if 'rating' in request.POST:
                rating = request.POST["rating"]
                profullist = product.annotate(avg_rating=Avg('reviews__rating'))
                storefullist = store.annotate(avg_rating=Avg('reviews__rating'))
                product = profullist.filter(avg_rating__gte=rating)
                store = storefullist.filter(avg_rating__gte=rating)
            else:
                product = product.annotate(avg_rating=Avg('reviews__rating'))
                store = store.annotate(avg_rating=Avg('reviews__rating'))

            if 'sort' in request.POST:
                sortcategory=request.POST["sort"]
                if sortcategory=="newest":
                    product=product.order_by('-id')
                    store = store.order_by('-id')
                elif sortcategory=="price-lh":
                    sort_flag == True
                    productlist=product.values_list('id',flat=True)
                    # listToStr = ','.join(map(str, productlist))
                    listToStrpro = ','.join(str(x) for x in productlist)
                    query = """select *, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrpro+""") order by prrr;"""
                    cursor = connection.cursor()
                    cursor.execute(query)
                    product=dictfetchall(cursor)
                    # j=[]
                    # for i in product:
                    #     j.append(i["id"])
                    # k=set(j)
                    # k=list(k)
                    # product = Products.objects.filter(id__in=k)

                    storelist=store.values_list('id',flat=True)
                    # listToStr = ','.join(map(str, productlist))
                    listToStrstore = ','.join(str(x) for x in storelist)
                    querystore = """select *, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrstore+""") order by prrr;"""
                    cursorstore = connection.cursor()
                    cursorstore.execute(querystore)
                    store=dictfetchall(cursorstore)
                    # r=[]
                    # for i in store:
                    #     r.append(i["id"])
                    # h=set(r)
                    # h=list(h)
                    # store = Products.objects.filter(id__in=h)

                elif sortcategory=="price-hl":
                    sort_flag == True
                    productlist=product.values_list('id',flat=True)
                    # listToStr = ','.join(map(str, productlist))
                    listToStrpro = ','.join(str(x) for x in productlist)
                    querypro = """select *, if(is_fixedprice,price,to_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrpro+""") order by prrr desc;"""
                    cursorpro = connection.cursor()
                    cursorpro.execute(querypro)
                    product=dictfetchall(cursorpro)
                    # j=[]
                    # for i in product:
                    #     j.append(i["id"])
                    # k=set(j)
                    # k=list(k)
                    # product = Products.objects.filter(id__in=k)

                    storelist=store.values_list('id',flat=True)
                    # listToStr = ','.join(map(str, productlist))
                    listToStrstore = ','.join(str(x) for x in storelist)
                    querystore = """select *, if(is_fixedprice,price,to_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrstore+""") order by prrr desc;"""
                    cursorstore = connection.cursor()
                    cursorstore.execute(querystore)
                    store=dictfetchall(cursorstore)
                    # r=[]
                    # for i in store:
                    #     r.append(i["id"])
                    # h=set(r)
                    # h=list(h)
                    # store = Products.objects.filter(id__in=h)

                elif sortcategory=="relevance":                
                    product=product.order_by('-avg_rating')  
                    store=store.order_by('-avg_rating')  
                else:
                    product = product.annotate(pec=Count('product_popular__product')).order_by('-pec')       
                    store = store.annotate(pec=Count('product_popular__product')).order_by('-pec')       
            # product = list(set(product))
            if sort_flag == True:
                pid = []
                pdts = [] 
                for pdt in product:
                    if pdt['id'] not in pid:
                        pdts.append(pdt)
                        pid.append(pdt['id'])
                product = pdts
            paginator = Paginator(product, NO_PRODUCTS_PER_PAGE)
            import math
            no_of_pages_product = 0
            if len(product) > 0 and NO_PRODUCTS_PER_PAGE > 0:
                no_of_pages_product = math.ceil(len(product) / NO_PRODUCTS_PER_PAGE)

            page = 1
            if 'page' in request.GET:
                try:
                    page = int(request.GET['page'])
                except:
                    return Response(product.errors,status=status.HTTP_400_BAD_REQUEST)
            response_product = []
            if no_of_pages_product >= page:
                paged_products = paginator.get_page(page)
                if sort_flag == True:  
                    serializerproduct=ProductsearchSortSerializer(paged_products, many=True, context={'user_id': request.user.id})
                else:
                    serializerproduct=ProductsearchSerializer(paged_products, many=True, context={'user_id': request.user.id})
                response_product = serializerproduct.data
            
            # store = list(set(store))
            if sort_flag == True:
                sid = []
                sts = [] 
                for st in store:
                    if st['id'] not in sid:
                        sts.append(st)
                        sid.append(st['id'])
                store = sts
            paginatorstore = Paginator(store, NO_PRODUCTS_PER_PAGE)    
            no_of_pages_store = 0
            if len(store) > 0 and NO_PRODUCTS_PER_PAGE > 0:
                no_of_pages_store = math.ceil(len(store) / NO_PRODUCTS_PER_PAGE)

            page = 1
            if 'page' in request.GET:
                try:
                    page = int(request.GET['page'])
                except:
                    return Response(store.errors,status=status.HTTP_400_BAD_REQUEST)

            response_store = []
            if no_of_pages_store >= page:
                paged_store = paginatorstore.get_page(page)
                if sort_flag == True:
                    serializerstore=ProductsearchSortSerializer(paged_store, many=True, context={'user_id': request.user.id})
                else:
                    serializerstore=ProductsearchSerializer(paged_store, many=True, context={'user_id': request.user.id})
                response_store = serializerstore.data

            if no_of_pages_product>=no_of_pages_store:
                no_of_pages=no_of_pages_product
            else:
                no_of_pages=no_of_pages_store 

            return Response({
                    'product': response_product,
                    'store':response_store,
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
            })


@permission_classes([AllowAny])
class Productmodel(APIView):
    def get(self,request):    #for product and favourite method
        productdetail=Products.objects.filter(is_approved=True,is_deleted=False,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,is_suspended=False)[0:10]
        paginator = Paginator(productdetail, NO_PRODUCTS_PER_PAGE)
        import math
        no_of_pages = 0
        if len(productdetail) > 0 and NO_PRODUCTS_PER_PAGE > 0:
            no_of_pages = math.ceil(len(productdetail) / NO_PRODUCTS_PER_PAGE)

        page = 1
        if 'page' in request.GET:
            try:
                page = int(request.GET['page'])
            except:
                return Response(productdetail.errors,status=status.HTTP_400_BAD_REQUEST)
        if no_of_pages >= page:
            paged_products = paginator.get_page(page)
            serializer = ProductsfavouriteSerializer(paged_products, many=True, context={'user_id': request.user.id})
            # serializer=ProductsfavouriteSerializer(paged_products,many=True)

            response_context = {

                    "products": serializer.data,
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
                
            }
        else:
            response_context = {
                    "products": [],
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
                
            }

        return Response(response_context, status=status.HTTP_200_OK)


# @permission_classes([AllowAny])
class Customergetmodel(APIView):        
    def get(self,request):     # customer profile details
        user=request.user.id
        customer=Morwagon_users.objects.get(user=user,is_verified=True)
        serializer=CurrentUserSerializer(customer)
        return Response(serializer.data)


@permission_classes([AllowAny])
class Citygetmodel(APIView):        
    def get(self,request):     # city of products locaion  
        City_list = [
                    {
                        "value": "1",
                        "label": "Palakkad",
                        "lat": "10.7867",
                        "lng": "76.6548",
                    },
                    {
                        "value": "2",
                        "label": "Thrissur",
                        "lat": "10.5276",
                        "lng": "76.2144",
                    },
                    {
                        "value": "3",
                        "label": "Calicut",
                        "lat": "11.15",
                        "lng": "75.49",
                    },
                    {
                        "value": "4",
                        "label": "Kannur",
                        "lat": "11.52",
                        "lng": "75.25",
                    },
                    {
                        "value": "5",
                        "label": "Ernakulam",
                        "lat": "10.00",
                        "lng": "76.15",
                    },
                    {
                        "value": "6",
                        "label": "Cochin",
                        "lat": "9.58",
                        "lng": "76.17",
                    },
                    {
                        "value": "7",
                        "label": "Kasaragod",
                        "lat": "12.4996",
                        "lng": "74.9869",
                    },
                    {
                        "value": "8",
                        "label": "Malappuram",
                        "lat": "11.0510",
                        "lng": "76.0711",
                    },
                    {
                        "value": "9",
                        "label": "Wayanad",
                        "lat": "11.6854",
                        "lng": "76.1320",
                    },
                    {
                        "value": "10",
                        "label": "Pathanamthitta",
                        "lat": "9.2648",
                        "lng": "76.7870",
                    },
                    {
                        "value": "11",
                        "label": "Kollam",
                        "lat": "8.8932",
                        "lng": "76.6141",
                    },
                    {
                        "value": "12",
                        "label": "Kottayam",
                        "lat": "9.5916",
                        "lng": "76.5222",
                    },
                    {
                        "value": "13",
                        "label": "Thiruvananthapuram",
                        "lat": "8.5241",
                        "lng": "76.9366",
                    },
                    {
                        "value": "14",
                        "label": "Alappuzha",
                        "lat": "9.4981",
                        "lng": "76.3388",
                    },
                    ]

        response_context = {
                    "cityData": City_list,
            }

        return Response(response_context, status=status.HTTP_200_OK)



@permission_classes([AllowAny])
class Popularproductmodel(APIView):   
    def post(self,request):    #for popular search get method (by button click of products)
        sort_flag = False
        lat = request.POST["lat"]
        lng = request.POST["lng"]
        radius = request.POST["radius"]
        limit = int(request.POST["limit"]) 
        latitude = float(lat)
        logitude = float(lng)
        radius = float(radius)
        # limit=float(limit)
        radius = float(radius) / 1000.0

        query = """SELECT id, (6367*acos(cos(radians(%2f))
                *cos(radians(lat))*cos(radians(lng)-radians(%2f))
                +sin(radians(%2f))*sin(radians(lat))))
                AS distance FROM celebrations_products HAVING
                distance < %2f ORDER BY distance""" % (
            float(latitude),
            float(logitude),
            float(latitude),
            radius
        )
        cursor = connection.cursor()
        cursor.execute(query)
        row = cursor.fetchall()
        leng=len(row)
        list1=[]
        for i in range(leng):
            p=row[i][0]
            list1.append(p)        
        # duplicate_names = Popular.objects.filter(product__id__in=list1).values('product').annotate(Count('product')).filter(product__count__gt=0).order_by('product__count')
        # print(duplicate_names,"lllllllllllllllllllllll")        
        # lengpro=len(duplicate_names)
        # list2=[]
        # for i in range(lengpro):
        #     x=duplicate_names[i]["product"]
        #     list2.append(x)        
        product = Products.objects.filter(id__in=list1,is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False).annotate(pec=Count('product_popular__product')).order_by('-pec')


        if 'min_price' in request.POST:
            min_price = int(request.POST["min_price"])
            max_price = int(request.POST["max_price"])
            productlist=[]
            for proid in product:
                productlist.append(proid.id) 
            pricepro = PriceUnit.objects.filter(product__id__in=productlist)
            list3=[]
            for profix in pricepro:
                fixornot=profix.is_fixedprice
                if fixornot:
                    if(profix.price>=min_price and profix.price<=max_price ):                            
                        list3.append(profix.product.id) 
                else:
                    if((profix.from_price>=min_price and profix.to_price>=min_price) and (profix.from_price<=max_price and profix.to_price<=max_price)):
                        list3.append(profix.product.id)
            mylist1 = list(dict.fromkeys(list3))
            product = product.filter(id__in=mylist1)


        if 'rating' in request.POST:
            rating = request.POST["rating"]
            profullist = product.annotate(avg_rating=Avg('reviews__rating'))
            # storefullist = store.annotate(avg_rating=Avg('reviews__rating'))
            product = profullist.filter(avg_rating__gte=rating)
            # store = storefullist.filter(avg_rating__gte=rating)
        else:
            product = product.annotate(avg_rating=Avg('reviews__rating'))
            # store = store.annotate(avg_rating=Avg('reviews__rating'))

        if 'sort' in request.POST:
            sortcategory=request.POST["sort"]
            if sortcategory=="newest":
                product=product.order_by('-id')
            elif sortcategory=="price-lh":
                sort_flag = True
                productlist=product.values_list('id',flat=True)
                # listToStr = ','.join(map(str, productlist))
                listToStrpro = ','.join(str(x) for x in productlist)
                if listToStrpro == '':
                    query = """select *, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id order by prrr;"""
                else:
                    query = """select *, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrpro+""") order by prrr;"""                
                cursorpro = connection.cursor()
                cursorpro.execute(query)
                product=dictfetchall(cursorpro)
            elif sortcategory=="price-hl":
                sort_flag = True
                productlist=product.values_list('id',flat=True)
                # listToStr = ','.join(map(str, productlist))
                listToStrpro = ','.join(str(x) for x in productlist)
                if listToStrpro == '':
                    querypro = """select *, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id order by prrr desc;"""
                else:
                    querypro = """select *, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrpro+""") order by prrr desc;"""                
                cursorpro = connection.cursor()
                cursorpro.execute(querypro)
                product=dictfetchall(cursorpro)

            elif sortcategory=="relevance":                
                product=product.order_by('-avg_rating')           
            else:
                product = product.filter(is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False).annotate(pec=Count('product_popular__product')).order_by('-pec')       

        # if sort_flag == True:
        #     pid = []
        #     pdts = [] 
        #     for pdt in product:
        #         if pdt['id'] not in pid:
        #             pdts.append(pdt)
        #             pid.append(pdt['id'])
        #     product = pdts
        paginator = Paginator(product, NO_PRODUCTS_PER_PAGE)
        import math
        no_of_pages = 0
        if len(product) > 0 and NO_PRODUCTS_PER_PAGE > 0:
            no_of_pages = math.ceil(len(product) / NO_PRODUCTS_PER_PAGE)

        page = 1
        if 'page' in request.GET:
            try:
                page = int(request.GET['page'])
            except:
                return Response(product.errors,status=status.HTTP_400_BAD_REQUEST)
        if no_of_pages >= page:
            paged_products = paginator.get_page(page)
            # serializer = ProductsearchSerializer(paged_products, many=True, context={'user_id': request.user.id})
            if sort_flag == True:  
                serializerproduct=ProductsearchSortSerializer(paged_products, many=True, context={'user_id': request.user.id})
                # print(product[0]['image'],"------------------------------------------")
            else:
                serializerproduct=ProductsearchSerializer(paged_products, many=True, context={'user_id': request.user.id})
            response_product = serializerproduct.data
            response_context = {
                    "products": response_product,
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
            }
        else:
            response_context = {            
                    "products": [],
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
            }

        return Response(response_context, status=status.HTTP_200_OK)


@permission_classes([AllowAny])
class Trendingproductmodel(APIView):   
    def post(self,request):    # get most trending product (by button click of call me and send message)
        sort_flag = False
        lat = request.POST["lat"]
        lng = request.POST["lng"]
        radius = request.POST["radius"]
        limit = int(request.POST["limit"])
        latitude = float(lat)
        logitude = float(lng)
        radius = float(radius)
        radius = float(radius) / 1000.0

        query = """SELECT id, (6367*acos(cos(radians(%2f))
                *cos(radians(lat))*cos(radians(lng)-radians(%2f))
                +sin(radians(%2f))*sin(radians(lat))))
                AS distance FROM celebrations_products HAVING
                distance < %2f ORDER BY distance""" % (
            float(latitude),
            float(logitude),
            float(latitude),
            radius
        )
        cursor = connection.cursor()
        cursor.execute(query)
        row = cursor.fetchall()
        leng=len(row)
        list1=[]
        for i in range(leng):
            p=row[i][0]
            list1.append(p)        
        # duplicate_names = Enquiry.objects.filter(product__id__in=list1).values('product').annotate(Count('product')).filter(product__count__gt=0).order_by('-product__count')
        # # print(Enquiry.objects.filter(product__in=list1,product__vendor__expirydate__gte=datetime.today()).values('product').annotate(Count('product')).filter(product__count__gt=0).order_by('product__count').all().query)
        # print(duplicate_names,"lllllllllllllllllllllll")        
        # lengpro=len(duplicate_names)
        # list2=[]
        # for i in range(lengpro):
        #     x=duplicate_names[i]["product"]
        #     list2.append(x)
        # product=Products.objects.filter(id__in=list2,is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False)
        product=Products.objects.filter(id__in=list1,is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False).annotate(pec=Count('product_equiry__product')).order_by('-pec')


        if 'min_price' in request.POST:
            min_price = int(request.POST["min_price"])
            max_price = int(request.POST["max_price"])
            # product = product.filter(price__gte=min_price)
            # productlist = product.filter.values_list('id')
            productlist=[]
            for proid in product:
                productlist.append(proid.id) 
            pricepro = PriceUnit.objects.filter(product__id__in=productlist)
            # serializer=ProductsSerializer(product, many=True)
            # response_context = {"pro":serializer}
            # return Response(response_context, status=status.HTTP_200_OK)
            list3=[]
            for profix in pricepro:
                fixornot=profix.is_fixedprice
                if fixornot:
                    if(profix.price>=min_price and profix.price<=max_price):                            
                        list3.append(profix.product.id) 
                else:
                    if((profix.from_price>=min_price and profix.to_price>=min_price) and (profix.from_price<=max_price and profix.to_price<=max_price)):
                        list3.append(profix.product.id)
            mylist1 = list(dict.fromkeys(list3))
            product = product.filter(id__in=mylist1)

        if 'rating' in request.POST:
            rating = request.POST["rating"]
            profullist = product.annotate(avg_rating=Avg('reviews__rating'))
            # storefullist = store.annotate(avg_rating=Avg('reviews__rating'))
            product = profullist.filter(avg_rating__gte=rating)
            # store = storefullist.filter(avg_rating__gte=rating)
        else:
            product = product.annotate(avg_rating=Avg('reviews__rating'))
            # store = store.annotate(avg_rating=Avg('reviews__rating'))
        
        if 'sort' in request.POST:
            sortcategory=request.POST["sort"]
            if sortcategory=="newest":
                product=product.order_by('-id')
            elif sortcategory=="price-lh":
                sort_flag = True
                productlist=product.values_list('id',flat=True)
                # listToStr = ','.join(map(str, productlist))
                listToStrpro = ','.join(str(x) for x in productlist)
                if listToStrpro == '':
                    query = """select *, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id order by prrr;"""
                else:
                    query = """select *, if(is_fixedprice,price,from_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrpro+""") order by prrr;"""                

                cursorpro = connection.cursor()
                cursorpro.execute(query)
                product=dictfetchall(cursorpro)


            elif sortcategory=="price-hl":
                sort_flag = True
                productlist=product.values_list('id',flat=True)
                # listToStr = ','.join(map(str, productlist))
                listToStrpro = ','.join(str(x) for x in productlist)
                if listToStrpro == '':
                    querypro = """select *, if(is_fixedprice,price,to_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id order by prrr desc;"""
                else:
                    querypro = """select *, if(is_fixedprice,price,to_price) as prrr from celebrations_products inner join 
                    celebrations_priceunit on celebrations_products.id=celebrations_priceunit.product_id where celebrations_products.id in("""+listToStrpro+""") order by prrr desc;"""                
                cursorpro = connection.cursor()
                cursorpro.execute(querypro)
                product=dictfetchall(cursorpro)


            elif sortcategory=="relevance":                
                product=product.order_by('-avg_rating')           
            else:
                product = product.filter(is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False).annotate(pec=Count('product_popular__product')).order_by('-pec')       
        # if sort_flag == True:
        #     pid = []
        #     pdts = [] 
        #     for pdt in product:
        #         if pdt['id'] not in pid:
        #             pdts.append(pdt)
        #             pid.append(pdt['id'])
        #     product = pdts
        paginator = Paginator(product, NO_PRODUCTS_PER_PAGE)
        import math
        no_of_pages = 0
        if len(product) > 0 and NO_PRODUCTS_PER_PAGE > 0:
            no_of_pages = math.ceil(len(product) / NO_PRODUCTS_PER_PAGE)

        page = 1
        if 'page' in request.GET:
            try:
                page = int(request.GET['page'])
            except:
                return Response(product.errors,status=status.HTTP_400_BAD_REQUEST)
        if no_of_pages >= page:
            paged_products = paginator.get_page(page)
            # serializer = ProductsearchSerializer(paged_products, many=True, context={'user_id': request.user.id})
            if sort_flag == True:  
                serializerproduct=ProductsearchSortSerializer(paged_products, many=True, context={'user_id': request.user.id})
                # print(product[0]['image'],"------------------------------------------")
            else:
                serializerproduct=ProductsearchSerializer(paged_products, many=True, context={'user_id': request.user.id})
            response_product = serializerproduct.data
            response_context = {
          
                    "products": response_product,
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
        
            }
        else:
            response_context = {
              
                    "products": [],
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
                
            }

        return Response(response_context, status=status.HTTP_200_OK)



@permission_classes([AllowAny])
class Explorenearbyproductmodel(APIView):   
    def post(self,request):    # get Explore nearby product (by location)
        lat = request.POST["lat"]
        lng = request.POST["lng"]
        radius = request.POST["radius"]
        limit = request.POST["limit"] 
        latitude = float(lat)
        logitude = float(lng)
        radius = float(radius)
        limit=float(limit)
        radius = float(radius) / 1000.0

        query = """SELECT id, (6367*acos(cos(radians(%2f))
                *cos(radians(lat))*cos(radians(lng)-radians(%2f))
                +sin(radians(%2f))*sin(radians(lat))))
                AS distance FROM celebrations_products HAVING
                distance < %2f ORDER BY distance LIMIT 0, %d""" % (
            float(latitude),
            float(logitude),
            float(latitude),
            radius,
            limit
        )
        cursor = connection.cursor()
        cursor.execute(query)
        row = cursor.fetchall()
        leng=len(row)
        list1=[]
        for i in range(leng):
            p=row[i][0]
            list1.append(p)                                                        
        product = Products.objects.filter(id__in=list1,is_approved=True,vendor__expirydate__gte=datetime.now(),vendor__is_suspended=False,vendor__is_rejected=False,vendor__approval=True,is_suspended=False,is_deleted=False)
        # product_names = product.annotate(avg_rating=Avg('reviews__rating'))

        if 'min_price' in request.POST:
            min_price = int(request.POST["min_price"])
            max_price = int(request.POST["max_price"])
            # product = product.filter(price__gte=min_price)
            # productlist = product.filter.values_list('id')
            productlist=[]
            for proid in product:
                productlist.append(proid.id) 
            pricepro = PriceUnit.objects.filter(product__id__in=productlist)
            # serializer=ProductsSerializer(product, many=True)
            # response_context = {"pro":serializer}
            # return Response(response_context, status=status.HTTP_200_OK)
            list3=[]
            for profix in pricepro:
                fixornot=profix.is_fixedprice
                if fixornot:
                    if(profix.price>=min_price and profix.price<=max_price ):                            
                        list3.append(profix.product.id) 
                else:
                    if((profix.from_price>=min_price and profix.to_price>=min_price) and (profix.from_price<=max_price and profix.to_price<=max_price)):
                        list3.append(profix.product.id)
            mylist1 = list(dict.fromkeys(list3))
            product = product.filter(id__in=mylist1)




        if 'rating' in request.POST:
            rating = request.POST["rating"]
            profullist = product.annotate(avg_rating=Avg('reviews__rating'))
            # storefullist = store.annotate(avg_rating=Avg('reviews__rating'))
            product = profullist.filter(avg_rating__gte=rating)
            # store = storefullist.filter(avg_rating__gte=rating)
        else:
            product = product.annotate(avg_rating=Avg('reviews__rating'))
            # store = store.annotate(avg_rating=Avg('reviews__rating'))


        
        paginator = Paginator(product, NO_PRODUCTS_PER_PAGE)
        import math
        no_of_pages = 0
        if len(product) > 0 and NO_PRODUCTS_PER_PAGE > 0:
            no_of_pages = math.ceil(len(product) / NO_PRODUCTS_PER_PAGE)

        page = 1
        if 'page' in request.GET:
            try:
                page = int(request.GET['page'])
            except:
                return Response(product.errors,status=status.HTTP_400_BAD_REQUEST)
        if no_of_pages >= page:
            paged_products = paginator.get_page(page)
            
            serializer = ProductsearchSerializer(paged_products, many=True, context={'user_id': request.user.id})

            response_context = {
                    "products": serializer.data,
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
            }
        else:
            response_context = {            
                    "products": [],
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
            }

        return Response(response_context, status=status.HTTP_200_OK)   

#  /////////////////////////////////////////////////// new suggestions apr 20               

@permission_classes([AllowAny])
class Report_choices(APIView):
    def get(self,request):
        response_context = {            
                    "0": "vendors pricing is incorrect",
                    "1": "vendors phone number is incorrect",
                    "2": "vendors address/city is incorrect",
                    "3": "This vendor has plagiarised images"
                }
        return Response(response_context, status=status.HTTP_200_OK)   



@permission_classes([AllowAny])
class Reportvendormodel(APIView):
    def post(self,request):
        vendor_id=request.data["vendor"]
        vendor_details=Morwagon_users.objects.filter(id=vendor_id).exists()
        print(vendor_details)
        if vendor_details:
            details=Report_vendorSerializer(data=request.data)            
            if details.is_valid():
                details.save()
                return Response(status=status.HTTP_200_OK) 
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

@permission_classes([AllowAny])
class gene_Otp_cont_phone(APIView):    # continue with phone
    def post(self, request):
        mobile = request.POST['mobile']
        mobdupli = Morwagon_users.objects.filter(mobile_number=mobile).exists()
        userdupli = User.objects.filter(username=mobile).exists()

        num = random_with_N_digits(4) # otp generation code with mobile number(replace when getting sms gateway)
        otpmodel = Verifymobileupdate()
        otpmodel.mobile=mobile
        otpmodel.otp=num
        otpmodel.save()
        if userdupli==True and mobdupli == True:
            response_context = {"otp": num,"newuser": True}
            return Response(response_context, status=status.HTTP_200_OK) 
        elif userdupli==False and mobdupli == True:
            return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"mobile is already exists with another account"}) 
        else:
            response_context = {"otp": num,"newuser": False}
            return Response(response_context, status=status.HTTP_200_OK) 




@permission_classes([AllowAny])
class verfication_phone_otp(APIView):    # continue with phone
    def post(self, request):
        # mobile = request.POST['mobile']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        # otp = request.POST['otp']
        password="hacker@89"
        current_datetime = datetime.now()
        otpcheck = Verifymobileupdate.objects.filter(otp=request.POST['otp'], mobile=request.POST['mobile']).last()        
        if otpcheck:
            otpback=otpcheck.otp
            y=otpcheck.created_on
            otp_from_front=request.POST['otp']
            if otpback == int(otp_from_front):
                now = datetime.utcnow().replace(tzinfo=utc)
                x =now-y
                total_seconds = x.total_seconds()
                if total_seconds > 300:
                    return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"otp verification expired"})
                else:
                    dict1 = {"message":"otp verification success"}
                    otpcheck.authenticated = True
                    otpcheck.save()
                    mobileduplicate=Morwagon_users.objects.filter(mobile_number=request.POST['mobile']).exists()
                    if mobileduplicate:
                        user = User.objects.get(username=request.POST['mobile'])
                        payload = JWT_PAYLOAD_HANDLER(user)
                        jwt_token = JWT_ENCODE_HANDLER(payload)
                        userid=payload['user_id']
                        print(jwt_token,"jjjjjj")
                        userdetails=User.objects.get(id=userid)
                        first_name = userdetails.first_name
                        last_name = userdetails.last_name
                        payload["first_name"] = first_name
                        payload["last_name"] = last_name
                        ordinary_dict = {'jwt_token': jwt_token, 'user_details': payload, 'message':"successfully created user"}
                        query_dict = QueryDict('', mutable=True)
                        query_dict.update(query_dict)
                        return Response(ordinary_dict)
                        # return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"Mobile number is already exist"})
                    else:
                        userrecord=User.objects.create_user(first_name=first_name,last_name=last_name,username=request.POST['mobile'],password=password,is_staff=1)
                        id=userrecord.id
                        print(id,"iddddddddddddddd")
                        ordinary_dict = {'user': id, 'first_name':first_name,'last_name':last_name,'mobile_number':request.POST['mobile'],'role': 3,'is_verified':True}
                        query_dict = QueryDict('', mutable=True)
                        query_dict.update(ordinary_dict)
                        print(first_name,"passwordddddddddd")
                        userdetails=PhoneUserSerializer(data=query_dict)
                        if userdetails.is_valid():
                            print(userdetails.errors,"errorsssssssssss")
                            userdetails.save() 
                            user = authenticate(username=request.POST['mobile'], password=password)
                            # user = User.objects.get(username=request.POST['mobile'])
                            if user is not None: 
                                payload = JWT_PAYLOAD_HANDLER(user)
                                jwt_token = JWT_ENCODE_HANDLER(payload)
                                userid=payload['user_id']
                                print(jwt_token,"jjjjjj")
                                userdetails=User.objects.get(id=userid)
                                first_name = userdetails.first_name
                                last_name = userdetails.last_name
                                payload["first_name"] = first_name
                                payload["last_name"] = last_name
                                ordinary_dict = {'jwt_token': jwt_token, 'user_details': payload, 'message':"successfully created user"}
                                query_dict = QueryDict('', mutable=True)
                                query_dict.update(query_dict)
                                return Response(ordinary_dict) 
                            else:
                                return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"invalid credentials user"})          
                        else:
                            # return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"invalid credentials morwagon user"}) 
                            return Response(userdetails.errors,status=status.HTTP_400_BAD_REQUEST)         
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"Incorrect does not match"}) 
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"Incorrect OTP"})  




class Customerupdatemodel(APIView):        
    def put(self,request):     # customer profile details (last and first name) update
        first_name=''
        last_name=''
        # mobile_number=''
        if 'first_name' in request.data:
            first_name=request.data["first_name"]
        if 'last_name' in request.data:
            last_name=request.data["last_name"]
        # if 'mobile_number' in request.data:
        #     mobile_number=request.data["mobile_number"]

        user=request.user.id
        customer=Morwagon_users.objects.filter(user=user,is_verified=True).exists()
        if customer is True:
            morwag=Morwagon_users.objects.get(user=user)
            # if mobile_number is '':
            #     mobile_number=morwag.mobile_number
            # else:
            #     mobileexist=Morwagon_users.objects.filter(mobile_number=mobile_number).exists()
            #     if mobileexist is True:
            #         return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"Mobile already exist"})
            if first_name is '':
                first_name=morwag.first_name
            if last_name is '':
                last_name=morwag.last_name

            ordinary_dict = {'first_name': first_name, 'last_name': last_name,}
            query_dict = QueryDict('', mutable=True)
            query_dict.update(ordinary_dict)
            serializer = CurrentUserSerializer(instance=morwag, data=query_dict, partial=True)
            if serializer.is_valid():
                serializer.save()
                userid=request.user.id
                userdetail=User.objects.get(id=userid)
                userdetail.first_name = first_name
                userdetail.last_name = last_name
                userdetail.save()
                # user = authenticate(username=mobilenum, password=password)
                return Response(serializer.data) 
            else:
                return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

class Customeremailmodel(APIView):        
    def put(self,request):     # customer email details update
        email=''
        # flag = request.data["flag"]
        if 'email' in request.data:
            email=request.data["email"]

        user=request.user.id
        customer=Morwagon_users.objects.filter(user=user,is_verified=True).exists()
        if customer is True:
            morwag=Morwagon_users.objects.get(user=user)
            if email is '':
                email=morwag.email

            ordinary_dict = {'email': email,}
            query_dict = QueryDict('', mutable=True)
            query_dict.update(ordinary_dict)
            serializer = CurrentUserSerializer(instance=morwag, data=query_dict, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data) 
            else:
                return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


# @permission_classes([AllowAny])
class generateOtpformobile(APIView):
    def post(self, request):
        if not request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED,data={"message":"credentials not provided"})
        else:
            mobiledupli=Morwagon_users.objects.filter(mobile_number=request.POST['mobile'],is_verified=True).exists()
            if mobiledupli:
                return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":" Mobile number is already exist"}) 
            else:
                mobilenum = request.POST['mobile']
                num = random_with_N_digits(4) # otp generation code with mobile number(replace when getting sms gateway)
                mutable = request.POST._mutable
                request.POST._mutable = True
                request.POST['otp'] = num
                request.POST._mutable = mutable
                serializer = VerifymobileupdateSerializer(data=request.data)
                validate = serializer.is_valid()
                if validate is False:
                    return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
                serializer.save()
                return Response(serializer.data)





# @permission_classes([AllowAny])
# class gene_Otp_cont_phone(APIView):    # continue with phone
#     def post(self, request):
#         serializer = MorwagonduplicheckSerializer(data=request.data)
#         validate = serializer.is_valid()
#         if validate is False:
#             return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
#         else:
#             num = random_with_N_digits(4) # otp generation code with mobile number(replace when getting sms gateway)
#             mobilenum = request.POST['mobile']
#             otpmodel = Verifymobileupdate()
#             otpmodel.mobile=mobilenum
#             otpmodel.otp=num
#             otpmodel.save()
#             return Response(status=status.HTTP_200_OK) 



# @permission_classes([AllowAny])
class verifyOtp(APIView):
    def post(self, request):
        mobilenum=request.POST['mobile']
        flag=request.POST['flag']
        password = "hacker@89"
        current_datetime = datetime.now()
        otpcheck = Verifymobileupdate.objects.filter(otp=request.POST['otp'], mobile=mobilenum).last()
        if otpcheck:
            otpback=otpcheck.otp
            y=otpcheck.created_on
            otp_from_front=request.POST['otp']
            if otpback == int(otp_from_front):
                now = datetime.utcnow().replace(tzinfo=utc)
                x =now-y
                total_seconds = x.total_seconds()
                if total_seconds > 300:
                    return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"otp verification failed,expired"})
                else:
                    dict1 = {"message":"otp verification success"}
                    otpcheck.authenticated = True
                    otpcheck.save()
                    if flag=="true":
                        user_id=request.user.id
                        moruser=Morwagon_users.objects.get(user=user_id)
                        moruser.mobile_number=mobilenum
                        moruser.save()
                        userdetail=User.objects.get(id=user_id)
                        userdetail.username = mobilenum
                        userdetail.save()
                        # user = authenticate(username=mobilenum, password=password)
                        return Response(dict1, status=status.HTTP_200_OK)
                    else:
                        user_id=request.user.id
                        moruser=Morwagon_users.objects.get(user=user_id)
                        moruser.mobile_number=mobilenum
                        moruser.save()
                        return Response(dict1, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST,data={"message":"otp verification failed"})

@permission_classes([AllowAny])
class faqmodel(APIView):
    def get(self,request,id,page):   
        NO_FAQ_PER_PAGE = 4
        Faqset=Faq.objects.filter(product=id,is_deleted=False)
        print(len(Faqset),"kkkkkkkkkkkkkkkkkkkkkkkkkk")
        paginator = Paginator(Faqset, NO_FAQ_PER_PAGE)
        import math
        no_of_pages = 0
        if len(Faqset) > 0 and NO_FAQ_PER_PAGE > 0:
            no_of_pages = math.ceil(len(Faqset) / NO_FAQ_PER_PAGE)

        # page = 1
        # if page == None:
        #     try:
        #         page = page
        #     except:
        #         return Response(Faqset.errors,status=status.HTTP_400_BAD_REQUEST)
        if no_of_pages >= page:
            paged_products = paginator.get_page(page)
            
            serializer = FaqSerializer(paged_products, many=True)

            response_context = {
                    "faq": serializer.data,
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
                    "total_records": len(Faqset),

            }
        else:
            response_context = {            
                    "products": [],
                    "no_of_pages": no_of_pages,
                    "current_page": int(page),
                    "total_records": len(Faqset),
            }
        return Response(response_context, status=status.HTTP_200_OK) 



# @permission_classes([AllowAny])
class contactus(APIView):
    def post(self, request):
        name=request.POST['name']
        # email=request.POST['email']
        email="liya@beaconinfo.tech"
        messagefromcus=request.POST['message']
        # htmlgen = '<p>Message from customer is <strong>'+messagefromcus+'</strong></p>'
        htmlgen = '<p>Message from customer <strong>'+name+' , '+messagefromcus+'</strong></p>'
        send_mail('Morwagon enquiry',messagefromcus,'reshma@beaconinfo.tech',[email], fail_silently=True, html_message=htmlgen)
        dict1 = {"message":"email send successfully"}
        return Response(dict1, status=status.HTTP_200_OK) 