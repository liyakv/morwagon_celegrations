import re
from unicodedata import category
from django.shortcuts import render
from django.shortcuts import render
from celebrations.forms import BannerForm
from django.http import JsonResponse, HttpResponse
from celebrations. models import Morwagon_users, Popular, Favourites, Trending, Adds, Category, Sub_category, Banner, Products, Morwagon_users, Packages, Vendor_package, Album, Adds, Videos, Album_details, Review, Review_images, Settings, Tax_details
import json
from django.core import serializers
from django.forms.models import model_to_dict
from django.shortcuts import redirect
from celebrations.serializer import SubcategorySerializer, TimeSerializer, AddSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from dateutil.relativedelta import relativedelta
import datetime
from celebrations.models import *
from django.http import HttpResponseRedirect
from django.core.files.base import ContentFile
import base64
import json
from datetime import datetime
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import authenticate, login, logout
from django.contrib import auth
import datetime
from django.utils.timezone import utc
from send_notification import *
from django.db.models import Q
from django.db.models import Count, Case, When
from decor import *
from django.db.models import Count
from django.core.mail import send_mail
from django.db.models.query import QuerySet


# Create your views here.

# def bestservicestore(request):  # for getting best service store method (by rating)
#     list1=list(Products.objects.filter(is_deleted=False,is_approved=True).values_list('id',flat=True))
#     context={'pro':Products.objects.filter(id__in=list1,reviews__approvel_sts=1).annotate(avg_rating=Avg('reviews__rating')).order_by('-avg_rating')[:10]}
#     print(context,"lllllllllllllllllllllllllll")
#     return render(request,'admin/dashboard.html',context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def dashboard(request):
    print('its in dashboard')
    vendors_count = Morwagon_users.objects.filter(
        role=2, approval=True, is_done=True, is_deleted=False).count()
    customers_count = Morwagon_users.objects.filter(
        role=3, is_deleted=False).count()
    adds_count = Adds.objects.filter(is_approved=True).count()
    # poplr = Popular.objects.all().order_by('click_count')
    popular = Popular.objects.filter(is_deleted=False).values('product').annotate(
        Count('product')).filter(product__count__gt=0).order_by('product__count')[:5]
    product = []
    for i in popular:
        product.append(i['product'])
    poplr = Products.objects.filter(id__in=product)

    best_service = Favourites.objects.all()
    list1 = list(Products.objects.filter(is_deleted=False,
                 is_approved=True).values_list('id', flat=True))
    best_service = Products.objects.filter(id__in=list1, reviews__approvel_sts=1,is_suspended=True).annotate(
        avg_rating=Avg('reviews__rating')).order_by('-avg_rating')[:10]
    trending = Trending.objects.all().order_by('click_count')
    list1 = list(Products.objects.filter(
        is_deleted=False).values_list('id', flat=True))
    duplicate_names = Enquiry.objects.filter(product__in=list1).values('product').annotate(
        Count('product')).filter(product__count__gt=0).order_by('product__count')[:5]
    leng = len(duplicate_names)
    list2 = []
    for i in range(leng):
        p = duplicate_names[i]['product']
        list2.append(p)
    trndng_pdts = Products.objects.filter(id__in=list2, is_deleted=False)

    context = {
        'vendors_count': vendors_count,
        'customers_count': customers_count,
        'best_service': best_service,
        'trending': trending,
        'poplr': poplr,
        'adds_count': adds_count,
        'trndng_pdts': trndng_pdts,
    }

    return render(request, 'admin/dashboard.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def services(request):
    category = Category.objects.filter(is_deleted=False)
    subcategory = Sub_category.objects.filter(is_deleted=False)
    print(subcategory)

    context = {
        "category": category,
        "subcategory": subcategory


    }
    if request.GET.get("data1"):
        values = request.GET['data1']
        print(values)
        dict1 = {}
        each_category = Sub_category.objects.get(category=values)
        dict1['id'] = each_category.pk
        dict1['name'] = each_category.subcategory_name

        return JsonResponse(dict1)

    return render(request, 'admin/services.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def banner(request):

    banner = Banner.objects.filter(is_deleted=False)
    banner_count = Banner.objects.all().count()
    context = {
        "banner": banner,
        "banner_count": banner_count,
        "CategoryList": Category.objects.filter(is_deleted=False)
    }

    if request.method == 'POST':
        name = request.POST.get("name")
        category = request.POST.get("category")
        image = request.POST.get("hidden-image")
        format, imgstr = image.split(';base64,')
        ext = format.split('/')[-1]
        print(category,"========================>>>>>>>>>>>")
        data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
        category_obj = Category.objects.get(id=category)
        banner = Banner(name=name, type="service", category=category_obj,image=data)
        banner.save()
        return HttpResponseRedirect(request.path_info)
    if request.GET.get("data1"):
        values = request.GET['data1']
        dict1 = {}
        each_banner = Banner.objects.get(id=values)
        dict1['id'] = each_banner.pk
        dict1['name'] = each_banner.name
        dict1['type'] = each_banner.type
        dict1['category_name'] = each_banner.category.category_name
        dict1['category_id'] = each_banner.category.id
        dict1['image'] = each_banner.image.url
        return JsonResponse(dict1)

    return render(request, 'admin/banner.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def create_category(request):
    if request.method == 'POST':
        name = request.POST.get("name")
        image = request.POST.get("hidden-category")
        format, imgstr = image.split(';base64,')
        ext = format.split('/')[-1]
        data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
        category = Category(category_name=name, thumbnail=data)
        category.save()
    if request.GET.get("data1"):
        values = request.GET['data1']
        dict1 = {}
        each_banner = Category.objects.get(id=values)
        dict1['id'] = each_banner.pk
        dict1['name'] = each_banner.category_name
        dict1['image'] = each_banner.thumbnail.url
        return JsonResponse(dict1)
    return redirect('services')


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def delete_category(request):
    if request.method == 'GET':
        get_id = request.GET['id']
        if Products.objects.filter(category__id=get_id,is_deleted=False).exists():
            return JsonResponse({'status':'no'})
        else:
            Category.objects.filter(id=get_id).update(is_deleted=True)
            return JsonResponse({'status':'yes'})




@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def delete_subcategory(request):
    if request.method == 'GET':
        get_id = request.GET['id']
        print(get_id,'testststsststt')
       
        if Products.objects.filter(subcategory__id=get_id,is_deleted=False).exists():
            return JsonResponse({'status':'no'})
        else:
            Sub_category.objects.filter(id=get_id).update(is_deleted=True)
            return JsonResponse({'status':'yes'})



@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def create_subcategory(request):
    category_id = request.POST.get("category", '')
    category_obj = Category.objects.get(id=category_id)
    subcategory = request.POST.get("sub-category", '')
    createsub = Sub_category(
        subcategory_name=subcategory, category=category_obj)
    createsub.save()
    return redirect('services')


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def edit_subcategory(request):

    if request.GET.get("varid"):
        varID = request.GET['varid']
        dict1 = {}
        subcategory = Sub_category.objects.get(id=varID)
        dict1['id'] = subcategory.pk
        dict1['name'] = subcategory.subcategory_name
        dict1['category_id'] = subcategory.category.pk
        dict1['category_name'] = subcategory.category.category_name
        print(dict1, 'mydict')

        return JsonResponse(dict1)

    if request.method == 'POST':
        get_category = request.POST.get("category", '')
        get_subcategory = request.POST.get("sub-category", '')
        get_subcategoryid = request.POST.get("sub-id", '')
        category_obj = Category.objects.get(id=get_category)
        Sub_category.objects.filter(id=get_subcategoryid).update(
            subcategory_name=get_subcategory, category=category_obj)
    return redirect('services')


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def service_request(request):
    if request.method == "POST":
        get_to_date = request.POST.get('toDate')
        get_from_date = request.POST['fromDate']
        from_date = datetime.strptime(
            get_from_date, "%d/%m/%Y").strftime('%Y-%m-%d')
        to_date = datetime.strptime(
            get_to_date, "%d/%m/%Y").strftime('%Y-%m-%d')
        print(from_date, to_date)
        product = Products.objects.filter(
            is_deleted=False, created_on__range=(from_date, to_date))
        context = {
            "product": product
        }

    else:

        product_running = Products.objects.filter(
            is_deleted=False, is_approved=True,vendor__is_suspended =False)
        product_rejected = Products.objects.raw("SELECT * FROM celebrations_products WHERE  (is_deleted = '0' AND is_approved='0' AND (is_rejected=1 OR update_rejected=1))")
        print('product rejected',product_rejected)
        product_approval = Products.objects.filter(
            is_deleted=False, is_approved=False, update_rejected=False, is_rejected=False, is_updated=False,vendor__is_suspended =False)
        product = Products.objects.filter(is_deleted=False)
        context = {
            "product": product,
            "running": product_running,
            "rejected": product_rejected,
            "approval": product_approval,
        }

    return render(request, 'admin/servicerequest.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def service_approve(request):
    if request.method == "GET":
        id = request.GET.get('id')

        Products.objects.filter(id=id).update(is_approved=True)

        actor = Morwagon_users.objects.get(user=request.user.id)
        vendor = Products.objects.get(id=id).vendor.id
        mor_user = [vendor, ]
        p_url = f"/vendor/ViewProduct/?pid={id}"
        save_notification(sndr=actor, rcpt=mor_user, msg=" Your service is approved ",
                          title="Service request approval", typ=p_url, get_id=0)

    if request.method == "POST":
        print("its in post")

        get_reason = request.POST.get("rejectReason", '')
        print(get_reason)
        get_id = request.POST.get("rejectId", '')
        actor = Morwagon_users.objects.get(user=request.user.id)
        vendor = Products.objects.get(id=get_id).vendor.id
        mor_user = [vendor, ]
        p_url = f"/vendor/ViewProduct/?pid={get_id}"
        save_notification(sndr=actor, rcpt=mor_user, msg=" Your service is Rejected ",
                          title="Service reject notification", typ=p_url, get_id=0)

        Products.objects.filter(id=get_id).update(
            reason=get_reason, is_rejected=True, is_approved=False)

    return redirect('service-request')


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def vendorlisting(request):
    vendors = Morwagon_users.objects.filter(
        role=2, is_deleted=False, approval=True)
    context = {
        "vendor": vendors
    }
    if request.method == 'POST':
        get_id = request.POST.get("id", '')

        Morwagon_users.objects.filter(id=get_id).update(is_deleted=True)

    return render(request, 'admin/vendorlisting.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def vendorapproval(request):
    if request.POST.get("accept-id"):

        get_id = request.POST.get("accept-id", '')

        Morwagon_users.objects.filter(id=get_id).update(approval=True)
        actor = Morwagon_users.objects.get(user=request.user.id)

        mor_user = [get_id]
        p_url = "/vendor/profile_vendor/"
        save_notification(sndr=actor, rcpt=mor_user, msg=" Your Registration is approved ",
                          title="Registration status", typ=p_url, get_id=0)
        return HttpResponseRedirect(request.path_info)

    if request.POST.get("reject-id"):

        get_id = request.POST.get("reject-id", '')
        get_reason = request.POST.get("reject-id", '')
        new_id = Morwagon_users.objects.get(id=get_id).user.id

        Morwagon_users.objects.filter(id=get_id).update(
            approval=False, is_rejected=True, reject_reason=get_reason,)
        actor = Morwagon_users.objects.get(user=request.user.id)

        mor_user = [get_id]
        p_url = "/vendor/profile_vendor/"
        save_notification(sndr=actor, rcpt=mor_user, msg=" Your Registration is rejected ",
                          title="Registration  status", typ=p_url, get_id=0)
        return HttpResponseRedirect(request.path_info)

    rejected_vendors = Morwagon_users.objects.filter(
        role=2, is_deleted=False, is_rejected=True,is_suspended=False)

    pending_vendors = Morwagon_users.objects.filter(
        role=2, is_deleted=False, approval=False, is_rejected=False,is_suspended =False)

    context = {
        "rejected": rejected_vendors,
        "pending": pending_vendors
    }
    return render(request, 'admin/vendorrequest.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def package(request):
    if request.method == 'POST':
        get_id = request.POST.get("id", '')

        Packages.objects.filter(id=get_id).update(is_deleted=True)

    package = Packages.objects.filter(is_deleted=False)
    context = {
        "package": package
    }
    return render(request, 'admin/package.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def package_create(request):
    if request.method == "POST":
        get_packagename = request.POST.get("package", '')
        get_price = request.POST.get("price", '')
        get_l_limit = request.POST.get("listing_limit", '')
        get_img = request.POST.get("image_limit", '')
        get_benefits = request.POST.get("benifits", '')
        get_duartion = request.POST.get("duration", '')
        get_videolimt = request.POST.get("videolimit", '')
        get_addlimit = request.POST.get("addlimit", '')
        package = Packages(package_name=get_packagename, price=get_price, product_listign_limit=get_l_limit, dispaly_image_limit=get_img,
                           benifits=get_benefits, months=get_duartion, add_limit=get_addlimit, video_limit=get_videolimt)
        package.save()
        return redirect('package')

    if request.GET.get("data"):
        get_package_name = request.GET['data']
        if Packages.objects.filter(package_name=get_package_name, is_deleted=False).exists():
            return JsonResponse({"message": "Successfully published", 'status': 200})
        else:
            return JsonResponse({"message": "Successfully published", 'status': 404})

    return render(request, 'admin/packagecreation.html')


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def package_edit(request, get_id):

    package = Packages.objects.get(id=get_id)
    context = {
        "package": package
    }
    if request.method == "POST":
        get_packagename = request.POST.get("package", '')
        get_price = request.POST.get("price", '')
        get_l_limit = request.POST.get("listing_limit", '')
        get_img = request.POST.get("image_limit", '')
        get_benefits = request.POST.get("benifits", '')
        get_duartion = request.POST.get("duration", '')
        get_videolimt = request.POST.get("videolimit", '')
        get_addlimit = request.POST.get("addlimit", '')
        Packages.objects.filter(id=get_id).update(package_name=get_packagename, price=get_price, product_listign_limit=get_l_limit,
                                                  dispaly_image_limit=get_img, benifits=get_benefits, months=get_duartion, add_limit=get_addlimit, video_limit=get_videolimt)
        return redirect('package')

    return render(request, 'admin/packagecreation.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def package_view(request, get_id):
    package = Packages.objects.get(id=get_id)
    context = {
        "package": package
    }
    return render(request, 'admin/packageview.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def package_upgrade(request):
    v_pending = Morwagon_users.objects.filter(
        role=2, is_deleted=False, is_updated=True,is_suspended=False)
    v_rejected = Morwagon_users.objects.filter(
        role=2, is_deleted=False, update_rejected=True)
    context = {
        "pending":  v_pending,
        "rejected": v_rejected

    }
    if request.method == 'POST':
        get_id = request.POST.get("rejectid", '')
        get_reason = request.POST.get("rejectReason", '')
        Morwagon_users.objects.filter(id=get_id).update(
            update_reject_reasson=get_reason, update_rejected=True, is_updated=False)
        actor = Morwagon_users.objects.get(user=request.user.id)
        mor_user = [get_id]
        p_url = "/vendor/profile_vendor/"
        save_notification(sndr=actor, rcpt=mor_user, msg=" Your Update is rejected ",
                          title="Update status", typ=p_url, get_id=0)

    return render(request, 'admin/vendorupgrade.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def accept_upgrade(request):
    print(request.POST)
    get_id = request.POST['acceptupdate-id']
    print(get_id)
    # # get_package_id = request.POST.get("newpackage",'')
    # # package  = Packages.objects.get(id=get_package_id)
    # # morwagon_user = Morwagon_users.objects.get(id =get_id)
    # # end_date = morwagon_user.expirydate + relativedelta(months=package.months)
    Morwagon_users.objects.filter(id=get_id).update(
        approval=True, is_updated=False)
    actor = Morwagon_users.objects.get(user=request.user.id)
    mor_user = [get_id]
    p_url = "/vendor/profile_vendor/"
    save_notification(sndr=actor, rcpt=mor_user, msg=" Your Update is Accepted ",
                      title="Update status", typ=p_url, get_id=0)
    return redirect('package-upgrade')


# def edit_vendor(request): # pending
#     vendors = Morwagon_users.objects.filter(role = "vendor",is_deleted ='False',user__is_active ="True",is_updated='True')
#     context={
#         "vendor": vendors
#     }
#     if request.method == 'POST':
#         get_id = request.POST.get("id",'')

#         Morwagon_users.objects.filter(id = get_id).update (is_deleted ='True',)


#     return render(request,'admin/vendorseditrequest.html',context)

@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def productview(request, get_id):
    SELECTUNIT=PriceUnit.objects.filter(is_deleted=False,product=get_id)

    # SELECTUNIT=((1, "Per day"), (2, "Per hour"), (3, "Per person"), (4, "Per unit"), (5, "Per pair"))

    product = Products.objects.get(id=get_id)
    if Album.objects.filter(service=product.id).exists():
        album = Album.objects.get(service=product.id)
        album_details = Album_details.objects.filter(album=album,is_deleted=False)
        print( album_details)
    else:
        album_details = {}
    if Videos.objects.filter(service=product.id).exists():
        videos = Videos.objects.filter(service=product.id)

    else:
        videos = {}

    context = {
        "videos": videos,
        "product": product,
        "album": album_details,
        'SELECTUNIT': SELECTUNIT
    }
    return render(request, 'admin/service-details.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def approve_albumimgae(request, get_id):

    Album_details.objects.filter(id=get_id).update(is_approved=True)
    id = Album_details.objects.get(id=get_id).album.id

    product_id = Album.objects.get(id=id).service.id

    return redirect('product-view', product_id)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def editserviceapproval(request):
    if request.method == 'POST':

        get_reason = request.POST.get("rejectReason", '')
        get_id = request.POST.get("rejectId", '')
        Products.objects.filter(id=get_id).update(
            update_reject_reason=get_reason, is_approved=False, update_rejected=True)
        actor = Morwagon_users.objects.get(user=request.user.id)
        vendor = Products.objects.get(id=get_id).vendor.id
        mor_user = [vendor, ]
        p_url = f"/vendor/ViewProduct/?pid={get_id}"
        save_notification(sndr=actor, rcpt=mor_user, msg=" Your service is approved ",
                          title="Service request approval", typ=p_url, get_id=0)

    productforupdate = Products.objects.filter(
        is_updated=True, is_deleted=False, is_rejected=False, update_rejected=False)
    updaterejected = Products.objects.filter(
        is_updated=True, is_deleted=False, update_rejected=True)

    context = {
        "update": productforupdate,
        "rejected": updaterejected


    }
    return render(request, 'admin/editedvendorserviceapproval.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def addlist(request):
    products = Products.objects.filter(is_deleted=False, is_approved=True)
    adds = Adds.objects.filter(is_deleted=False)
    context = {
        "products": products,
        "adds": adds

    }
    if request.method == 'POST':
        print("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")

        reason = request.POST['text']
        id = request.POST['varid']
        new_id = id.split('#edit-image')[1]
        Adds.objects.filter(id=new_id).update(
            reject_reason=reason, is_approved=False)

    return render(request, 'admin/addaprove.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def edit_banner(request):
    print("hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii")
    print(request.POST)

    image = request.POST.get("hidden-imageedit")
    print(image, 'imagellllllllllllllllllllllllllllllllll')
    get_name = request.POST.get("editBannerName")
    get_category = request.POST.get("editBannerCategory")
    get_cat_byid=Category.objects.get(id=get_category)
    print(get_name, 'nsmeeeeeeeeeeeeeeeeeeeeeeeee')
    get_id = request.POST.get("edit-id-banner")

    if request.POST.get("hidden-imageedit"):
        print("its in post")
        format, imgstr = image.split(';base64,')
        ext = format.split('/')[-1]
        data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
        banner = Banner.objects.get(id=get_id)
        print(banner.id, 'jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj')
        banner.image = data
        banner.save()

    Banner.objects.filter(id=get_id).update(name=get_name,category=get_cat_byid)
    return redirect("banner")


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def bannerimgaeview(request, get_id):

    banner = Banner.objects.get(id=get_id)
    context = {
        'banner': banner
    }
    return render(request, 'admin/bannerimageview.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def banner_delete(request):

    getId = request.POST.get('id')

    Banner.objects.filter(id=getId).update(is_deleted=True)
    return redirect('banner')


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def editcategory(request):
    image = request.POST.get("hidden-categoryedit")
    get_name = request.POST.get("name")
    get_id = request.POST.get("edit-id-category")

    if request.POST.get("hidden-categoryedit"):
        format, imgstr = image.split(';base64,')
        ext = format.split('/')[-1]
        data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
        category = Category.objects.get(id=get_id)
        category.thumbnail = data
        category.save()
    Category.objects.filter(id=get_id).update(category_name=get_name,)
    return redirect("services")


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def addapprove(request):
    if request.method == 'POST':
        id = request.POST['varid']
        new_id = id.split('#edit-image')[1]
        Adds.objects.filter(id=new_id).update(is_approved=True)
        addobj = Adds.objects.get(id='new_id').product.id
        actor = Morwagon_users.objects.get(user=request.user.id)

        vendor = Products.objects.get(id=addobj).vendor.id
        mor_user = [vendor, ]
        p_url = f"/vendor/ViewProduct/?pid={addobj}"
        save_notification(sndr=actor, rcpt=mor_user, msg=" view your approved add ",
                          title="Add approval", typ=p_url, get_id=0)

        return redirect("add-approve")


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def videoapprove(request, get_id):
    Videos.objects.filter(id=get_id).update(is_approved=True)
    return_id = Videos.objects.get(id=get_id).service.id
    return redirect('product-view', return_id)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def productreview(request):
    approvedReview=Review.objects.filter(is_deleted=False,approvel_sts=1)
    pendingReview=Review.objects.filter(is_deleted=False,approvel_sts=0)
    rejectedReview=Review.objects.filter(is_deleted=False,approvel_sts=2)
    flaggedReview=Review.objects.filter(is_deleted=False,flag=True)
    context = {
        'approved': approvedReview,
        'pending':pendingReview,
        'rejected':rejectedReview,
        'flagged':flaggedReview,

    }
    # if request.method == "POST":
    #     get_to_date = request.POST.get('toDate')
    #     get_from_date = request.POST.get('fromDate')
    #     from_date = datetime.datetime.strptime(
    #         get_from_date, "%d/%m/%Y").strftime('%Y-%m-%d')
    #     to_date = datetime.datetime.strptime(
    #         get_to_date, "%d/%m/%Y").strftime('%Y-%m-%d')
    #     review = Review.objects.filter(
    #         is_deleted=False, created_on__range=(from_date, to_date))
    #     context = {

    #     }

    return render(request, 'admin/reviews.html', context)

def reviewFilter(request):
    get_to_date = request.POST.get('toDate')
    get_from_date = request.POST['fromDate']
    id_hidden = int(request.POST.get('id_hidden'))
    from_date = datetime.datetime.strptime(
        get_from_date, "%d/%m/%Y").strftime('%Y-%m-%d %H:%M:%S.%f')
    to_date = datetime.datetime.strptime(get_to_date, "%d/%m/%Y").strftime(
        '%Y-%m-%d %H:%M:%S.%f').replace("00:00:00.000000", "23:59:59.000000")
    product = ""
    if id_hidden == 1:
        product = Review.objects.filter(is_deleted=False, is_approved=True, created_on__gte=from_date, created_on__lte=to_date,).values(
            'id', 'product_name', 'category__category_name', 'subcategory__subcategory_name', 'vendor__first_name', 'created_on', 'image')
    if id_hidden == 2:
        product = Review.objects.filter(is_deleted=False, is_approved=False, update_rejected=False, is_rejected=False).filter(created_on__gte=from_date, created_on__lte=to_date).values(
            'id', 'product_name', 'category__category_name', 'subcategory__subcategory_name', 'vendor__first_name', 'created_on', 'image')
    if id_hidden == 3:
        product = Review.objects.filter(Q(is_deleted=False, is_approved=False) | Q(Q(is_rejected=True) | Q(update_rejected=True))).filter(
            created_on__gte=from_date, created_on__lte=to_date).values('id', 'product_name', 'category__category_name', 'subcategory__subcategory_name', 'vendor__first_name', 'created_on', 'image')
    x1 = list(product)
    x = {'id': x1}
    return JsonResponse(x)    

@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def reviewFlagSuspendVendor(request,id):
    obj=Review.objects.get(id=id)
    obj.approvel_sts=2
    obj.reject_reason="Flagged Review"
    obj.flag=False
    use=obj.user
    use.is_suspended=True
    use.save()
    obj.save()
    return redirect('/review')


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def review(request, id):
    review=Review.objects.get(id=id)
    img=Review_images.objects.filter(review=id)
    context={
        'review':review,
        'reviewimages_attribute':img,
    }
    return render(request, 'admin/reviewView.html', context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def deleteReviewimage(request, get_id):

    Review_images.objects.filter(id=get_id).update(is_deleted=True)

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def approvereview(request, get_id):

    Review.objects.filter(id=get_id).update(approvel_sts=1)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def rejectreview(request, id):
    obj=Review.objects.get(id=id)
    obj.reject_reason=request.POST.get('rejectReason')
    obj.approvel_sts=2
    obj.flag=False
    obj.save()
    # Review.objects.filter(id=id).update(approvel_sts=2)
    return redirect('/review')


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def startreview(request):
    if request.GET.get("succuss"):

        varID = request.GET['succuss']

        if int(varID) == 1:

            if Settings.objects.get(name='review').need_approval == False:
                Settings.objects.filter(
                    name='review').update(need_approval=True)
            else:
                Settings.objects.filter(
                    name='review').update(need_approval=False)
        dict1 = {'succuss': 'succuss'}
    return JsonResponse(dict1)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def checkback(request):

    if request.GET.get("data"):
        varID = request.GET['data']
        if Category.objects.filter(category_name=varID, is_deleted=False).exists():
            return JsonResponse({"messages": 333})
        return JsonResponse({"messages": 555})


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def checkback1(request):
    if request.GET.get("data"):
        varID = request.GET['data']
        if Sub_category.objects.filter(subcategory_name=varID, is_deleted=False).exists():
            return JsonResponse({"messages": 333})
        return JsonResponse({"messages": 555})


def admin_login(request):
    if request.method == 'GET' and request.user.id:
        if Morwagon_users.objects.filter(user=request.user.id, role=2, is_done=True).exists():
            return redirect('VendorDashboard')
        elif Morwagon_users.objects.filter(user=request.user.id, role=1).exists():
            return redirect('dashboard')
        else:
            return render(request, 'admin/admin_login.html')

    else:
        if request.method == 'POST':
            error = ''
            get_mail = request.POST.get("email")
            get_passowrd = request.POST.get("password")
            if get_mail == '':
                error = "The Email ID field is required"
                context = {
                    "error": error
                }
                return render(request, 'admin/admin_login.html', context)
            users_with_reports = User.objects.filter(username=get_mail)
            print(users_with_reports.count(), 'kkk')
            if users_with_reports.count() == 0:
                error = " This Email does not exists "
                context = {
                    "error": error
                }
                return render(request, 'admin/admin_login.html', context)
            if get_passowrd == '':
                error = "The Password field is required"
                context = {
                    "error": error
                }
                return render(request, 'admin/admin_login.html', context)
            if get_mail == '' and get_passowrd == '':
                error = "Fields cannot be null "
                context = {
                    "error": error
                }
                return render(request, 'admin/admin_login.html', context)
            try:
                user = authenticate(username=get_mail, password=get_passowrd)
                if Morwagon_users.objects.get(user=user.id).is_suspended ==True:
                    error = "This user is suspended "
                    context = {
                        "error": error
                    }
                    return render(request, 'admin/admin_login.html', context)
                else:   

                    if Morwagon_users.objects.get(user=user.id).role == 1:
                        login(request, user)
                        messages.success(request, 'Login  success.')
                        return redirect('dashboard')
                    if Morwagon_users.objects.get(user=user.id).role == 2:
                        if Morwagon_users.objects.get(user=user.id).is_done == True:
                            login(request, user)
                            messages.success(request, 'Login  success.')
                            return redirect("VendorDashboard")
                        else:
                            print(user,'inlogin sandraaa')
                            print(user.id,'id login sandraaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                            id = user.id
                            page = Morwagon_users.objects.get(user=user.id).page_no
                            pages = page+1
                            return redirect('/vendor/Vendorswal/'+str(pages)+'/'+str(id))
            except:
                error = "Incorrect password"
                context = {
                    "error": error
                }
                return render(request, 'admin/admin_login.html', context)
        return render(request, 'admin/admin_login.html')


@login_required(login_url='/login')
def logout_admin_vendor(request):
    list(messages.get_messages(request))
    auth.logout(request)
    return redirect('/login')


@login_required(login_url='login/admin')
@user_passes_test(lambda u: u.is_superuser, login_url='login/admin')
def packagecheck(request):
    id = request.GET['varid']
    new = Packages.objects.get(id=id)
    users_mor = Morwagon_users.objects.filter(packages=new.id)
    print(users_mor)
    now = datetime.datetime.utcnow().replace(tzinfo=utc)
    print(now, 'now')
    check = 0
    for each in users_mor:
        if each.expirydate < now:
            check = 1
            break
    if check == 1:
        return JsonResponse({'new': "expired"})

    else:

        return JsonResponse({'new': "pending"})


def picknotification(request):

    id = request.user.id
    mor = Morwagon_users.objects.get(user=id)
    noti = Notification.objects.filter(recipient=mor.id, read=False).order_by('-id')
    for i in noti:
        print(i.recipient, 'kioki')
    qs_json = serializers.serialize("json", noti)
    return HttpResponse(qs_json, content_type='application/json')


def notification_details(request):
    id = request.user.id
    mor = Morwagon_users.objects.get(user=id)
    noti = reversed(Notification.objects.filter(recipient=mor.id))

    contetx = {
        "notification": noti,

    }
    return render(request, 'admin/notification.html', contetx)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def serviceedit_approve(request, id):
    if request.method == "GET":
        get_id = id

        Products.objects.filter(id=get_id).update(
            is_approved=True, is_updated=False)

        actor = Morwagon_users.objects.get(user=request.user.id)
        vendor = Products.objects.get(id=get_id).vendor.id
        mor_user = [vendor, ]
        p_url = f"/vendor/ViewProduct/?pid={id}"
        save_notification(sndr=actor, rcpt=mor_user, msg=" Your service edit is approved ",
                          title="Service edit approval", typ=p_url, get_id=0)
    return redirect(request.META['HTTP_REFERER'])


@login_required(login_url='/login')
def notification_change(request):
    if request.method == 'GET':
        id = request.GET['varid']
        Notification.objects.filter(id=id).update(read=True)
        return JsonResponse({'done': "done"})


def ServiceFilter(request):
    get_to_date = request.POST.get('toDate')
    get_from_date = request.POST['fromDate']
    id_hidden = int(request.POST.get('id_hidden'))
    from_date = datetime.datetime.strptime(
        get_from_date, "%d/%m/%Y").strftime('%Y-%m-%d %H:%M:%S.%f')
    to_date = datetime.datetime.strptime(get_to_date, "%d/%m/%Y").strftime(
        '%Y-%m-%d %H:%M:%S.%f').replace("00:00:00.000000", "23:59:59.000000")
    product = ""
    if id_hidden == 1:
        product = Products.objects.filter(is_deleted=False, is_approved=True, created_on__gte=from_date, created_on__lte=to_date,).values(
            'id', 'product_name', 'category__category_name', 'subcategory__subcategory_name', 'vendor__first_name', 'created_on', 'image')
    if id_hidden == 2:
        product = Products.objects.filter(is_deleted=False, is_approved=False, update_rejected=False, is_rejected=False).filter(created_on__gte=from_date, created_on__lte=to_date).values(
            'id', 'product_name', 'category__category_name', 'subcategory__subcategory_name', 'vendor__first_name', 'created_on', 'image')
    if id_hidden == 3:
        product = Products.objects.filter(Q(is_deleted=False, is_approved=False) | Q(Q(is_rejected=True) | Q(update_rejected=True))).filter(
            created_on__gte=from_date, created_on__lte=to_date).values('id', 'product_name', 'category__category_name', 'subcategory__subcategory_name', 'vendor__first_name', 'created_on', 'image')
    x1 = list(product)
    x = {'id': x1}
    return JsonResponse(x)


def UpdateServiceFilter(request):
    get_to_date = request.POST.get('toDate')
    get_from_date = request.POST['fromDate']
    id_hidden = int(request.POST.get('id_hidden'))
    from_date = datetime.datetime.strptime(
        get_from_date, "%d/%m/%Y").strftime('%Y-%m-%d %H:%M:%S.%f')
    to_date = datetime.datetime.strptime(
        get_to_date, "%d/%m/%Y").strftime('%Y-%m-%d %H:%M:%S.%f')
    product = ""
    print("llllllllllllllllllllllllllllllll555555555555555555555555555555556666666666666666666666666")
    if id_hidden == 1:
        product = Products.objects.filter(is_updated=True, is_deleted=False, is_approved=False, update_rejected=False, is_rejected=False).filter(
            created_on__gte=from_date, created_on__lte=to_date).values('id', 'product_name', 'category__category_name', 'subcategory__subcategory_name', 'vendor__first_name', 'created_on', 'image')
    if id_hidden == 2:
        # product = Products.objects.filter(Q(is_deleted =False,is_approved=False)|Q( Q(is_rejected=True)|Q(update_rejected=True))).filter(created_on__gte= from_date, created_on__lte=to_date).values('id','product_name','category__category_name','subcategory__subcategory_name','vendor__first_name','created_on','image')
        # product = Products.objects.filter(Q(is_deleted =False,is_approved=False)|Q(update_rejected=True)).filter(created_on__gte= from_date, created_on__lte=to_date).values('id','product_name','category__category_name','subcategory__subcategory_name','vendor__first_name','created_on','image')
        product = Products.objects.filter(is_deleted=False, is_approved=False, update_rejected=True).filter(created_on__gte=from_date, created_on__lte=to_date).values(
            'id', 'product_name', 'category__category_name', 'subcategory__subcategory_name', 'vendor__first_name', 'created_on', 'image')
    x1 = list(product)
    x = {'id': x1}
    return JsonResponse(x)

# //////////////////////////////////////////////////////////////////////////


@login_required(login_url='login/admin')
@user_passes_test(lambda u: u.is_superuser, login_url='login/admin')
def profile_vendor_admin(request, vendor_id):
    context = {'vendor': Morwagon_users.objects.get(id=vendor_id)}
    print(context, "jjjjjjjjjjjjj")
    return render(request, "admin/vendor_profile_admin.html", context)


@login_required(login_url='login/admin')
@user_passes_test(lambda u: u.is_superuser, login_url='login/admin')
def edit_vendor_admin(request, vendor_id):
    print(vendor_id,"11111111111111111111111111qqqqqqqqq")
    if request.method == 'POST':

        first_name = request.POST.get('first_name')
        print(first_name, "nameeeeeeeeeeeeeeeee")
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        mobile = request.POST.get('mobile')
        whatsapp_number = request.POST.get('whatsapp')
        address1 = request.POST.get('address1')
        address2 = request.POST.get('address2')
        town = request.POST.get('town')
        district = request.POST.get('district')
        state = request.POST.get('state')
        pincode = request.POST.get('pincode')
        location = request.POST.get('location')
        store_name = request.POST.get('store_name')

        latitude = request.POST.get('latitude')
        logitude = request.POST.get('longitude')

        logo_image = request.FILES.get('logo_image')
        print(logo_image, "aaaaaaaaaaaaa")
        thumbnail = request.FILES.get('thumbnail')
        print(thumbnail, "tttttt")
        caption = request.POST.get('caption')
        Description = request.POST.get('Description')
        incorp_cirtificate = request.FILES.get('incorp_cirtificate')
        gst = request.FILES.get('gst')
        pancard = request.FILES.get('pancard')
        address_proof = request.FILES.get('address_proof')
        gst_number = request.POST.get('gst_number')
        userrecord = Morwagon_users.objects.get(id=vendor_id)
        userrecord.first_name = first_name
        userrecord.last_name = last_name
        userrecord.email = email
        userrecord.username = email
        userrecord.is_active = False
        # userrecord=User.objects.create_user(first_name=first_name,last_name=last_name,email=email,username=email,password=password,is_staff=1,user__is_active=False)
        Morwagon_users.objects.filter(id=vendor_id).update(first_name=first_name,
                                                             last_name=last_name, email=email, mobile_number=mobile, role=2, town=town,
                                                             whatsapp_number=whatsapp_number, address1=address1, address2=address2,
                                                             district=district, state=state, pincode=pincode, location=location,
                                                             store_name=store_name, caption=caption, Description=Description, gst_number=gst_number, latitude=latitude, logitude=logitude)

        mor_user = Morwagon_users.objects.get(id=vendor_id)
        if logo_image is not None:
            if mor_user.logo_image.name is not '':
                mor_user.logo_image.storage.delete(mor_user.logo_image.name)
            mor_user.logo_image = logo_image
        if thumbnail is not None:
            if mor_user.thumbnail.name is not '':
                mor_user.thumbnail.storage.delete(mor_user.thumbnail.name)
            mor_user.thumbnail = thumbnail
        if incorp_cirtificate is not None:
            if mor_user.incorporation_certificate.name is not '':
                mor_user.incorporation_certificate.storage.delete(
                    mor_user.incorporation_certificate.name)
            mor_user.incorporation_certificate = incorp_cirtificate
        if gst is not None:
            mor_user.GST.storage.delete(mor_user.GST.name)
            mor_user.GST = gst
        if pancard is not None:
            mor_user.pancard.storage.delete(mor_user.pancard.name)
            mor_user.pancard = pancard
        if address_proof is not None:
            mor_user.address_proof.storage.delete(mor_user.address_proof.name)
            mor_user.address_proof = address_proof
        mor_user.save()
        return redirect("/vendors")
    context = {'vendor': Morwagon_users.objects.get(id=vendor_id)}
    print("context", "connnnnnnnnnnnnn")
    return render(request, "admin/edit_vendor_admin.html", context)


@login_required(login_url='/login')
@user_passes_test(lambda u: u.is_superuser, login_url='/login')
def profile_admin(request):
    admin_id = request.user.id
    context = {'admin': Morwagon_users.objects.get(user=admin_id)}
    return render(request, "admin/admin_profile.html", context)


def delete_vendor(request):
    id = request.POST.get('id')
    Morwagon_users.objects.filter(user=id).update(is_deleted=True)
    return HttpResponseRedirect(request.path_info)


def vendorlistingfilter(request):

    if request.method == "POST":
        get_to_date = request.POST.get('toDate')
        get_from_date = request.POST['fromDate']
        from_date = datetime.datetime.strptime(
            get_from_date, "%d/%m/%Y").strftime('%Y-%m-%d')
        to_date = datetime.datetime.strptime(
            get_to_date, "%d/%m/%Y").strftime('%Y-%m-%d')
        print(from_date, to_date)
        vendor = Morwagon_users.objects.filter(
            is_deleted=False, approval=True, role=2, created_on__range=(from_date, to_date),)
        context = {
            "vendor": vendor
        }
    return render(request, 'admin/vendorlisting.html', context)
# def servicerequestfilter(request):

#     if request.method == "POST":
#         get_to_date = request.POST.get('toDate')
#         get_from_date = request.POST['fromDate']
#         id_hidden = int(request.POST['find'])
#         print(id_hidden)
#         from_date =datetime.datetime.strptime(get_from_date, "%d/%m/%Y").strftime('%Y-%m-%d')
#         to_date = datetime.datetime.strptime(get_to_date, "%d/%m/%Y").strftime('%Y-%m-%d')

#         running={}
#         rejected={}
#         approval={}
#         if id_hidden == 1:
#             running  = Products.objects.filter(is_deleted =False,is_approved=True,created_on__range=(from_date, to_date))
#             print(1,'one')
#         if id_hidden == 2:
#             rejected = Products.objects.filter(is_deleted =False,is_approved=False,update_rejected=False,is_rejected=False,created_on__range=(from_date, to_date))
#             print(2,'two')
#         if id_hidden == 3:
#             approval = Products.objects.filter(Q(is_deleted =False,is_approved=False)|Q( Q(is_rejected=True)|Q(update_rejected=True))).filter(created_on__range=(from_date, to_date))
#             print(3,'three')
#         context={
#             "running":running,
#             "rejected":rejected,
#             "approval":approval
#         }
#         print(context)
#     return render(request,'admin/servicerequest.html',context)


def add_reject(request):
    print("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj")
    print(request.POST)

    return HttpResponseRedirect(request.path_info)


def add_tax(request):
    tax = Tax_details.objects.filter(is_deleted=False)
    context = {
        'tax': tax,
    }
    return render(request, 'admin/add_tax.html', context)


def createtax(request):
    print(request.POST)
    taxname = request.POST['taxname']
    get_percentage = request.POST['Percentage']
    if request.POST.get('radio'):
        get_flag = request.POST['radio']
    else:
        get_flag = 0
    Tax_details.objects.create(
        tax_name=taxname, percentage=get_percentage, flag=get_flag)
    return redirect('tax')


def get_taxdetails(request):

    if request.method == 'GET':
        data = request.GET['data1']
        print(data)
        dict1 = {}
        each_tax = Tax_details.objects.get(id=data)
        dict1['id'] = each_tax.pk
        dict1['name'] = each_tax.tax_name
        dict1['flag'] = each_tax.flag
        dict1['percentage'] = each_tax.percentage
        
        return JsonResponse(dict1)


def edit_tax(request):
    print(request.POST)
    name = request.POST['edittaxname']
    get_percentage = request.POST['editpercentage']
    get_id = request.POST['edit-id-tax']

    if request.POST.get('editradio'):

        get_flag = request.POST['editradio']
    else:
        get_flag = 0
    Tax_details.objects.filter(id=get_id).update(
        tax_name=name, percentage=get_percentage, flag=get_flag)
    return redirect("tax")


def delete_tax(request):
    get_id = request.POST['delete_tax']
    Tax_details.objects.filter(id=get_id).update(is_deleted=True)

    return redirect("tax")

def edit_services(request, id):
    print("inside edit_service function...............................O")
    edit_product=Products.objects.get(id=id)
    opening_Time=edit_product.opening_Time
    closing_Time=edit_product.closing_Time
    Otime=opening_Time
    Ctime=closing_Time
    _category=edit_product.category
    context = {'Ctime':Ctime,'Otime':Otime,'EditId':id,'EditList':edit_product,'CategoryList': Category.objects.filter(is_deleted=False),'SubCategoryList':Sub_category.objects.filter(category=_category)}
    return render(request, 'admin/editvendorproduct.html', context)


def editbyadmin(request,id):
    if request.method=='POST':
        r=request.POST
        data =  json.dumps(r)
        data = json.loads(data)
        service_name=request.POST.get('service_name')
        unit_lst=data['unit_lst']
        _location=data['_location']
        cke_description=data['cke_description']
        lat=data['lat']
        lng=data['lng']
        category1=data['category1']
        subcategory=data['subcategory']
        opening_time=data['opening_time']
        closing_time=data['closing_time']
        phonenmbr=data['phonenmbr']
        address=data['address']
        textappointment=data['textappointment']
        textmenu=data['textmenu']
        img=data['img']
        editid= data['editid']
        vendor=request.user
        vid=Morwagon_users.objects.get(user=vendor)
        unitPriceJson=json.loads(unit_lst)
        priceunitobj=[]
        if data['editid']!='':
            editid=request.POST.get('editid')
            product_obj = Products.objects.get(pk=editid)
            priceunitobj=list(PriceUnit.objects.filter(product=product_obj.id).values_list('id',flat=True))
            for i in unitPriceJson:
                unit=i['unit']
                field1=i['field1']
                field2=i['field2']
                field3=i['field3']
                checkbox=i['checkbox']
                if(checkbox==1):
                    checkbox=0
                else:
                    checkbox=1
                if (i['unit']!='' and ( i['field1']!='' or( i['field2']!='' and i['field2']!='') )):
                    priceunitsaveobj=PriceUnit(unit_id=unit,product_id=product_obj.id,is_fixedprice=checkbox)
                    priceunitsaveobj.save()
                    updateId=priceunitsaveobj.id
                    obj=PriceUnit.objects.get(id=updateId)
                    if field1:
                        obj.price=field1
                    if field2:
                        obj.from_price=field2
                    if field3:
                        obj.to_price=field3
                    obj.save()
                    PriceUnit.objects.filter(id__in=priceunitobj).delete()
            if data['img']:
                image=data['img']
                format, imgstr = image.split(';base64,') 
                ext = format.split('/')[-1] 
                img = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
                product_obj.image = img
                product_obj.save()
            # Products.objects.filter(id=editid).update(address=address,phone=phonenmbr,appointments=textappointment,menu=textmenu,location=_location,product_name=service_name,opening_Time=opening_time, closing_Time=closing_time ,category_id=category1 ,subcategory_id=subcategory, description=cke_description,vendor=vid,is_approved=False,is_updated=True,is_rejected=False,update_rejected=False,lat=lat,lng=lng)

            Products.objects.filter(id=editid).update(address=address,phone=phonenmbr,appointments=textappointment,menu=textmenu,location=_location,product_name=service_name,opening_Time=opening_time, closing_Time=closing_time ,category_id=category1 ,subcategory_id=subcategory, description=cke_description,vendor=vid,lat=lat,lng=lng)
            superusers_id = list( User.objects.filter(is_superuser=True).values_list('id',flat=True))
            mor_user = list(Morwagon_users.objects.filter(user__in=superusers_id).values_list('id',flat=True))
            actor = Morwagon_users.objects.get(user =request.user)
            p_url = "/service/requests"
            save_notification(sndr=actor,rcpt=mor_user,msg="Product Edited",title="Product edit approval",typ=p_url,get_id=0)
            return redirect('/service/requests')
        # format, imgstr = img.split(';base64,') 
        # ext = format.split('/')[-1] 
        # img = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
        # save_pdts=Products(address=address,phone=phonenmbr,appointments=textappointment,menu=textmenu,location=_location,product_name=service_name,image=img, opening_Time=opening_time, closing_Time=closing_time ,category_id=category1 ,subcategory_id=subcategory, description=cke_description,vendor=vid,is_approved=False,lat=lat,lng=lng)
        # save_pdts.save()
        # save_id=save_pdts.id
        # for i in unitPriceJson:
        #     unit=i['unit']
        #     field1=i['field1']
        #     field2=i['field2']
        #     field3=i['field3']
        #     checkbox=i['checkbox']
        #     if(checkbox==1):
        #             checkbox=0
        #     else:
        #         checkbox=1
        #     priceunitsaveobj=PriceUnit(unit_id=unit,product_id=save_id,is_fixedprice=checkbox)
        #     priceunitsaveobj.save()
        #     updateId=priceunitsaveobj.id
        #     obj=PriceUnit.objects.get(id=updateId)
        #     if field1:
        #         obj.price=field1
        #     if field2:
        #         obj.from_price=field2
        #     if field3:
        #         obj.to_price=field3
        #     obj.save()
    #     superusers_id = list( User.objects.filter(is_superuser=True).values_list('id',flat=True))
    #     mor_user = list(Morwagon_users.objects.filter(user__in=superusers_id).values_list('id',flat=True))
    #     actor = Morwagon_users.objects.get(user =request.user)
    #     p_url = "/package/upgrade"
    #     save_notification(sndr=actor,rcpt=mor_user,msg="New product created",title="Product creation",typ=p_url,get_id=0)
    # context = {'CategoryList': Category.objects.filter(is_deleted=False),'hide_id':0,'units':Units.objects.filter(is_deleted=False)}
    # return render(request, "vendor/productDetails.html",context)

# def edit_services(request, id):
#     product = Products.objects.get(id=id)
#     SELECTUNIT=PriceUnit.objects.filter(is_deleted=False,product=id)

#     # SELECTUNIT=((1, "Per day"), (2, "Per hour"), (3, "Per person"), (4, "Per unit"), (5, "Per pair"))
# # ghhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
#     if request.method == 'POST':
#         product_id = request.POST['p_id']
#         print(request.POST)
#         product_name = request.POST['pdtName']
#         price = request.POST['price']
#         category = request.POST['category']
#         subcategory = request.POST['subcategory']
#         description = request.POST['content']
#         unit = request.POST['unit']

#         lng = request.POST['lng']
#         lat = request.POST['lat']
#         location = request.POST['location']
#         print(id, 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk')
#         Products.objects.filter(id=product_id).update(product_name=product_name, price=price, category=category,
#                                                       subcategory=subcategory, description=description, unit=unit, lng=lng, lat=lat, location=location)
#         return redirect('service-request')
#     _category=product.category
#     context = {
#         "SubCategoryList":Sub_category.objects.filter(category=_category),
#         "EditList": product,
#         "CategoryList": Category.objects.filter(is_deleted=False),
#         "unit": SELECTUNIT
#     }
#     return render(request, 'admin/editvendorproduct.html', context)


def getcategory(request):
    id = request.GET['varid']
    category_obj = Category.objects.get(id=id)
    vds = Sub_category.objects.filter(category=category_obj)
    qs_json = serializers.serialize("json", vds)
    return HttpResponse(qs_json, content_type='application/json')


def prodct_status(request):

    id = request.GET['varid']
    product = Products.objects.get(id=id)
    if product.is_suspended == True:
        Products.objects.filter(id=id).update(is_suspended=False)
    else:
        Products.objects.filter(id=id).update(is_suspended=True)
    return JsonResponse({'done': 'done'})
def vendor_suspend_revoke(request):
    id = request.GET['varid']
    mor =Morwagon_users.objects.get(id=id)
    if mor.is_suspended == True:
        Morwagon_users.objects.filter(id=id).update(is_suspended=False)
    else:
        Morwagon_users.objects.filter(id=id).update(is_suspended=True)
    return JsonResponse({'done': 'done'})



# ////////////////////////////////////////////////////////////////////////// new suggestions from april 28

@login_required(login_url='login/admin')
@user_passes_test(lambda u: u.is_superuser, login_url='login/admin')
def report_admin_customer(request):#report vendor list view
    print("88888888888888888888888888888888888888888888888888888888888888")
    #Case(When(user_report__is_deleted=False)))
    # report_vendor=Morwagon_users.objects.filter(is_deleted=False,is_suspended=False).annotate(count=Count('user_report',filter=Q(user_report__is_deleted=False))).filter(count__gt=0).annotate(flag_count=Count('user_report',filter=Q(user_report__flag=True)))
    # report_vendor=Morwagon_users.objects.filter(is_deleted=False,is_suspended=False).annotate(count=Count('user_report',filter=Q(user_report__is_deleted=False))).filter(count__gt=0).annotate(flag_count=Count('user_report',filter=Q(user_report__flag=True)))
    report_vendor=Report_vendor.objects.filter(is_deleted=False,vendor__is_suspended=False)
    # print(report_vendor,"=================")
    # lent=len(report_vendor)
    # st=Morwagon_users.objects.none()  #empty query
    
    # for i in range(0,lent):
    #     st= st | report_vendor[i].user_report.all().filter(is_deleted=False)
    # report_vendor=st.annotate(count=Count('vendor')).filter(count__gt=0)
    
    # print(report_vendor,111111111111)
    # print(type(report_vendor))
    # print(report_vendor[1].user_report.all().filter(is_deleted=True),'1111111111111111111')
    
    # print(type(query))
    # report_vendor=Report_vendor.objects.filter(is_deleted=False,vendor__is_suspended=False).annotate(count=Count('vendor_id'))
    # print(report_vendor[0].user_report.all()[0].description,"mmmmmmmmmmm")
    context = {'report_vendor': report_vendor}
    return render(request, "admin/admin_report_customer.html",context)

@login_required(login_url='login/admin')
@user_passes_test(lambda u: u.is_superuser, login_url='login/admin')
def suspend_report_admin(request,id):
    print(id,"hhhhhhh")
    morvendor=Report_vendor.objects.get(id=id)
    morvendor.is_deleted=True
    morvendor.save()
    morvendorid=morvendor.vendor.id
    mor =Morwagon_users.objects.get(id=morvendorid)
    mor.is_suspended=True
    mor.save()
    morid=mor.id
    moremail=mor.email
    print(moremail,"ggggggggggggg")
    # Morwagon_users.objects.filter(id=morid).update(is_suspended=True)
    htmlgen = '<p>Your account has suspended </p>'
    send_mail('Account deletion',"kkkk",'reshma@beaconinfo.tech',[moremail], fail_silently=True, html_message=htmlgen)            
    print("liyaaaaaaaaaa")
    # return HttpResponse("lol")
    return redirect('/report_admin_customer')


@login_required(login_url='login/admin')
@user_passes_test(lambda u: u.is_superuser, login_url='login/admin')
def delete_report_admin(request,id):
    report_vendor=Report_vendor.objects.get(id=id)
    report_vendor.is_deleted=True
    report_vendor.save()
    return redirect('/report_admin_customer')