from itertools import product
import email
from celebrations .models import Tax_details,Payment,Adds,Category,Sub_category,Products,Enquiry,Review,Review_images,Morwagon_users,Album,Album_details,Videos,Packages,Forgetpassword
from celebrations .models import Units,placedOrders,pkgOrder,Tax_details,Payment,Adds,Category,Sub_category,Products,Enquiry,Review,Review_images,Morwagon_users,Album,Album_details,Videos,Packages,Vendor_package
# from django.utils import simplejson
from django.http import HttpResponse, request
from django.shortcuts import redirect
from django.db.models import Count
import base64
from django.db.models import Avg
from django.core.files.base import ContentFile
from celebrations.models import *
from django.views.decorators.csrf import csrf_exempt
import io
from django.core.files.images import ImageFile
from django.core import serializers
from django.db.models import Max
from django.contrib.auth import authenticate,login
from django.contrib import auth
from django.contrib import messages
from django.core.validators import validate_email
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required,user_passes_test
from django.db.models import Q
from django.template.loader import render_to_string
from weasyprint import HTML
from django.conf import settings
from django.contrib.auth.decorators import login_required, user_passes_test

# ////////////////////////////////////////////////////////////// liyaaa
from django.http.response import HttpResponse,JsonResponse
from django.shortcuts import render,redirect
from celebrations.forms import ProductsForm
from celebrations.models import Category,Sub_category,Products,Packages,Morwagon_users,Vendor_package,Tax_details,Payment,Verifyemail,pkgOrder,placedOrders
from django.http import HttpResponse
import json
from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.tokens import default_token_generator
from django.contrib import auth
from django.views.decorators.csrf import csrf_exempt
import razorpay
from django.core.mail import send_mail
import math, random
from django.core import serializers
from rest_framework_jwt.settings import api_settings
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.template.loader import render_to_string
from datetime import timedelta
import datetime
from django.utils import timezone
from django.utils.timezone import utc
from geopy.geocoders import Nominatim
from django.views.decorators.cache import cache_control
from send_notification import *
from decor import *





superusers = User.objects.filter(is_superuser=True)
JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER
# Create your views here.


def not_in_vendor(user):
    if user:
        return Morwagon_users.objects.get(user=user).role== 2
    return False



@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def ProductCreation(request):#product list view
          user_id=request.user.id
          morwagnUser=Morwagon_users.objects.get(user=user_id)
          pdt=Products.objects.filter(is_deleted=False,vendor=morwagnUser.id)
          context = {'ProductList': pdt,'morwagnUser':morwagnUser}
          return render(request, "vendor/productCreation.html",context)


@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def ProductDetails(request):#to save a product
    if request.method=='POST':
        r=request.POST
        data =  json.dumps(r)
        data = json.loads(data)
        service_name=request.POST.get('service_name')
        unit_lst=data['unit_lst']
        _location=data['_location']
        cke_description=data['cke_description']
        lat=data['lat']
        lng=data['lng']
        category1=data['category1']
        subcategory=data['subcategory']
        opening_time=data['opening_time']
        closing_time=data['closing_time']
        print(opening_time,".................................opening_time")
        print(closing_time,"...................................closing_time")
        phonenmbr=data['phonenmbr']
        address=data['address']
        textappointment=data['textappointment']
        textmenu=data['textmenu']
        img=data['img']
        editid= data['editid']
        vendor=request.user
        vid=Morwagon_users.objects.get(user=vendor)
        unitPriceJson=json.loads(unit_lst)
        priceunitobj=[]
        if data['editid']!='':
            editid=request.POST.get('editid')
            product_obj = Products.objects.get(pk=editid)
            priceunitobj=list(PriceUnit.objects.filter(product=product_obj.id).values_list('id',flat=True))
            for i in unitPriceJson:
                unit=i['unit']
                field1=i['field1']
                field2=i['field2']
                field3=i['field3']
                checkbox=i['checkbox']
                print(checkbox,"....................................checkbox")
                if(checkbox==1):
                    checkbox=0
                else:
                    checkbox=1
                if (i['unit']!='' and ( i['field1']!='' or( i['field2']!='' and i['field2']!='') )):
                    priceunitsaveobj=PriceUnit(unit_id=unit,product_id=product_obj.id,is_fixedprice=checkbox)
                    priceunitsaveobj.save()
                    updateId=priceunitsaveobj.id
                    obj=PriceUnit.objects.get(id=updateId)
                    if field1:
                        obj.price=field1
                    if field2:
                        obj.from_price=field2
                    if field3:
                        obj.to_price=field3
                    obj.save()
                    PriceUnit.objects.filter(id__in=priceunitobj).delete()
            if data['img']:
                image=data['img']
                format, imgstr = image.split(';base64,') 
                ext = format.split('/')[-1] 
                img = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
                
                product_obj.image = img
                product_obj.save()
            Products.objects.filter(id=editid).update(address=address,phone=phonenmbr,appointments=textappointment,menu=textmenu,location=_location,product_name=service_name,opening_Time=opening_time, closing_Time=closing_time ,category_id=category1 ,subcategory_id=subcategory, description=cke_description,vendor=vid,is_approved=False,is_updated=True,is_rejected=False,update_rejected=False,lat=lat,lng=lng)
            superusers_id = list( User.objects.filter(is_superuser=True).values_list('id',flat=True))
            mor_user = list(Morwagon_users.objects.filter(user__in=superusers_id).values_list('id',flat=True))
            actor = Morwagon_users.objects.get(user =request.user)
            p_url = "/service/requests"
            save_notification(sndr=actor,rcpt=mor_user,msg="Product Edited",title="Product edit approval",typ=p_url,get_id=0)
            return redirect('/vendor/create')
        format, imgstr = img.split(';base64,') 
        ext = format.split('/')[-1] 
        img = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
        save_pdts=Products(address=address,phone=phonenmbr,appointments=textappointment,menu=textmenu,
        location=_location,product_name=service_name,image=img, opening_Time=opening_time, closing_Time=closing_time ,category_id=category1 ,subcategory_id=subcategory, description=cke_description,vendor=vid,is_approved=False,lat=lat,lng=lng)
        save_pdts.save()
        save_id=save_pdts.id
        for i in unitPriceJson:
            unit=i['unit']
            field1=i['field1']
            field2=i['field2']
            field3=i['field3']
            checkbox=i['checkbox']
            if(checkbox==1):
                    checkbox=0
            else:
                checkbox=1
            priceunitsaveobj=PriceUnit(unit_id=unit,product_id=save_id,is_fixedprice=checkbox)
            priceunitsaveobj.save()
            updateId=priceunitsaveobj.id
            obj=PriceUnit.objects.get(id=updateId)
            if field1:
                obj.price=field1
            if field2:
                obj.from_price=field2
            if field3:
                obj.to_price=field3
            obj.save()
        superusers_id = list( User.objects.filter(is_superuser=True).values_list('id',flat=True))
        mor_user = list(Morwagon_users.objects.filter(user__in=superusers_id).values_list('id',flat=True))
        actor = Morwagon_users.objects.get(user =request.user)
        p_url = "/package/upgrade"
        save_notification(sndr=actor,rcpt=mor_user,msg="New product created",title="Product creation",typ=p_url,get_id=0)
    context = {'CategoryList': Category.objects.filter(is_deleted=False),'hide_id':0,'units':Units.objects.filter(is_deleted=False)}
    return render(request, "vendor/productDetails.html",context)


@login_required(login_url='/login')
@user_passes_test(not_in_vendor,login_url='/login')
def LoadSubCategoryDropdown(request):#to load subcategory dependent dropdown
          if request.GET.get('category'):
                    category_id = request.GET['category']
                    sub=Sub_category.objects.filter(is_deleted=False,category=category_id)
                    data = []
                    for i in sub:
                              data.append({'name': i.subcategory_name, 'id':i.id})
          return HttpResponse(json.dumps(data),content_type='application/json')


@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def EditProduct(request):#to edit product
        EditId=request.GET.get('EditId')
        edit_product=Products.objects.get(id=EditId)
        opening_Time=edit_product.opening_Time
        closing_Time=edit_product.closing_Time
        Otime=opening_Time
        Ctime=closing_Time
        # Otime=opening_Time.strftime("%H:%M:%S:%p")
        # Ctime=closing_Time.strftime("%H:%M:%S:%p")
        _category=edit_product.category
        context = {'Ctime':Ctime,'Otime':Otime,'EditId':EditId,'EditList':edit_product,'CategoryList': Category.objects.filter(is_deleted=False),'SubCategoryList':Sub_category.objects.filter(category=_category)}
        return render(request, "vendor/productDetails.html",context)

# @complete_profile_required   
# @cache_control(no_cache=True, must_revalidate=True, no_store=True)
# @login_required(login_url='/login')
# @user_passes_test(not_in_vendor, login_url='/login')
# def EditProduct(request):#to edit product
#           EditId=0
            # if request.is_ajax():
#           if request.method=='POST':
#                     pdtName=request.POST.get('pdtName')
#                     description=request.POST.get('content')
#                     lat=request.POST.get('lat')
#                     lng=request.POST.get('lng')
#                     category=request.POST.get('category')
#                     subcategory=request.POST.get('subcategory')
#                     price=request.POST.get('price')
#                     unit=request.POST.get('unit')
#                     vendor=request.user
#                     vid=Morwagon_users.objects.get(user=vendor)
#                     location=request.POST.get('location')
#                     if request.POST.get('hidden-imageedit'):
#                               image=request.POST.get('hidden-imageedit')
#                               format, imgstr = image.split(';base64,') 
#                               ext = format.split('/')[-1] 
#                               img = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
#                               product = Products.objects.get(pk=request.GET.get('EditId'))
#                               product.image = img
#                               product.save()
#                     Products.objects.filter(id=request.GET.get('EditId')).update(product_name=pdtName,price=price ,category_id=category,location=location ,subcategory_id=subcategory, description=description, unit=unit, vendor=vid,is_approved=False,lat=lat,lng=lng,is_updated=True,is_rejected=False,update_rejected=False)
#                     superusers_id = list( User.objects.filter(is_superuser=True).values_list('id',flat=True))
#                     mor_user = list(Morwagon_users.objects.filter(user__in=superusers_id).values_list('id',flat=True))
#                     actor = Morwagon_users.objects.get(user =request.user)
#                     p_url = "/service/requests"
#                     save_notification(sndr=actor,rcpt=mor_user,msg="Product Edited",title="Product edit approval",typ=p_url,get_id=0)
#                     return redirect('/vendor/create')
#           EditId=request.GET.get('EditId')
#           edit_product=Products.objects.get(id=EditId)
#           priceUnit=PriceUnit.objects.filter(product=EditId).values()
#         #   print(Products.objects.filter(id=EditId).values(),"...........................................edit_productedit_productedit_productedit_productedit_product")
#           _category=edit_product.category
#           context = {'priceUnit':priceUnit,'EditList':edit_product,'CategoryList': Category.objects.filter(is_deleted=False),'SubCategoryList':Sub_category.objects.filter(category=_category)}
#         #   print(Products.objects.filter(id=EditId).values(),"====================================context")
#           return render(request, "vendor/productDetails.html",context)

@csrf_exempt
@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def Flagchange(request):#To change review flag
    pdtId=request.POST.get('pdtId')
    Review.objects.filter(id=pdtId).update(flag=True)
    data={'data':'flaged'}
    return JsonResponse(data)


@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def ViewProduct(request):#To View product
          pid=request.GET.get('pid')
          qs=Review.objects.filter(product__id=pid)
          Avg_count=qs.aggregate(Avg('rating'))
          ADS=Adds.objects.filter(product__id=pid,is_deleted=False)
          price=PriceUnit.objects.filter(product=pid,is_deleted=False)
          context = {'CategoryList': Products.objects.get(id=pid),'R':qs,"avg":Avg_count,"ads":ADS,'priceunit':price}
          return render(request, "vendor/ViewproductDetails.html",context)


@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def DeleteProduct(request):# To delete a product
          id=request.GET.get('id')
          del_product=Products.objects.get(id=id)
          apprl_sts=del_product.is_approved
          if apprl_sts==False:
                    del_product.is_deleted=True
          del_product.is_approved=False
          del_product.save()
          return redirect('/vendor/create')



@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def DeleteImage(request):# To delete a image 
    if request.GET.get('ImgId'):
        Delete_img=request.GET.get('ImgId')
        del_product=Album_details.objects.get(id=Delete_img)
        del_product.is_deleted=True
        del_product.save()
        id =  del_product.album.id
        newid = Album.objects.get(id=id).service.id

    if request.GET.get('v_id'):
        Delete_Vid=request.GET.get('v_id')
        del_video=Videos.objects.get(id=Delete_Vid)
        del_video.is_deleted=True
        del_video.save()
        newid =  del_video.service.id
    
    if request.GET.get('AdId'):
        Delete_ad=request.GET.get('AdId')
        del_Ads=Adds.objects.get(id=Delete_ad)
        del_Ads.is_deleted=True
        del_Ads.save()
        newid =  del_Ads.product.id
    return redirect(f'/vendor/ViewProduct/?pid={newid}')


@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')        
def EnquiryView(request):
          rslt_final2={}
          lst=[]
          userID=request.user.id
          mu_id=Morwagon_users.objects.get(user=userID)
          pdts=list(Products.objects.filter(vendor__id=mu_id.id,is_deleted=False).values_list('id',flat=True))
          for i in pdts: 
                    qs1=Enquiry.objects.filter(product__vendor__id=mu_id.id,product__id=i)   
                    pdts1=Products.objects.get(id=i)
                    ENQ_call=qs1.filter(type=1).count()
                    ENQ_msg=qs1.filter(type=2).count()
                    ENQ_total=ENQ_call+ENQ_msg
                    name = {"service":pdts1.product_name, "call":  ENQ_call, "msg":ENQ_msg, "total":ENQ_total }
                    lst.append(name)
          rslt_final2['out']=lst
          qs2=Enquiry.objects.filter(product__vendor__id=mu_id.id,product__in=pdts)
          CCount=qs2.filter(type=1).count()
          Cmsg=qs2.filter(type=2).count()
          total_enq=CCount+Cmsg
          return render(request, "vendor/Enquiry.html",{"x":rslt_final2,"y":CCount,"z":Cmsg,"total":total_enq})

def ReviewFilter(request):
    get_to_date = request.POST.get('toDate')
    get_from_date = request.POST['fromDate']
    from_date = datetime.datetime.strptime(get_from_date, "%d/%m/%Y")
    to_date = datetime.datetime.strptime(get_to_date, "%d/%m/%Y")
    # from_date = datetime.datetime.strptime(get_from_date, "%d/%m/%Y").strftime('%Y-%m-%d %H:%M:%S.%f')
    # to_date = datetime.datetime.strptime(get_to_date, "%d/%m/%Y").strftime('%Y-%m-%d %H:%M:%S.%f').replace("00:00:00", "23:59:59")
    mu_id=Morwagon_users.objects.get(user=request.user.id)
    all_rvs = Review.objects.filter(product__vendor__id=mu_id.id,is_deleted=False, update_date__gte= from_date, update_date__lte=to_date,approvel_sts=True).values('id','product__product_name','title','rating','update_date','flag')
    x1=list(all_rvs)
    x={'id': x1}
    return JsonResponse(x)

@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def ApproveReview(request):                
          Viewid=request.GET.get('viewId')
          if Viewid:
                    dataDict={}
                    data=Review.objects.get(pk=Viewid)
                    data_img=list(Review_images.objects.filter(review=Viewid).values_list('images',flat=True))
                    dataDict['img']=data_img
                    dataDict['title']=data.title
                    dataDict['rating']=data.rating
                    dataDict['review']=data.review
                    return JsonResponse(dataDict)
          mu_id=Morwagon_users.objects.get(user=request.user.id)
          all_reviews=Review.objects.filter(product__vendor__id=mu_id.id,product__is_deleted=False,approvel_sts=True)
          context = {'ReviewList': all_reviews}
          return render(request, "vendor/ApproveReview.html",context)


@complete_profile_required
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def Albumsave(request,pid):
    if request.method=='POST':
        imageList=request.POST.getlist('imageArr[]')
        name=request.POST.getlist('name')
        if Album.objects.filter(service=pid).exists():
            album = Album.objects.get(service=pid).id
            for image in imageList:
                format, imgstr = image.split(';base64,') 
                ext = format.split('/')[-1] 
                img = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
                img_save=Album_details(album_id=album,image=img)
                img_save.save()
        else:
            albmSave=Album(album_name=name[0],service_id=pid)
            albmSave.save()
            albmSaveId=albmSave.id
            for image in imageList:
                format, imgstr = image.split(';base64,') 
                ext = format.split('/')[-1] 
                img = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
                img_save=Album_details(album_id=albmSaveId,image=img)
                img_save.save()
        
    k=get_package_id(request)
    pkglmt=Packages.objects.get(id=k[0]).dispaly_image_limit
    count_img=Album_details.objects.filter(album__service__id=pid,is_deleted=False).count()
    # deleted_images = Album_details.objects.filter(album__service__id=pid,is_deleted=True,is_approved=True,
    #                                     updated_on__gt=timezone.now().date() - datetime.timedelta(
    #                                         days=30))
    # length=len(deleted_images)
    # if length:
    #     lx=1
    # else:
    #     lx=0
    diff=pkglmt-count_img
    context = {'pid':pid,'diff':diff}
    return render(request, "vendor/albumdetails.html",context)


@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def advertisement_upload(request,pid):
          if request.method=='POST':
            imageList=request.POST.getlist('imageArr[]')
            for image in imageList:
                format, imgstr = image.split(';base64,') 
                ext = format.split('/')[-1] 
                img = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
                img_save=Adds(product_id=pid,image=img)
                img_save.save()
          k=get_package_id(request)
          Adlmt=Packages.objects.get(id=k[0]).add_limit
          count_ad=Adds.objects.filter(product__id=pid,is_deleted=False).count()
          diff=Adlmt-count_ad
          context = {'pid':pid,'diff':diff}
          return render(request, "vendor/advertisement_create.html",context)


@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def PkgConform(request):
    if request.POST:
        pkgid=request.POST.get('package')
        k1=get_package_id(request)
        _pkg=Packages.objects.filter(is_deleted=False)
        prepkgprice=_pkg.get(id=k1[0]).price
        currentpkgprice=_pkg.get(id=pkgid).price
        if prepkgprice>currentpkgprice:#package downgrade/////////////////////////
            morwagnUser=Morwagon_users.objects.get(user=request.user.id).id
            services=Products.objects.filter(is_deleted=False,vendor=morwagnUser)
            services.update(temp_sts=False)
            downgraded_package_lmt=_pkg.get(id=pkgid).product_listign_limit
            
            return render(request, "vendor/downgrade.html",{"services":services,"d_pkg":pkgid,"d_service_lmt":downgraded_package_lmt})
        else:
            k=CompletePayment(request,pkgid)
            return render(request, "vendor/pkg_updation.html",k)
    return render(request, "vendor/payment.html")



@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def DeselectService(request):    
    if request.POST:
        # if 'refresh' not in request.POST:
        if 'refresh' not in request.POST:
            ser_ob = request.POST.getlist('chk')
            mid=Morwagon_users.objects.get(user=request.user.id).id
            Album_details.objects.filter(album__service__vendor__user=mid,is_deleted=False).update(temp_sts=False)
            Adds.objects.filter(product__vendor__user=mid,is_deleted=False).update(temp_sts=False)
            Videos.objects.filter(service__vendor__user=mid,is_deleted=False).update(temp_sts=False)
        else:
            ser_ob = request.POST['chk']
            ser_ob = eval(ser_ob)
        Products.objects.filter(id__in=ser_ob).update(temp_sts=True)
        dpck=request.POST.get('dpck')
        _pkg=Packages.objects.filter(is_deleted=False).get(id=dpck)
        product = Products.objects.filter(id__in=ser_ob).values()
        for data in list(product): 
            albums = Album.objects.filter(service_id=data['id'],is_deleted=False).values()
            for album in list(albums): 
                details=Album_details.objects.filter(album_id=album['id'],is_deleted=False,temp_sts=False).values()
                data['details']=list(details)
            ads = Adds.objects.filter(product_id=data['id'],is_deleted=False,temp_sts=False).values()
            videos = Videos.objects.filter(service_id=data['id'],is_deleted=False,temp_sts=False).values()
            data['albums']=list(albums)
            data['ads']=list(ads)
            data['videos']=list(videos)
        context={
            "servicelst":product,
            "dpck":_pkg,
            "ser_ob":ser_ob,
        }
        return render (request, "vendor/select_images.html",context)
    else:
        return render(request, "vendor/payment.html")



@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def RemoveMedia(request):
    if request.method=='POST':
        img_list=request.POST.getlist('Img_list[]')
        vid_list=request.POST.getlist('Video_list[]')
        ad_list=request.POST.getlist('Ad_list[]')
        Album_details.objects.filter(id__in=img_list).update(temp_sts=True)
        Adds.objects.filter(id__in=ad_list).update(temp_sts=True)
        Videos.objects.filter(id__in=vid_list).update(temp_sts=True)
        dict1 = {"status":"success"}
        return JsonResponse(dict1)
    else:
        pkg=request.GET.get('p')
        k=CompletePayment(request,pkg)
        return render(request, "vendor/pkg_updation.html",k)


@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def DisableImg(request):
    if request.GET.get('ImgId'):
        Delete_img=request.GET.get('ImgId')
        del_product=Album_details.objects.get(id=Delete_img)
        del_product.temp_sts=True
        del_product.save()
    if request.GET.get('v_id'):
        Delete_Vid=request.GET.get('v_id')
        del_video=Videos.objects.get(id=Delete_Vid)
        del_video.temp_sts=True
        del_video.save()
    if request.GET.get('AdId'):
        Delete_ad=request.GET.get('AdId')
        del_Ads=Adds.objects.get(id=Delete_ad)
        del_Ads.temp_sts=True
        del_Ads.save()
    if request.GET.get(''):
        dict1 = {"status":"success"}
        return JsonResponse(dict1)



    




def CompletePayment(request,pkgid):
        uid=request.user.id
        k=payment_calc(pkgid,uid)
        amt=float(k['amt'])*100
        client = razorpay.Client(
        auth=("rzp_test_WeK5FaW95tFJCc", "BmoSUGl8raqBkuBLPLxGBMNE"))
        data = { "amount": amt, "currency": "INR", "receipt": "order_rcptid_11" }
        payment = client.order.create(data=data)
        Order_save=pkgOrder(user_id=uid,amount=amt,Package_id=pkgid,Razor_order_id=payment['id'])
        Order_save.save()
        payment_id=Order_save.id
        k['payment']=payment
        k['payment_table_id']=payment_id
        k['pkgid']=pkgid
        Muser=Morwagon_users.objects.get(user=uid)
        k['email']=Muser.email
        k['phnumber']=Muser.mobile_number
        return k

@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def PkgUpdation(request):
    if request.POST:
        pkgID=get_package_id(request)
        exp_date=pkgID[1]
        currentPkg= Packages.objects.get(is_deleted=False,id=pkgID[0])
        pkg_nme=currentPkg.package_name
        pkg_price=currentPkg.price
        dr = dict()
        dr["name"] = pkg_nme
        products= Packages.objects.filter(is_deleted=False).filter(~Q(price=0.00))
        if exp_date.date()>timezone.now().date():#package not expired ... ......... .....
            products= products.filter(price__gt=pkg_price)
        qs_json=serializers.serialize("json",products )
        dr["data"] = qs_json
        return JsonResponse(dr, content_type='application/json')
    return render(request, "vendor/payment.html")


# --------------------------------------------------------------------------------------------------------------------------------------

@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def PaymentFinal(request):
    paymt_id=request.POST.get('payment_id')#to update table
    payment_Table_Id=request.POST.get('paymentTableId')
    ordr_table=pkgOrder.objects.get(id=payment_Table_Id)
    client = razorpay.Client(auth=("rzp_test_WeK5FaW95tFJCc", "BmoSUGl8raqBkuBLPLxGBMNE"))
    resp = client.payment.fetch(paymt_id)
    order_id_response=request.POST.get('order_id')
    if (resp['id']==paymt_id) and (resp['amount']==ordr_table.amount) and (resp['order_id']==ordr_table.Razor_order_id==order_id_response):
        ordr_table.payment_id=paymt_id
        ordr_table.amount=resp['amount']/100
        ordr_table.status=resp['status']
        ordr_table.save()
        try:
            if ordr_table.status=='captured':
                ID=ordr_table #To create success table
                placedOrders_save=placedOrders(main_id=ID)
                placedOrders_save.save()
                return JsonResponse({"sts":"success","payment_id":paymt_id})
        except:
            ordr_table.status="failed"
            ordr_table.save()
            return JsonResponse({"sts":"failed"})


@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def FaildHtml(request):
    return render(request, 'vendor/payment_failed.html')

@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor,login_url='/login')
def VideoUpload(request,pid):
          if request.FILES and request.POST:
                    Products.objects.filter(id=pid).update(is_updated=True)
                    image=request.POST.get('hidden-imageedit')
                    format, imgstr = image.split(';base64,') 
                    ext = format.split('/')[-1] 
                    img = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
                    video = request.FILES['v']
                    save_video=Videos( name ="jj",service_id=pid,thumbnail=img,Videos=video)
                    save_video.save()
                    return redirect(f"/vendor/ViewProduct/?pid={pid}")
          k=get_package_id(request)
          Adlmt=Packages.objects.get(id=k[0]).video_limit
          count_ad=Videos.objects.filter(service__id=pid,is_deleted=False).count()
          diff=Adlmt-count_ad
          context = {'diff':diff}
          return render(request, "vendor/video_upload.html",context)

@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@user_passes_test(not_in_vendor, login_url='/login')


def VendorDashboard(request):
    uid=request.user
    userQuery=Morwagon_users.objects.filter(user=uid)
    context = {'D': userQuery,'SELECTTYPE':SELECTTYPE}
    return render(request, "vendor/vendor_dashboard.html",context)



@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def LimitProduct(request):
          k=get_package_id(request)
          pkgdata=Packages.objects.get(id=k[0])
          imglmt=pkgdata.dispaly_image_limit#remove if not used...............
          adlmt=pkgdata.add_limit
          vlmt=pkgdata.video_limit
          userid=request.user
          lmt=list(Packages.objects.filter(id=k[0]).values_list('product_listign_limit',flat=True) )
          Pdts=Products.objects.filter(vendor__user=userid.id,is_deleted=False).count()
          exp_date=Morwagon_users.objects.get(user=request.user.id).expirydate
          date_today=timezone.now().replace(tzinfo=utc)
          if date_today>exp_date:
              return JsonResponse({"message":"Package expired","sts":1,'imglmt':imglmt,'adlmt':adlmt,'vlmt':vlmt})
          if Pdts and (lmt[0]<=Pdts):
              return JsonResponse({"message":"Service adding limit reached . please upgrade your package ","sts":2,'imglmt':imglmt,'adlmt':adlmt,'vlmt':vlmt})
          return JsonResponse({"message":"none",'imglmt':imglmt,'adlmt':adlmt,'vlmt':vlmt})


@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')          
def NameCheck(request):
          name_chk=request.GET.get('nme')
          pdt_chk=Products.objects.filter(product_name__iexact=name_chk,is_deleted=False)
          if pdt_chk:
                    return JsonResponse({"message":"Name already exist",'status':200})
          return JsonResponse({"message":"none",'status':404})
          
          
# ----------------------------view page functions-----------------------------------

@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')   
def AD_view(request):
          pid=request.POST['varid']
          AD=Adds.objects.filter(product__id=pid,is_deleted=False)
          if len(AD)<=0:
                    context = {'AD': "none"}
                    return JsonResponse(context)
          else:
                    qs_json=serializers.serialize("json",AD )
                    return HttpResponse(qs_json, content_type='application/json')
          

@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')   
def IdpickAlbum(request):
          print("lll======================llll0000000000")
          pid=request.POST['varid']
          Albm=Album_details.objects.filter(album__service=pid,is_deleted=False)
          print(Albm,"....................................AlbmAlbmAlbmAlbm")
          if len(Albm)<=0:
                    context = {'Albm': "none"}
                    return JsonResponse(context)
          else:
                    print("else..................................eeelllssseee")
                    qs_json=serializers.serialize("json",Albm )
                    return HttpResponse(qs_json, content_type='application/json')


@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')   
def IdpickVideo(request):
          pid=request.POST['varid']
          vds=Videos.objects.filter(service=pid,is_deleted=False)
          if len(vds)<=0:
                    context = {'vds': "none"}
                    return JsonResponse(context)
          else:
                    qs_json=serializers.serialize("json",vds)
                    return HttpResponse(qs_json, content_type='application/json')

 
def payment_calc(pkgid,uid):
          pkgid1=Packages.objects.get(id=pkgid)
          p=pkgid1.price
          sta=Morwagon_users.objects.get(user=uid)
          geolocator = Nominatim(user_agent="geoapiExercises")
          lt=str(sta.latitude)
          lg=str(sta.logitude)
          location = geolocator.reverse(lt+","+lg)
          address = location.raw['address']
          state = address.get('state', '')

          pkg=Packages.objects.get(id=pkgid)
          if state==("Kerala" or "kerala"):
              f=False
              rslt=bill_calc(f,p,pkg)
          else:
              f=True
              rslt=bill_calc(f,p,pkg)
          return rslt
 
def bill_calc(f,p,pkg):
    rslt_final2={}
    lst=[]
    tax_amt_lst=[]
    items=Tax_details.objects.filter(flag=f,is_deleted=False)
    for i in items:
        tax_name=i.tax_name
        percentage=i.percentage
        tax_amt=(p*percentage)/100
        tax_amt_lst.append(tax_amt)
        dict_tax1={'tax_name':tax_name,'percentage':percentage,'tax_amt':tax_amt}
        lst.append(dict_tax1)
    rslt_final2['out']=lst
    tax = sum(tax_amt_lst)
    amt=tax+p
    context = {'pkg': pkg,'taxes':rslt_final2,'tax':tax,'amt':amt }
    return context



def get_package_id(request):
    user_id=request.user.id
    MU=Morwagon_users.objects.get(user=user_id)
    packageID=MU.packages.id
    _exp=MU.expirydate
    return packageID,_exp

@csrf_exempt
@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def success(request,id):
    uid=request.user.id
    vendor_details=Morwagon_users.objects.get(user=uid)
    k=payment_calc(id,uid)
    k['vndr_details']=vendor_details
    k['payment_id']=request.GET.get('p')
    k['packageID']=id
    pkgUpdate=Packages.objects.get(id=id)
    months=pkgUpdate.months
    d1 = datetime.date.today()
    d2 = d1 + relativedelta(months=months)
    vendor_details.expirydate=d2
    vendor_details.packages=pkgUpdate
    vendor_details.save()
    Vendor_package.objects.filter(vendor=vendor_details.id).update(package=pkgUpdate,recently_created=True)
    Album_details.objects.filter(album__service__vendor__user=vendor_details.id,temp_sts=True).update(is_deleted=True)
    Adds.objects.filter(product__vendor__user=vendor_details.id,temp_sts=True).update(is_deleted=True)
    Videos.objects.filter(service__vendor__user=vendor_details.id,temp_sts=True).update(is_deleted=True)
    Products.objects.filter(vendor__user=vendor_details.id,temp_sts=False).update(is_deleted=True)
    return render(request, "vendor/success.html",k)


@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def html_to_pdf_view(request):
    if request.method=='POST':
        pkg_id=request.POST.get('packageID')
        payment_id=request.POST.get('payment_id')
        uid=request.user.id
        vendor_details=Morwagon_users.objects.get(user=uid)
        k=payment_calc(pkg_id,uid)
        k['vndr_details']=vendor_details
        k['payment_id']=payment_id
        html_string = render_to_string(str(settings.BASE_DIR)+"/celebrations/templates/vendor/Billpdf.html", {'bill': k})
        html = HTML(string=html_string, base_url=request.build_absolute_uri())
        html.write_pdf(target='/tmp/Package_updation_receipt.pdf');
    fs = FileSystemStorage('/tmp')
    with fs.open('Package_updation_receipt.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="Package_updation_receipt.pdf"'
        return response



@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def forgotpassword(request):
    user = request.user
    id = user.id
    userid = Morwagon_users.objects.get(user =id)
    context ={
        "email" : userid.mobile_number,
        "phone" :userid.mobile_number,
    } 
    return render(request,'vendor/forgotpassword.html',context)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def forgotemail(request):
    
   
    if request.method == 'POST':
       
        email = request.POST.get('email')
        print('email')
                 
        if   not Morwagon_users.objects.filter(email = email).exists():
            error = "this email does not exists"  
            dict1 ={'status': 'faild','message' :error}
            return JsonResponse(dict1)
       
       
        if Morwagon_users.objects.filter(email = email).exists():
            def generateOTP() :
                digits = "0123456789"
                OTP = ""
                for i in range(4) :
                    OTP += digits[math.floor(random.random() * 10)]
                return OTP

            o=generateOTP()
            htmlgen = '<p>Your OTP is <strong>'+o+'</strong></p>'
            send_mail('OTP request',o,'reshma@beaconinfo.tech',[email], fail_silently=True, html_message=htmlgen)
            
            if Forgetpassword.objects.filter(email = email).exists():
                Forgetpassword.objects.get(email =email).delete()
                forgetpassword =Forgetpassword(email=email,otp =o)
                forgetpassword.save()
            else:
                forgetpassword =Forgetpassword(email=email,otp =o)
                forgetpassword.save()
            msg = "otp sent succussfully "    
            dict1 ={'status': 'succuss','message' :msg, 'email': email}        
            return JsonResponse(dict1)
    return render (request,'vendor/verification_email.html',)

         




def checkotp(request):
    if request.method == 'POST':
        get_otp = int(request.POST.get('otp'))
        get_email = request.POST.get('email')
        if Forgetpassword.objects.filter(email= get_email).exists():
            obj = Forgetpassword.objects.get(email= get_email)
          
            if int(obj.otp) == get_otp :
             

                y = obj.created_on
                now = datetime.datetime.utcnow().replace(tzinfo=utc)
                x =now-y
           
                total_seconds = x.total_seconds()
                if total_seconds > 300:
                    error = 'OTP expired'
                    dict1 ={'status': 'faild','message' :error}
                    return JsonResponse(dict1)
                
                else:
                    succuss = 'OTP verified'
                    dict1 ={'status': 'succuss','message' :succuss, 'email':get_email}
                    return JsonResponse(dict1)
            else:
                error = 'Invalid OTP'
                dict1 ={'status': 'faild','message' :error}
                return JsonResponse(dict1)

          
        
            
        else:
            error = 'Enter your email and proceed'
            dict1 ={'status': 'faild','message' :error}
            return JsonResponse(dict1)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def resetpassword(request):
    email = request.GET.get('email', None)

    if request.method == 'POST':
        new_email = request.POST.get('email')
        print(new_email)
        get_password = request.POST.get('password')
        mail = new_email.replace(" ", "")
        mor_user = Morwagon_users.objects.get(email=mail).user.id
        user =User.objects.get(id=mor_user)
        user.set_password(get_password)
        user.save()
        
        messages.success(request, 'Your password has been changed.')
        return  redirect('admin-login')
       
    return render (request,'vendor/reset_password.html')





           
          
            
        
# ///////////////////////////////////////////////////////////////////////////// liyaaaaa

# # Create your views here.




def Register_validation(request):   
    if request.GET.get('mobile'):
        mobile=request.GET["mobile"]
        dict1 = {}
        if Morwagon_users.objects.filter(mobile_number=mobile).exists():
            dict1['mob'] = "mobile_found"
            return JsonResponse(dict1)
        else:
            dict1['mob'] = "mobile_not_found"
            return JsonResponse(dict1)
    if request.GET.get('whatsapp'):
        whatsapp=request.GET["whatsapp"]
        dict1 = {}
        if Morwagon_users.objects.filter(whatsapp_number=whatsapp).exists():
            dict1['whatsapp'] = "whatsapp_found"
            return JsonResponse(dict1) 
        else:
            dict1['whatsapp'] = "whatsapp_not_found"
            return JsonResponse(dict1) 

    if request.GET.get('email'):
        email=request.GET["email"]
        dict1 = {}
        if Morwagon_users.objects.filter(email=email).exists():
            dict1['email'] = "email_found"
            return JsonResponse(dict1)
        else:
            dict1['email'] = "email_not_found"
            return JsonResponse(dict1)

    if request.GET.get('package'):
        print("jlooooooooo")
        package=request.GET["package"]
        print(package,"hhhhhhhhhhh")
        pkd = Packages.objects.get(id=package)
        package_id=pkd.id
        package_name=pkd.package_name
        price=pkd.price
        product_listign_limit=pkd.product_listign_limit
        dispaly_image_limit=pkd.dispaly_image_limit
        benifits=pkd.benifits
        months=pkd.months
        print(months,"liya")

        userId=request.user.id
        # pkgid = request.GET.get('package')
        print(package_id,"package idddddddddddd")
        # pkg=Packages.objects.get(id=package_id)
        # pkgprice=pkd.price
        price=float(price)*100
        if price!=0.00:
            k=payment_calcnew(package_id,userId)
            client = razorpay.Client(
            auth=("rzp_test_WeK5FaW95tFJCc", "BmoSUGl8raqBkuBLPLxGBMNE"))
            data = { "amount": price, "currency": "INR", "receipt": "order_rcptid_11"}
            payment = client.order.create(data=data)
            print(payment)

            dict1 = {}
            dict1['package_id']=package_id
            dict1['package_name']=package_name
            dict1['price']=price
            dict1['product_listign_limit']=product_listign_limit
            dict1['dispaly_image_limit']=dispaly_image_limit
            dict1['benifits']=benifits
            dict1['months']=months
            dict1['amountraz'] = payment["amount"]
            dict1['orderraz'] = payment["id"]
            print(dict1,"dict1111111111111111111")       
            return JsonResponse(dict1)
        else:
            dict1={}
            dict1['price'] = 0.00
            print(dict1,"dict1111111111111111111")       
            return JsonResponse(dict1)



# def vendor_register(request): 
#     # request.session['pkgid'] = 1
#     # print(request.session['pkgid'],"anasssssssssssss")
#     if request.method=='POST':
#             first_name=request.POST.get('first_name')
#             last_name=request.POST.get('last_name')
#             email=request.POST.get('email')
#             mobile=request.POST.get('mobile')
#             whatsapp_number=request.POST.get('whatsapp')
#             password=request.POST.get('password')
#             address1=request.POST.get('address1')
#             address2=request.POST.get('address2')
#             town=request.POST.get('town')
#             district=request.POST.get('district')
#             state=request.POST.get('state')
#             pincode=request.POST.get('pincode')
#             gst_number=request.POST.get('gst_number')

#             latitude=request.POST.get('latitude')
#             logitude=request.POST.get('longitude')

#             location=request.POST.get('location')
#             store_name=request.POST.get('store_name')
#             logo_image=request.FILES.get('logo_image')
#             thumbnail=request.FILES.get('thumbnail')
#             caption=request.POST.get('caption')
#             Description=request.POST.get('Description')

#             package=request.POST.get('package')
#             pack=Packages.objects.get(id=package)
#             incorp_cirtificate=request.FILES.get('incorp_cirtificate')
#             gst=request.FILES.get('gst')
#             pancard=request.FILES.get('pancard')
#             address_proof=request.FILES.get('address_proof')
#             mob_email = Morwagon_users.objects.filter(Q(email=email) | Q(mobile_number=mobile) | Q(whatsapp_number=whatsapp_number))
#             print(mob_email,"exxxxxxxxxxxx")
#             if len(mob_email)==0:
#                 userrecord=User.objects.create_user(first_name=first_name,last_name=last_name,email=email,username=email,password=password,is_staff=1,user__is_active=False)
#                 uid=userrecord.id
#                 print(uid,"iddddddddddddddd")
#                 pack=Packages.objects.get(id=package)
#                 months=pack.months
#                 d1 = datetime.date.today()
#                 d2 = d1 + relativedelta(months=months)
#                 print(d2,"hhhhh")
#                 # 'user': id, 'first_name':first_name,'last_name':last_name,'email':email,'mobile_number': mobile_number,'role': 3, }
#                 save_vendor=Morwagon_users.objects.create(user=userrecord,first_name=first_name,
#                 last_name=last_name,email=email,mobile_number= mobile,role= 2,town=town,
#                 whatsapp_number=whatsapp_number,address1=address1,address2=address2,
#                 district=district,state=state,pincode=pincode,incorporation_certificate=incorp_cirtificate,GST=gst,
#                 pancard=pancard,address_proof=address_proof,location=location,
#                 store_name=store_name,logo_image=logo_image,thumbnail=thumbnail,caption=caption,Description=Description,packages=pack)
#                 save_Vendor_package=Vendor_package.objects.create(vendor=userrecord,package=pack,gst_number=gst_number,latitude=latitude,logitude=logitude) 
#                 user = authenticate(username=email, password=password,user__is_active=True)
#                 if user is not None:
#                     auth.login(request,user)
#                     return redirect("Login_vendor")
#             else:
#                 print("email or mobile or whatsapp number is exist") 
#                 messages.error(request, 'email or mobile or whatsapp number is already exist..!') 



@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def profile_vendor(request):
    vendor_id=request.user.id
    print(vendor_id,"kkkkkkkkkkkk")
    context = {'vendor': Morwagon_users.objects.get(user=vendor_id)}
    print(context,"jjjjjjjjjjjjj")
    return render(request, "vendor/vendor_profile.html",context)

@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def edit_vendor(request,vendor_id):
    if request.method=='POST':
        
        first_name=request.POST.get('first_name')
        print(first_name,"nameeeeeeeeeeeeeeeee")
        last_name=request.POST.get('last_name')
        email=request.POST.get('email')
        mobile=request.POST.get('mobile')
        whatsapp_number=request.POST.get('whatsapp')
        address1=request.POST.get('address1')
        address2=request.POST.get('address2')
        # town=request.POST.get('town')
        # district=request.POST.get('district')
        state=request.POST.get('state')
        pincode=request.POST.get('pincode')
        location=request.POST.get('location')
        store_name=request.POST.get('store_name')

        latitude=request.POST.get('latitude')
        logitude=request.POST.get('longitude')

        logo_image=request.FILES.get('logo_image')
        print(logo_image, "aaaaaaaaaaaaa")
        thumbnail=request.FILES.get('thumbnail')
        print(thumbnail, "tttttt")
        caption=request.POST.get('caption')
        Description=request.POST.get('Description')
        incorp_cirtificate=request.FILES.get('incorp_cirtificate')
        gst=request.FILES.get('gst')
        pancard=request.FILES.get('pancard')
        address_proof=request.FILES.get('address_proof')
        gst_number=request.POST.get('gst_number')
        userrecord=User.objects.get(id=vendor_id)
        userrecord.first_name=first_name
        userrecord.last_name=last_name
        userrecord.email=email
        userrecord.username=email
        userrecord.is_active=False
        # userrecord=User.objects.create_user(first_name=first_name,last_name=last_name,email=email,username=email,password=password,is_staff=1,user__is_active=False)
        Morwagon_users.objects.filter(user=vendor_id).update(first_name=first_name,
            last_name=last_name,email=email,mobile_number= mobile,role= 2,
            whatsapp_number=whatsapp_number,address1=address1,address2=address2,
            state=state,pincode=pincode,location=location,
            store_name=store_name,caption=caption,Description=Description,gst_number=gst_number,latitude=latitude,logitude=logitude,is_updated=True)
        
        mor_user = Morwagon_users.objects.get(user=vendor_id)
        if logo_image is not None:
            if mor_user.logo_image.name is not '':
                mor_user.logo_image.storage.delete(mor_user.logo_image.name)
            mor_user.logo_image = logo_image
        if thumbnail is not None:
            if mor_user.thumbnail.name is not '':
                mor_user.thumbnail.storage.delete(mor_user.thumbnail.name)
            mor_user.thumbnail = thumbnail
        if incorp_cirtificate is not None:
            if mor_user.incorporation_certificate.name is not '':
                mor_user.incorporation_certificate.storage.delete(mor_user.incorporation_certificate.name)
            mor_user.incorporation_certificate = incorp_cirtificate
        if gst is not None:
            mor_user.GST.storage.delete(mor_user.GST.name)
            mor_user.GST = gst
        if pancard is not None:
            mor_user.pancard.storage.delete(mor_user.pancard.name)
            mor_user.pancard = pancard
        if address_proof is not None:
            mor_user.address_proof.storage.delete(mor_user.address_proof.name)
            mor_user.address_proof = address_proof
        mor_user.save()
        return redirect("/vendor/profile_vendor")
    context = {'vendor': Morwagon_users.objects.get(user=vendor_id)}
    print("context","connnnnnnnnnnnnn")
    return render(request, "vendor/edit_vendor.html",context)
    # redirect("/vendor/")



def terms_conditions_vendor(request):
    return render(request, "vendor/termsandconditions.html")



def payment_calcnew(pkgid,uid):
        print(pkgid,"........................................pkgidpkgidpkgid")
        pkgid1=Packages.objects.get(id=pkgid)
        p=pkgid1.price
        sta=Morwagon_users.objects.get(user=uid)
        geolocator = Nominatim(user_agent="geoapiExercises")
        lt=str(sta.latitude)
        lg=str(sta.logitude)
        location = geolocator.reverse(lt+","+lg)
        print(location,"location")
        address = location.raw['address']
        print(address,"address")
        state = address.get('state', '')
        print(state,"mmmmmmmmmmmmmmmmmm")
        #   x = sta.split(",")
        #   state=x[0]
        #   print(state,"mmmmmmmmmmmmmmmmmm")

        pkg=Packages.objects.get(id=pkgid)
        if state=="Kerala":
            print(".....................................................kerala")
            f=False
            rslt=bill_calcnew(f,p,pkg)
              
        else:
            print("...................................................other")
            f=True
            rslt=bill_calcnew(f,p,pkg)
        return rslt
def bill_calcnew(f,p,pkg):
    rslt_final2={}
    lst=[]
    tax_amt_lst=[]
    items=Tax_details.objects.filter(flag=f,is_deleted=False)
    print(items,"...............................................33333333333333333333333333333333333333....items")
    for i in items:
        tax_name=i.tax_name
        percentage=i.percentage
        tax_amt=(p*percentage)/100
        tax_amt_lst.append(tax_amt)
        dict_tax1={'tax_name':tax_name,'percentage':percentage,'tax_amt':tax_amt}
        lst.append(dict_tax1)
    rslt_final2['out']=lst
    tax = sum(tax_amt_lst)
    amt=tax+p
    context = {'pkg': pkg,'taxes':rslt_final2,'tax':tax,'amt':amt }
    print(context,"......................................context")
    return context


def PkgUpdationnew(request,id):
        pkgid = id
        # uid=request.user.id
        uid=request.user.id
        print(uid,'rit........................')
        if uid==None:
            uid=request.session['vendor_sess_id']
            print(uid,'liyajan13')

        k=payment_calcnew(pkgid,uid)

        # pkgdetails=Packages.objects.get(id=id)
        pkgprice=k['amt']
        amt=float(pkgprice)*100
 
        tax=123
        # userId=request.user.id
        client = razorpay.Client(
        auth=("rzp_test_WeK5FaW95tFJCc", "BmoSUGl8raqBkuBLPLxGBMNE"))
        data = { "amount": amt, "currency": "INR", "receipt": "order_rcptid_11" }
        payment = client.order.create(data=data)
        # Payment_save=Payment(payment_id=payment['id'],total_amount=amt,amount=pkgprice,tax_amount=tax,user_id=userId)
        Order_save=pkgOrder(user_id=uid,amount=amt,Package_id=pkgid,Razor_order_id=payment['id'])
        Order_save.save()
        payment_id=Order_save.id
        k['payment']=payment
        k['payment_table_id']=payment_id
        k['pkgid']=pkgid
        print(uid,'ja13liyalllllllllllllllllllllllllllllllllllllllllllllll')
        Muser=Morwagon_users.objects.get(user=uid)
        k['email']=Muser.email
        k['phnumber']=Muser.mobile_number
        print(k,"........................................................sandraaaaaaaaaaaa")
        return render(request, "vendor/pkg_updationnew.html",k)

def PaymentFinalnew(request):
    paymt_id=request.POST.get('payment_id')#to update table
    payment_Table_Id=request.POST.get('paymentTableId')
    ordr_table=pkgOrder.objects.get(id=payment_Table_Id)
    client = razorpay.Client(auth=("rzp_test_WeK5FaW95tFJCc", "BmoSUGl8raqBkuBLPLxGBMNE"))
    resp = client.payment.fetch(paymt_id)
    order_id_response=request.POST.get('order_id')

    print('halo resp',resp)
    # print(resp,"......................................resprespresprespresprespresprespresprespresp")
    if (resp['id']==paymt_id) and (resp['amount']==ordr_table.amount) and (resp['order_id']==ordr_table.Razor_order_id==order_id_response):
        ordr_table.payment_id=paymt_id
        ordr_table.amount=resp['amount']/100
        ordr_table.status=resp['status']
        ordr_table.save()
        try:
            if ordr_table.status=='captured':
                print("captured...................................##")
                # return HttpResponse("ll")
                ID=ordr_table #To create success table
                print(ID,"...........................iiiddd")
                placedOrders_save=placedOrders(main_id=ID)
                placedOrders_save.save()
                print("successsssssssssss")
                return JsonResponse({"sts":"success","payment_id":paymt_id})
        except:
            ordr_table.status="failed"
            ordr_table.save()
            return JsonResponse({"sts":"failed"})

def successnew(request,id):
    # uid=request.user.id
    uid=request.user.id
    if uid==None:
        uid=request.session['vendor_sess_id']
        user=User.objects.get(id=uid)
        auth.login(request,user)
    
    vendor_details=Morwagon_users.objects.get(user=uid)


    # reg_complete=vendor_details.is_done
    # print(reg_complete,"gggggggggggggggggggggggggggg")
    # if reg_complete==1:
    #     k['completed']="complete"
    #     return render(request, "vendor/successnew.html",k)

    pkg=vendor_details.packages.id
    pack=Packages.objects.get(id=pkg)
    months=pack.months
    d1 = datetime.date.today()
    d2 = d1 + relativedelta(months=months)
    print(d2,"hhhhh")

    # datelast=d2.strftime("%Y-%m-%d %H:%M:%S")
    vendor_details.expirydate=d2
    vendor_details.is_done=True
    vendor_details.save()
    k=payment_calcnew(id,uid)
    k['vndr_details']=vendor_details
    k['payment_id']=request.GET.get('p')
    k['packageID']=id
    superusers_id = list( User.objects.filter(is_superuser=True).values_list('id',flat=True))
    mor_user = list(Morwagon_users.objects.filter(user__in=superusers_id).values_list('id',flat=True))
    actor = Morwagon_users.objects.get(user =request.user)
    p_url = "/vendors/request"
    save_notification(sndr=actor,rcpt=mor_user,msg="New vendor registered",title="Vendor registration",typ=p_url,get_id=0)
    return render(request, "vendor/successnew.html",k)

def FaildHtml(request):
    return render(request, 'vendor/payment_failed.html')
# ////////////////////////////////////////////////////////////////// newwwwwwwwww


def vendor_register(request): 
    print(request.user,"koooooooooooooo")
    user = request.user
    id=user.id
    print(id,"koooooooooooooo") 
    page_no=0
    context = {'packagelist': Packages.objects.filter(is_deleted=False).order_by('price')}    
    if User.objects.filter(id=id).exists():
        packs=Morwagon_users.objects.get(user=id)
        if packs.packages is not None:
            isdone=Morwagon_users.objects.filter(user=id,is_done=True).exists()
            if isdone:
                return  redirect('admin-login')
            else:
                packgs=packs.packages.id
                context = {'users': Morwagon_users.objects.get(user=id),'packagelist': Packages.objects.filter(is_deleted=False).order_by('price'),'from_login':1}
                return render(request, "vendor/vendor_register.html",context)
        else:
            packs=Morwagon_users.objects.get(user=id)
            if packs.packages is not None:
                packgs=packs.packages.id
                context = {'users': Morwagon_users.objects.get(user=id),'packagelist': Packages.objects.filter(is_deleted=False).order_by('price'),'from_login':1}
                return render(request, "vendor/vendor_register.html",context)
            else:
                context = {'users': Morwagon_users.objects.get(user=id),'packagelist': Packages.objects.filter(is_deleted=False).order_by('price'),'from_login':1}        
                return render(request, "vendor/vendor_register.html",context)       
    else:
        print("sandraaaa")
        return render(request, "vendor/vendor_register.html",context)


def vendor_personal(request): 
    print("vendor personal details")

    firstname=request.GET["firstname"]
    lastname=request.GET["lastname"]
    email=request.GET["email"]
    # mobile=request.GET["mobile"]
    whatsapp=request.GET["whatsapp"]
    password=request.GET["password"]
    mailotp=request.GET["mailotp"]
    dict1={}
    print("this is first next1")
    #  ......................................................................new code start
    vendor_id=request.user.id
    if vendor_id==None:
        print("this is first next2")
        # if Verifyemail.objects.filter(email=email,otp=int(mailotp),verify=True).exists():
        if Verifyemail.objects.filter(email=email,verify=True).exists():
            print("this is first next3")
            userrecord=User.objects.create_user(first_name=firstname,last_name=lastname,email=email,username=email,password=password,is_staff=1)
            morwagon = Morwagon_users(user=userrecord,first_name=firstname,last_name=lastname,email=email,whatsapp_number=whatsapp,role=2,page_no=1)
            morwagon.save()

            User.objects.filter(id=userrecord.id).update(is_active=True)

            print("hlooooooooooooooooooooooooo")

            user = authenticate(username=email, password=password)
            print(user,"hiiiiiiiiiiiiiiiiiiiiiiiii")

            userid=userrecord.id
            
            user=User.objects.get(id=userid)
            auth.login(request,user)
            print(user,"liyaaaaaaaaaaaaaaaaaaa")
            # payload = JWT_PAYLOAD_HANDLER(user)
            # jwt_token = JWT_ENCODE_HANDLER(payload)
            # userid=payload['user_id']
            
            # print(jwt_token,"jjjjjj")
            # print(user,"jipinnnnnnnnn")
            # userdetails=User.objects.get(email=email)
            # first_name = userdetails.first_name
            # last_name = userdetails.last_name
            # payload["first_name"] = first_name
            # payload["last_name"] = last_name
            # morwagon_user = Morwagon_users.objects.get(email=email)
            # payload["mobile"] = morwagon_user.mobile_number
            # ordinary_dict = {'jwt_token': jwt_token, 'user_details': payload}
            # print("hihlohihlo")
            # print(firstname,"lklklklklklkk")
            dict1 = {"status":"success"}
            return JsonResponse(dict1)
        else:
            dict1 = {"status":"fail"}
            return JsonResponse(dict1) 
    else:
        userrecord = User.objects.get(id=vendor_id)
        if password=="":
            userrecord.password=userrecord.password
        else:
            userrecord.set_password(password)
        userrecord.first_name=firstname
        userrecord.last_name=lastname
        userrecord.email=email
        userrecord.username=email
        userrecord.is_staff=1
        userrecord.is_active=True
        userrecord.save()
        user = authenticate(username=email, password=password)
        auth.login(request,user)
        usermor = Morwagon_users.objects.get(user=vendor_id)
        page_no = usermor.page_no
        morwagon = Morwagon_users.objects.filter(user=userrecord).update(first_name=firstname,last_name=lastname,email=email,whatsapp_number=whatsapp,role=2,page_no=page_no)
        dict1 = {"status":"success"}
        return JsonResponse(dict1)





# def vendor_address(request): 
#     print("hiiiiiiiiiiiiiiiiiii")
#     address1=request.GET["address1"]
#     address2=request.GET["address2"]
#     town=request.GET['town']
#     district=request.GET['district']
#     state=request.GET['state']
#     pincode=request.GET['pincode']
#     print(pincode,"hlooooooooooooooooooooooooo")


#     vendor_id=request.user.id
#     morpage = Morwagon_users.objects.get(user=vendor_id)
#     if morpage.page_no > 2:
#         Morwagon_users.objects.filter(user=vendor_id).update(address1=address1,address2=address2,
#         district=district,state=state,pincode=pincode,town=town)
#     else:
#         Morwagon_users.objects.filter(user=vendor_id).update(address1=address1,address2=address2,
#         district=district,state=state,pincode=pincode,town=town,page_no=2)
#     print(vendor_id,"hhhhhhhhhhhh")

        
#     print("hlooooooooooooooooooooooooo")
#     dict1={}
#     dict1 = {"status":200}
#     return JsonResponse(dict1) 


@csrf_exempt
def vendor_store(request): 
    print(request.POST,"hiiiiiiiiiiiiiiiiiii",request.FILES)

    district=request.POST.get("district1")
    storename=request.POST.get("storename")
    locations=request.POST.get("locations")
    logo_image=request.FILES.get('logo_image')
   
    Description=request.POST.get('Description')
    thumbnail=request.FILES.get('thumbnail')
   
    caption=request.POST.get('caption')
    gst_number=request.POST.get('gst_number')
  
    latitude=request.POST.get('latitude')
    logitude=request.POST.get('longitude')
    logo=request.POST.get("logo")
  
    
    vendor_id=request.user.id
    if vendor_id==None:
        vendor_id=request.session['vendor_sess_id']

    address1=request.POST.get('address1')
    address2=request.POST.get('address2')
    # town=request.POST.get('town')
    # district=request.POST.get('district')
    state=request.POST.get('state')
    pincode=request.POST.get('pincode')
    

    
    x=Morwagon_users.objects.get(user=vendor_id)
    x.store_name=storename
    x.location=locations
    x.latitude=latitude
    x.logitude=logitude
    x.gst_number=gst_number

    x.address1=address1
    x.address2=address2
    # x.town=town
    x.district=district
    x.state=state
    x.pincode=pincode


    # x.logo_image=logo_image
    if logo_image is not None:
        print("nidhinnnnnnnnnnnnnn")
        x.logo_image=logo_image
    if thumbnail is not None:
        x.thumbnail=thumbnail
    x.Description=Description
    x.caption=caption
    if x.page_no > 2:
        x.save()
    else:
        x.page_no = 2
        x.save()
        
    print("hihloooohihloooo")
    dict1={}
    dict1 = {"status":200}
    return JsonResponse(dict1)
    
@csrf_exempt
def vendor_document(request):
    print(request.POST,"hiiiiiiiiiiiiiiiiiii",request.FILES)

    incorp_cirtificate=request.FILES.get("incorp_cirtificate")
    gst=request.FILES.get("gst")
    pancard=request.FILES.get('pancard')
    address_proof=request.FILES.get('address_proof')
    # vendor_id=request.user.id

    vendor_id=request.user.id
    if vendor_id==None:
        vendor_id=request.session['vendor_sess_id']


    x=Morwagon_users.objects.get(user=vendor_id)
    if address_proof is not None:
        x.address_proof=address_proof
    if incorp_cirtificate is not None:
        x.incorporation_certificate=incorp_cirtificate
    if pancard is not None:
        x.pancard=pancard
    if gst is not None:
        x.GST=gst
    if x.page_no > 3:
        x.save()
    else:
        x.page_no = 3
        x.save()
           

    dict1={}
    dict1 = {"status":200}
    return JsonResponse(dict1)

def vendor_package(request): 
    package=request.GET["package"]
    # vendor_id=request.user.id
    
    vendor_id=request.user.id
    if vendor_id==None:
        vendor_id=request.session['vendor_sess_id']

    mor=Morwagon_users.objects.get(user=vendor_id)
    pack=Packages.objects.get(id=package)
    mor.packages=pack
    vendor_pack = Vendor_package.objects.create(vendor=mor,package=pack)
    vendor_pack.save()
    # mor.page_no=4

    if mor.page_no > 3:
        mor.save()
    else:
        mor.page_no = 3
        mor.save()

    print("hlooooooooooooooooooooooooo")
    dict1 = {}
    dict1['package_id']=pack.id
    id=pack.id
    dict1['package_name']=pack.package_name
    dict1['price']=pack.price
    dict1['product_listign_limit']=pack.product_listign_limit
    dict1['dispaly_image_limit']=pack.dispaly_image_limit
    dict1['dispaly_add_limit']=pack.add_limit
    dict1['dispaly_video_limit']=pack.video_limit
    dict1['benifits']=pack.benifits
    dict1['months']=pack.months
    price=pack.price
    price=float(price)*100
    k=payment_calcnew(id,vendor_id)
    total = k['amt']
    totals=float(total)*100
    if totals != 0.00:
        dict1['total'] = total
        client = razorpay.Client(
        auth=("rzp_test_WeK5FaW95tFJCc", "BmoSUGl8raqBkuBLPLxGBMNE"))
        data = { "amount": totals, "currency": "INR", "receipt": "order_rcptid_11"}
        payment = client.order.create(data=data)
        print(payment)
        print(k,"....................................kkkkkkkkkkkkkkkkkkkkk")
        dict1['amountraz'] = payment["amount"]
        dict1['orderraz'] = payment["id"]
        x=k["taxes"]
        dict1['taxes'] = x
        dict1["result"]="success"
        print(dict1,"..............................................dict")
        return JsonResponse(dict1)
    else:
        mor=Morwagon_users.objects.get(user=vendor_id)

        pkg=mor.packages.id
        pack=Packages.objects.get(id=pkg)
        months=pack.months
        d1 = datetime.date.today()
        d2 = d1 + relativedelta(months=months)
        print(d2,"hhhhh")

        # datelast=d2.strftime("%Y-%m-%d %H:%M:%S") 1346
        mor.expirydate=d2
        mor.is_done=True
        mor.save()
        print("fail ..................................................fail")
        dict1["result"]="fail"
        return JsonResponse(dict1) 

def generateOTP() :
     digits = "0123456789"
     OTP = ""
     for i in range(4) :
         OTP += digits[math.floor(random.random() * 10)]
     return OTP

def mail_verification(request):
    if request.GET.get('email'):
        email=request.GET["email"]    
        print(email,"emaillllllllllll")
        dict1 = {}
        if Verifyemail.objects.filter(email=email,verify=True).exists() and User.objects.filter(email=email).exists():
            moruser=Morwagon_users.objects.filter(email=email)
            # qs_json=serializers.serialize("json",moruser )
            # return HttpResponse(qs_json, content_type='application/json')
            dict1 = {"status":"login please"}
            return JsonResponse(dict1)        
        elif Verifyemail.objects.filter(email=email,verify=False).exists():
            print("second oneeeeeeeeeeeeeeeeeee")
            Verifyemail.objects.filter(email=email).delete()
            print("deleteeeeeeeeeeeee")
            o=generateOTP()
            verfiymodel = Verifyemail(email=email,otp=o)
            verfiymodel.save()
            htmlgen = '<p>Your OTP is <strong>'+o+'</strong></p>'
            send_mail('OTP request',o,'reshma@beaconinfo.tech',[email], fail_silently=False, html_message=htmlgen)
            print(o,"lklklklklklk")
            dict1 = {"status":204}
            return JsonResponse(dict1)
        else:
            print("third oneeeeeeeeeeeeeeeeee")
            o=generateOTP()
            htmlgen = '<p>Your OTP is <strong>'+o+'</strong></p>'
            send_mail('OTP request',o,'reshma@beaconinfo.tech',[email], fail_silently=False, html_message=htmlgen)
            print(o,"lklklklklklk")
            # dict1['otp_from_back']=o
            # print(dict1,"liyaaaaaaaaaa")
            verfiymodel = Verifyemail(email=email,otp=o)
            verfiymodel.save()
            print("daaaaaaaaaaaaaaaaaaaaaa")
            dict1 = {"status":204}
            return JsonResponse(dict1)
        

def otp_check(request):
    print("hiiiiii")
    if request.GET.get('otp_from_front'):
        otp_from_front=request.GET["otp_from_front"] 
        email=request.GET["email"] 
        print(email,"mmmmmmmmmmm")  
        print(otp_from_front,"otp")
        dict1={}
        if Verifyemail.objects.filter(email=email).exists():
            otpmail=Verifyemail.objects.filter(email=email).last()
            otpback=otpmail.otp
            y=otpmail.created_on
            print("hlooo guyssssssssssss")


            # if otpback == int(otp_from_front):
            if(int(otp_from_front)==int(otp_from_front)):
                now = datetime.datetime.utcnow().replace(tzinfo=utc)
                x =now-y
                total_seconds = x.total_seconds()
                if total_seconds > 300:
                    dict1 = {"status":"fail"}
                    print("failllllllllll111111111111111111")
                    return JsonResponse(dict1)
                else:
                    dict1 = {"status":"success"}
                    Verifyemail.objects.filter(email=email).update(verify=True)
                    print("successsssss")
                    return JsonResponse(dict1)
            else:
                print("failllllllllll2222222222222222222222")
                dict1 = {"status":"fail"}
                print("failllllllllll")
                return JsonResponse(dict1)


def html_to_pdf_viewnew(request):
    if request.method=='POST':
        pkg_id=request.POST.get('packageID')
        print(pkg_id,"idddddddddddddddddddddddddddd")
        payment_id=request.POST.get('payment_id')
        # uid=request.user.id

        uid=request.user.id
        if uid==None:
            uid=request.session['vendor_sess_id']
        vendor_details=Morwagon_users.objects.get(user=uid)
        k=payment_calcnew(pkg_id,uid)
        k['vndr_details']=vendor_details
        k['payment_id']=payment_id
    print(settings.BASE_DIR,"............................settings.BASE_DIR")
    html_string = render_to_string(str(settings.BASE_DIR)+"/celebrations/templates/vendor/Billpdfnew.html", {'bill': k})
    html = HTML(string=html_string,  base_url=request.build_absolute_uri())
    html.write_pdf(target='/tmp/Registration_receipt.pdf');
    fs = FileSystemStorage('/tmp')
    with fs.open('Registration_receipt.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="Registration_receipt.pdf"'
        return response

@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
# @user_passes_test(not_in_vendor, login_url='/login')
def vendor_reset_passwordfirst(request):
    return render(request, "vendor/resetpasswordnewfirst.html")

def password_verification(request):
    id=request.user.id
    user=User.objects.get(id=id)
    username=user.username
    if request.GET.get('password'):
        password=request.GET["password"]   
        authuser = authenticate(username=username, password=password) 
        dict1 = {}
        if authuser:
            dict1 = {"status":"success"}
            return JsonResponse(dict1) 
        else:
            dict1 = {"status":"fail"}
            return JsonResponse(dict1) 
    else:
        dict1 = {"status":"fail"}
        return JsonResponse(dict1)


def new_password_change(request):
    id=request.user.id 
    user=User.objects.get(id=id)
    username=user.username
    newpassword=request.GET["newpassword"]
    user.set_password(newpassword)
    user.save()
    dict1 = {}
    authuser = authenticate(username=username, password=newpassword)
    if authuser:
        dict1 = {"status":"success"}
        return JsonResponse(dict1) 
    

def Vendorswal(request,pages,id):
    print(id,'sandraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
    print(pages)
    id=id
    request.session['vendor_sess_id']=id
    context = {"swalpage":pages,'users': Morwagon_users.objects.get(user=id),'packagelist': Packages.objects.filter(is_deleted=False).order_by('price'),'from_login':1}
    return render(request, "vendor/vendor_register.html",context)



def vendor_logout(request):
    list(messages.get_messages(request))
    return redirect('/login')


def test(request):
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    if request.method=='POST':
        imageList=request.POST.getlist('imageArr[]')
        for image in imageList:
            format, imgstr = image.split(';base64,') 
            ext = format.split('/')[-1] 
            img = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
            print(img,"................................img55555")
            # print("ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo",img,".................")
        
    return render(request, "vendor/test.html")

def check_whatsapp_new(request):
    id_new=request.GET['new_id']
    wht = request.GET['w_no']
   
    if Morwagon_users.objects.filter(whatsapp_number=wht).exclude(id=wht).exists():
        print(id_new,'jkjkj')
        print(wht,'llk')
        return JsonResponse({'status':'exist'})
    else:
        return JsonResponse({'status':'not_exists'})  


    
  
    

def apiTest(request):
    product = Products.objects.filter(id__in=[21,22]).values()
       
    for data in list(product): 
        albums = Album.objects.filter(service_id=data['id'],is_deleted=False).values()
        data['albums']=list(albums)


    return JsonResponse(data,safe=False)


# ////////////////////////////////////////////////////////////////////////// new suggestions from april 28

@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def report_vendor_customer(request):#report vendor list view
    user_id=request.user.id
    print(user_id,'jijijijijijij')
    morwagnUser=Morwagon_users.objects.get(user=user_id)
    print("88888888888888888888888888888888888888888888888888888888888888")
    # report_vendor=Morwagon_users.objects.filter(is_deleted=False,is_suspended=False,id=morwagnUser.id).annotate(count=Count('user_report')).filter(count__gt=0)
    report_vendor=Report_vendor.objects.filter(is_deleted=False,vendor__is_suspended=False,vendor=morwagnUser.id)
    # report_vendor=Report_vendor.objects.filter(is_deleted=False,vendor__is_suspended=False).annotate(count=Count('vendor_id')).filter(count__gt=0)
    context = {'report_vendor': report_vendor}
    return render(request, "vendor/vendor_report_customer.html",context)

# @complete_profile_required   
# @cache_control(no_cache=True, must_revalidate=True, no_store=True)
# @login_required(login_url='/login')
# @user_passes_test(not_in_vendor, login_url='/login')
# def suspend_report(request,id):
#     print(id,"hhhhhhh")
#     morvendor=Report_vendor.objects.get(id=id)
#     morvendorid=morvendor.vendor.id
#     mor =Morwagon_users.objects.get(id=morvendorid)
#     morid=mor.id
#     moremail=mor.email
#     print(moremail,"ggggggggggggg")
#     # Morwagon_users.objects.filter(id=morid).update(is_suspended=True)
#     htmlgen = '<p>Your account has suspended </p>'
#     send_mail('Account deletion',"kkkk",'reshma@beaconinfo.tech',[moremail], fail_silently=True, html_message=htmlgen)            
#     print("liyaaaaaaaaaa")
#     # return HttpResponse("lol")
#     return redirect('/vendor/report_vendor_customer')

def createUnits(request):
    all_units=Units.objects.filter(is_deleted=False)
    if request.method=='POST':
        objunit=request.POST.get('unitname')
        unit_save=Units(unit=objunit)
        unit_save.save()
        return render(request, "admin/add_units.html",{"allunits":all_units})
    return render(request, "admin/add_units.html",{"allunits":all_units})



def EditUnit(request):
    all_units=Units.objects.filter(is_deleted=False)
    if request.method=='POST':
        id=request.POST.get('edit-id-unit')
        unitobj=request.POST.get('edittaxname')
        objunits=Units.objects.get(id=id)
        objunits.unit=unitobj
        objunits.save()
        return render(request, "admin/add_units.html",{"allunits":all_units})
    return render(request, "admin/add_units.html",{"allunits":all_units})



def DeleteUnit(request):
    all_units=Units.objects.filter(is_deleted=False)
    if request.method=='POST':
        id=request.POST.get('delete-unit')
        objunits=Units.objects.get(id=id)
        objunits.is_deleted=True
        objunits.save()
        return render(request, "admin/add_units.html",{"allunits":all_units})
    return render(request, "admin/add_units.html",{"allunits":all_units})


def getUnit(request):
    if request.method=='GET':
        id=request.GET.get('data1')
        objunits=Units.objects.get(id=id).unit
        return JsonResponse(objunits,safe=False)

def getUnitDropdown(request):
    if request.GET.get('p'):
        id=request.GET.get('p')
        priceUnit=PriceUnit.objects.filter(product=id)
        dataunit = serializers.serialize('json',priceUnit)
        return HttpResponse(dataunit, content_type="application/json")
    all_units=Units.objects.filter(is_deleted=False)
    data = serializers.serialize('json',all_units)
    return HttpResponse(data, content_type="application/json")

# def getNumber(request):
#     nmbr=request.GET.get('nmbr')
#     eid=request.GET.get('eid')
#     if eid:
#         objunits={"is_exists":"its edit"}
#         return JsonResponse(objunits,safe=False)
#     else:
#         is_exist=Products.objects.filter(is_deleted=False,phone=nmbr).exists()
#         objunits={"is_exists":is_exist}
#         return JsonResponse(objunits,safe=False)




@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def flag_report(request,id):
    print(id,"jjjjjjjjj")
    report_vendor=Report_vendor.objects.get(id=id)
    report_vendor.flag=True
    report_vendor.save()
    return redirect('/vendor/report_vendor_customer')

@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def faqFirstView(request):
    user_id=request.user.id
    morwagnUser=Morwagon_users.objects.get(user=user_id)
    prdt=Products.objects.filter(is_deleted=False,vendor=morwagnUser.id)   
    # faq=Faq.objects.filter(is_deleted=False)
    # print(faq)
    return render(request,'vendor/faq.html',{'service':prdt,})

# def AddFaq(request):
#     user_id=request.user.id
#     morwagnUser=Morwagon_users.objects.get(user=user_id)
#     prdt=Products.objects.filter(is_deleted=False,vendor=morwagnUser.id)
#     faq=None
#     servicepro=None
#     if request.method=='POST':
#         service=request.POST.get('product')
#         servicepro=Products.objects.get(id=service)
#         question=request.POST.get('question')
#         answer=request.POST.get('answer')
#         saveFaq=Faq(product=servicepro,question=question,answer=answer)
#         saveFaq.save()
#         faq=Faq.objects.filter(is_deleted=False,product=servicepro)
#     context={
#         'service':prdt,
#         'faq':faq,
#         'ser':servicepro,
#     }
#     return render(request,'vendor/faqAdd.html',context)

@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def viewFaqs(request,id):
    user_id=request.user.id
    morwagnUser=Morwagon_users.objects.get(user=user_id)
    prdt=Products.objects.filter(is_deleted=False,vendor=morwagnUser.id,id=id)
    faq=Faq.objects.filter(product=id,is_deleted=False)
    if request.method=='POST':
        service=request.POST.get('product')
        servicepro=Products.objects.get(id=service)
        question=request.POST.get('question')
        answer=request.POST.get('answer')
        saveFaq=Faq(product=servicepro,question=question,answer=answer)
        saveFaq.save()
    di=id
    sr=Products.objects.get(id=id)
    context={
        'faq':faq,
        'service':prdt,
        'di':di,
        'sr':sr
    }
    return render(request,'vendor/faqView.html',context)

@complete_profile_required   
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/login')
@user_passes_test(not_in_vendor, login_url='/login')
def editFaq(request,id):
    user_id=request.user.id
    morwagnUser=Morwagon_users.objects.get(user=user_id)
    prdt=Products.objects.filter(is_deleted=False,vendor=morwagnUser.id)
    obj=Faq.objects.get(id=id)
    di=obj.product_id
    edit=True
    print(di)
    if request.method=='POST':
        obj.question=request.POST.get('question')
        obj.answer=request.POST.get('answer')
        obj.save()
        return redirect('/vendor/ViewFaq/'+str(di))
    context={
        'service':prdt,
        'obj':obj,
        'di':di,
        'edit':edit,
    }
    return render(request,'vendor/faqView.html',context)

def deleteFaq(request,id):
    obj=Faq.objects.get(id=id)
    obj.is_deleted=True
    obj.save()
    di=obj.product_id
    return redirect('/vendor/ViewFaq/'+str(di))





