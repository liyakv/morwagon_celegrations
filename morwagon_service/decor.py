from django.contrib.messages.api import error
from django.shortcuts import redirect
from  celebrations. models import Morwagon_users,Packages
from django.shortcuts import render

def complete_profile_required(view_func):

    def wrapper_func(request, *args, **Kwargs):
        if request.user.id!=None:
            
            print(request.user.id,".........................................noneee")
            mor=Morwagon_users.objects.get(user=request.user)
            if mor.is_done == False:

                page =Morwagon_users.objects.get(user=request.user).page_no
                if page==4:
                    page=4
                else:
                    page=page+1
              
                context = {"swalpage":page,'users': Morwagon_users.objects.get(user=request.user.id),'packagelist': Packages.objects.filter(is_deleted=False),'from_login':'iiiii'}
                return render(request, "vendor/vendor_register.html",context)
                
            if mor.is_done == True:
                return view_func(request, *args, **Kwargs)  
                
            else:
                return view_func(request, *args, **Kwargs)
        else:
            return render(request, "admin/admin_login.html")
    return wrapper_func



def admin_issuspended(view_func):

    def wrapper_func(request, *args, **Kwargs):
        if request.user.id!=None:
            mor=Morwagon_users.objects.get(user=request.user)
            if mor.is_suspended == True:
                return render(request, "admin/admin_login.html")
            else:    
                return view_func(request, *args, **Kwargs)      
        else:
            return render(request, "admin/admin_login.html")
    return wrapper_func    