"""morwagonproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
# from django.urls import path

# urlpatterns = [
#     path('admin/', admin.site.urls),
# ]
from os import name
from django.contrib import admin
from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns

from celebrations.views import alpha
from celebrations.views import beta
from celebrations.views import gamma
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.contrib.auth import views
# import notifications.url

urlpatterns = [
    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('password_change/', views.PasswordChangeView.as_view(),
         name='password_change'),
    path('password_change/done/', views.PasswordChangeDoneView.as_view(),
         name='password_change_done'),

    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', views.PasswordResetDoneView.as_view(),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('reset/done/', views.PasswordResetCompleteView.as_view(),
         name='password_reset_complete'),
    path('vendor/', include('celebrations.urls')),
    path('admin/', admin.site.urls),
    # path('',include('customerdetails.urls')),
    # path('login',views.loginuser.as_view()),
    # path('workor',views.workor.as_view()),
    path('dashboard', alpha.dashboard, name='dashboard'),
    # path('croptest',views.croptest,name='dashboard'),
    path('services', alpha.services, name='services'),
    path('banner', alpha.banner, name='banner'),
    path('create/category', alpha.create_category, name='create-category'),
    path('delete/category', alpha.delete_category, name='delete-category'),
    path('delete/sub-category', alpha.delete_subcategory,
         name='delete-sub-category'),
    path('create/sub-category', alpha.create_subcategory,
         name='create-sub-category'),
    path('edit/sub-category', alpha.edit_subcategory, name='edit-sub-category'),
    path('service/requests', alpha.service_request, name='service-request'),
    path('service/requests/approve/',
         alpha.service_approve, name='service-approve'),
    path('vendors', alpha.vendorlisting, name='vendors'),
    path('vendors/request', alpha.vendorapproval, name='vendor-approve'),
    path('package', alpha.package, name='package'),
    path('package/create', alpha.package_create, name='package-create'),
    path('package/edit/<int:get_id>', alpha.package_edit, name='package-edit'),
    path('package/view/<int:get_id>', alpha.package_view, name='package-view'),
    path('package/upgrade', alpha.package_upgrade, name='package-upgrade'),
    path('accept/upgrade', alpha.accept_upgrade, name='accept-upgrade'),
    path('edit/vendor', alpha.accept_upgrade, name='accept-upgrade'),
    path('product/view/<int:get_id>', alpha.productview, name='product-view'),
    path('product/album/image/<int:get_id>',alpha.approve_albumimgae, name='imageapprove'),
    # dont confuse with services created by admin its from vendor
    path('service-vendor/edited', alpha.editserviceapproval, name='editservice'),
    path('banner/edit', alpha.edit_banner, name='banner-edit'),
    path('banner/image/<int:get_id>', alpha.bannerimgaeview, name='banner-image'),
    path('banner/delete', alpha.banner_delete, name='banner-delete'),
    path("edit/category", alpha.editcategory, name='edit-category'),
    path('add/list', alpha.addlist, name="add-approve"),
    # path('add/images',views.addimages,name="add-images"),
    path('add/approve', alpha.addapprove, name='add-apprrove'),
    path('video/approve/<int:get_id>', alpha.videoapprove, name='video-approve'),
    path('reviewFilter',alpha.reviewFilter,name='review-filter'),
    path('review', alpha.productreview, name='product-review'),
    path('review/<int:id>/suspend',alpha.reviewFlagSuspendVendor,name='suspend-review-vendor'),
    path('review/<int:id>', alpha.review, name='review'),
    path('delete/review/<int:get_id>',
         alpha.deleteReviewimage, name='reviewimg-delete'),
    path('approve/review/<int:get_id>',
         alpha.approvereview, name='review-approve'),
    path('reject/review/<int:id>/', alpha.rejectreview, name='review-reject'),
    path('start/review', alpha.startreview, name='start-review'),
    path('checkback', alpha.checkback, name='check-back'),
    path('checkback1', alpha.checkback1, name='check-back1'),
    path('login', alpha.admin_login, name='admin-login'),
    path('logout/admin', alpha.logout_admin_vendor, name='logout-admin-vendor'),


    # ........................................liya kv............................................................................
    # .th('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    # path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/register', gamma.registermodel.as_view()),  # bharathhhh
    path('api/login', gamma.Loginmodel.as_view()),  # bharathhhh
    # newwwwwwwwwwwwwwwwwwwwwwww
    path('api/verifyuser/<str:email>', gamma.VerifiedUsermodel.as_view()),
    path('api/category', gamma.categorymodel.as_view()),
    path('api/categorybyid/<int:id>', gamma.categorybyidmodel.as_view()),
    path('api/subcategory', gamma.Sub_categorymodel.as_view()),
    path('api/subcategorybyid/<int:id>', gamma.Sub_categorybyidmodel.as_view()),
    path('api/banner', gamma.Bannermodel.as_view()),
#     path('api/popularproduct/<str:lat>/<str:lng>/<str:radius>/<str:limit>',gamma.Popularproductmodel.as_view()),    
    path('api/popularproduct',gamma.Popularproductmodel.as_view()),    
    path('api/updatepopular', gamma.Popularproductpostmodel.as_view()),
    path('api/productbyid/<int:id>', gamma.Productbyidmodel.as_view()),
    path('api/productbycategoryid/<int:id>',gamma.Productbycategoryid.as_view()),
    path('api/productbysubcategoryid/<int:id>',gamma.Productbysubcategoryid.as_view()),
    path('api/productbyname/<str:name>', gamma.Productbyname.as_view()),
#     path('api/trendingproduct/<str:lat>/<str:lng>/<str:radius>/<str:limit>',gamma.Trendingproductmodel.as_view()),
    path('api/trendingproduct',gamma.Trendingproductmodel.as_view()),
#     path('api/nearbyproduct/<str:lat>/<str:lng>/<str:radius>/<str:limit>',gamma.Explorenearbyproductmodel.as_view()),
    path('api/nearbyproduct',gamma.Explorenearbyproductmodel.as_view()),
    path('api/bestservicestoreproduct/<str:lat>/<str:lng>/<str:radius>/<str:limit>',gamma.Bestservicestoremodel.as_view()),
    path('api/productsbyvendorid/<int:id>', gamma.Productbyvendorid.as_view()),
    path('api/allvendors', gamma.Allvendormodel.as_view()),
    path('api/vendorbyid/<int:id>', gamma.Vendorbyidmodel.as_view()),
    path('api/albumbyproductid/<int:id>/<int:page>',gamma.Albumsbyproductidmodel.as_view()),  # bharathhhh
    path('api/albumbyvendorid/<int:id>',gamma.Albumsbyvendoridmodel.as_view()),  # bharathhhh  ???????
    path('api/videobyproductid/<int:id>',gamma.Videobyproductidmodel.as_view()),  # bharathhhh
    path('api/videobyvendorid/<int:id>',gamma.Videobyvendoridmodel.as_view()),  # bharathhhh
    path('api/favourites/<int:productid>',gamma.Favouritesmodel.as_view()),  # bharathhhh  
    path('api/favouritesbyuser',gamma.Favouritesbyuser.as_view()),
    path('api/productisinwishlist',gamma.Favouritesbyuserandproduct.as_view()),
    path('api/storebyname/<str:name>', gamma.Vendorbystorename.as_view()),
    # register with sicialmedia
    path('api/registersocialmedia', gamma.registersocialmediamodel.as_view()),
    # path('api/loginsocialmedia',views.Loginsocialmediamodel.as_view()),
    path('api/reviewrating', gamma.Reviewratingmodel.as_view()),  # review post
    # review put by review and get by review id
    path('api/reviewratingputandget/<int:id>',gamma.Reviewratingputandgetmodel.as_view()),
    path('api/reviewratingproduct/<int:id>',gamma.Reviewratingpbyproductidmodel.as_view()),  # review get by product id
    path('api/reviewratingproductanduser',gamma.Reviewratinggetuserpromodel.as_view()),  # review get by product id and userid
    path('api/getadds/<str:lat>/<str:lng>/<str:radius>/<str:limit>', gamma.Addsgetmodel.as_view()),  # get Adds model
    # post Adds and enquiry model
    path('api/enquirypost', gamma.Enquirypostmodel.as_view()),
    # searck by productname and storename
    path('api/searchbykey/<str:search_key>', gamma.Searchbykeymodel.as_view()),
    path('api/searchbyproductcategory', gamma.Searchnearbyproductmodel.as_view()),
#     path('api/Productfavouritemodel', gamma.Productfavouritemodel.as_view()),
    path('api/updateaddcount', gamma.Addspostmodel.as_view()),
    path('api/product', gamma.Productmodel.as_view()),
    path('api/customerdetails', gamma.Customergetmodel.as_view()),
    path('api/updateuserdetails', gamma.Customerupdatemodel.as_view()),
    path('api/citylist', gamma.Citygetmodel.as_view()),
    path('api/generateotp', gamma.generateOtpformobile.as_view()),
    path('api/verifyotp', gamma.verifyOtp.as_view()),
    path('api/forgetpassword', gamma.forgetpassword.as_view()),
    path('api/reportvendorchoices', gamma.Report_choices.as_view()),
    path('api/reportvendor', gamma.Reportvendormodel.as_view()),
    path('api/mobilesignup', gamma.gene_Otp_cont_phone.as_view()),
    path('api/mobilesignupverifyotp', gamma.verfication_phone_otp.as_view()),
    path('api/customeremailchange', gamma.Customeremailmodel.as_view()),
    path('api/faqview/<int:id>/<int:page>', gamma.faqmodel.as_view()),
    path('api/contactus', gamma.contactus.as_view()),
     # alpha

    path('check/package', alpha.packagecheck, name='logout'),
    path('notification', alpha.picknotification, name='notification'),
    path('notification/details', alpha.notification_details,name='notification-details'),
    path('service/edit/approve/<int:id>',alpha.serviceedit_approve, name='serviceeditaqpprove'),
    path('editnotification', alpha.notification_change,name='serviceeditaqpprove'),
    path('delete/vendor', alpha.delete_vendor, name='del-vendor'),
    # path('bestservicestore',alpha.bestservicestore,name='bestservicestore'),
    

    # ------------------------------by sandra----------------------------------------------
    path('ServiceFilter/', alpha.ServiceFilter, name='ServiceFilter'),
    path('UpdateServiceFilter/', alpha.UpdateServiceFilter,name='UpdateServiceFilter'),
    path('profile_vendor_admin/<int:vendor_id>',alpha.profile_vendor_admin, name='profile_vendor_admin'),
    path('edit_vendor_admin/<int:vendor_id>',alpha.edit_vendor_admin, name='edit_vendor_admin'),
    path('profile_admin', alpha.profile_admin, name='profile_admin'),
    path('vendor/filter', alpha.vendorlistingfilter, name='vendor_filter'),
    # path('service/filter',alpha.servicerequestfilter,name='service_filter'),
    path('add/reject', alpha.add_reject, name='add-reject'),
    path('add/tax', alpha.add_tax, name='tax'),
    path('create/tax', alpha.createtax, name='create-tax'),
    path('get/tax', alpha.get_taxdetails, name='get-tax'),
    path('get/edit', alpha.edit_tax, name='tax-edit'),
    path('delete/tax', alpha.delete_tax, name='tax-delete'),
    path('edit/services/<int:id>',alpha.edit_services, name='edit_services'),
    path('edit/save/services/<int:id>',alpha.editbyadmin, name='edit_services'),
    path('get/category/details',alpha.getcategory, name='edit_services'),
    path('product/status',alpha.prodct_status, name='edit_services'),
    path('vendor/status',alpha.vendor_suspend_revoke, name='edit_services'),

# /////////////////////////////////////////////////////////////////////// new suggestions april 28

    path('report_admin_customer',alpha.report_admin_customer, name='report_admin_customer'),
    path('suspend_report_admin/<int:id>',alpha.suspend_report_admin, name='suspend_report_admin'),
    path('delete_report_admin/<int:id>',alpha.delete_report_admin, name='delete_report_admin'),
     



]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
